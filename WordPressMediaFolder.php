<?php

namespace SCGB;

use Exception;
use mysqli;

class WordPressMediaFolder
{
    private static array $mediaFoldersByName = [];
    private static array $mediaFoldersByID = [];
    private static string $optionFolderColours;

    private int $id;
    private string $name;
    private ?int $parent_id;
    private WordPressMediaFolder $parent;
    private ?string $colour = null;
    private ?int $ord;
    private array $arrChildren = [];
    private array $contents = [];

    public function __construct(
        int $id,
        string $name,
        int $parent_id,
        int $ord)
    {
        $this->id = $id;
        $this->name = $name;
        $this->parent_id = $parent_id;
        $this->ord = $ord;
    }

    static public function getMediaFolderByName(string $name): ?WordPressMediaFolder
    {
        return self::$mediaFoldersByName[$name] ?? null;
    }
    static public function getMediaFolderByID(int $id): ?WordPressMediaFolder
    {
        return self::$mediaFoldersByID[$id] ?? null;
    }

    /**
     * @throws Exception
     */
    public static function loadMediaFolders(mysqli $conn) : void
    {
        $sql = "SELECT id, name, parent, ord " .
            "FROM wp_fbv " .
            "ORDER BY id" . "\n";

        $result = $conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load media folders: ' . $conn->error);
        }
        while($row = $result->fetch_assoc()) {
            $id = $row['id'];
            $name = $row['name'];
            $parent = $row['parent'];
            $ord = $row['ord'];

            $mediaFolder = new WordPressMediaFolder($id, $name, $parent, $ord);
            self::$mediaFoldersByName[$name] = $mediaFolder;
            self::$mediaFoldersByID[$id] = $mediaFolder;
        }
        // Need to iterate through the array again to set the parents and children
        foreach (self::$mediaFoldersByID as $mediaFolder) {
            $parent_id = $mediaFolder->parent_id;
            if ($parent_id != 0) {
                $parent = self::$mediaFoldersByID[$parent_id];
                $mediaFolder->parent = $parent;
                $parent->arrChildren[] = $mediaFolder;
            }
        }

        // Now get the contents of each folder
        $sql = "SELECT folder_id, attachment_id " .
            "FROM wp_fbv_attachment_folder " .
            "ORDER BY folder_id, attachment_id" . "\n";

        $result = $conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load media folder contents: ' . $conn->error);
        }
        while($row = $result->fetch_assoc()) {
            $folder_id = $row['folder_id'];
            $attachment_id = $row['attachment_id'];
            $mediaFolder = self::$mediaFoldersByID[$folder_id];
            $mediaFolder->contents[] = $attachment_id;
        }

        // Finally get the folder colours
        $sql = "SELECT option_value " .
            "FROM wp_options " .
            "WHERE option_name = 'fbv_folder_colors'" . "\n";

        $result = $conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load media folder colours: ' . $conn->error);
        }
        $row = $result->fetch_assoc();
        if ($row !== null) {
            self::$optionFolderColours = $row['option_value'];
            // string of the form a:5:{i:18;s:7:"#F9D165";i:19;s:7:"#7AD149";i:31;s:7:"#F73A21";i:33;s:7:"#D7E2FA";i:17;s:7:"#3C78D8";}
            // Need to convert to an array of the form [18 => "#F9D165", 19 => "#7AD149", ...]
            $optionFolderColours = self::$optionFolderColours;
            $arrTemp = explode(':', $optionFolderColours);
            $numColours = $arrTemp[1];
            for ($i=0; $i<$numColours; $i++) {
                $index = 3 + $i*3;
                $folder_id = $arrTemp[$index];
                // strip the ;s
                $folder_id = substr($folder_id, 0, strlen($folder_id)-2);
                $x = explode('"', $arrTemp[$index+2]);
                $colour = $x[1];
                self::$mediaFoldersByID[$folder_id]->colour = $colour;
            }
        }
    }
}