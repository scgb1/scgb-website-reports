<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/SCGB/Utilities/scgb_autoload.php');

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Fire up the Template Engine
$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader, [
    'cache' => __DIR__ . '/templates/cache',
    'debug' => true,
]);
$twig->addExtension(new DebugExtension());

// Start of the main processing
Utils::logger()->info('Started SCGB Sugati Tour Processor', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    Holiday::initialise($conn);
    // load Holidays from Sugati
    SugatiTour::loadToursFromSugati();

    // Load the current Holidays from the database
    $arrWebSiteHolidays = HolidayFromWebsite::loadHolidays($conn);

    $reportSugatiTours = new ReportSugatiTours();

    // Now iterate through the Website Holidays and prices the information from Sugati
    foreach ($arrWebSiteHolidays as $holiday => $holidayData) {
        Utils::logger()->info("Processing holiday: " . $holidayData->getPostID() . "/" . $holiday);
        // This holiday is already on the website (which it should be) - check if it needs updating
        $holidayData->checkForUpdatesFromSugati();
        $reportSugatiTours->addReportData($holidayData);
    }

    HolidayFromWebsite::updateWebSite();
    $reportSugatiTours->renderReport($twig);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update Holiday: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$conn->close();
Utils::logger()->info('End of SCGB Sugati Holiday Processor', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED




