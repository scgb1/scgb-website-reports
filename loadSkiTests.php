<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use SCGB\Holiday\Utilities\SqlLogger;
use SCGB\Holiday\Utilities\Utils;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Utilities/SqlLogger.php';
require_once __DIR__ . '/Post/SkiTest/SkiTest.php';
require_once __DIR__ . '/Post/SkiTest/SkiTestFromWebsite.php';
require_once __DIR__ . '/Post/SkiTest/SkiTestFromFile.php';

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Source file in first argument
if ($argc < 3) {
    Utils::logger()->emergency(
        'Usage: php loadSkiTests.php source-file',
        array('file' => basename(__FILE__), 'line' => __LINE__));
    die(1);
}
$sourceFile = $argv[1];
$basePost = intval($argv[2]);
$metaFile = $argv[3];


// Start of the main processing
Utils::logger()->info('Started SCGB Ski Test Loader', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

try {
    $sqlLogger = new SqlLogger($conn);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to create SQL Logger: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    // Load the current ski tests from the database
    $arrWebSiteSkiTests = SkiTestFromWebsite::loadSkiTests($conn, $metaFile, $sqlLogger);

    // load the ski tests from the csv file
    $arrSkiTestsFromFile = SkiTestFromFile::loadSkiTests($sourceFile);

    // Now iterate through the data read from the csv file and see what needs to be updated
    foreach ($arrSkiTestsFromFile as $ski => $skiData) {
        if (key_exists($ski, $arrWebSiteSkiTests)) {
            // This ski is already on the website - check if it needs updating
            $arrWebSiteSkiTests[$ski]->checkForUpdates($skiData);
        } else {
            Utils::logger()->info(
                'Ski "' . $ski . '" not found in database - Creating',
                array('file' => basename(__FILE__), 'line' => __LINE__)
            );
            $arrSkiTestMeta = SkiTestFromWebsite::createEmptySkiTest($ski, $basePost, $conn, $sqlLogger);
            $newSkiTest = new SkiTestFromWebsite($ski, $arrSkiTestMeta);
            $newSkiTest->checkForUpdates($skiData);
        }
    }
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update Ski Tests: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

Utils::logger()->info('End of SCGB Ski Test Loader', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED




