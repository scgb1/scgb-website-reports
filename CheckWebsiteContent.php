<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use SCGB\Holiday\Report\ReportGenerator;
use SCGB\Holiday\Resort\ResortCheckFunctions;
use SCGB\Holiday\Taxonomy\CountryTaxonomy;
use SCGB\Holiday\Utilities\IssueLogger;
use SCGB\Holiday\Utilities\SqlLogger;
use SCGB\Holiday\Utilities\Utils;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Post/Resort/ResortCheckFunctions.php';
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Utilities/SqlLogger.php';
require_once __DIR__ . '/Utilities/IssueLogger.php';
require_once __DIR__ . '/Report/ReportGenerator.php';
require_once __DIR__ . '/Taxonomy/Taxonomy.php';
require_once __DIR__ . '/Taxonomy/CountryTaxonomy.php';


// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
$bulkHandler = Utils::initialise();


// Look for ant php file that start with report and end with .php and require them
$files = glob(__DIR__ . '/Report/report*.php');
foreach ($files as $file) {
    require_once "$file";
}

// Fire up the Template Engine
$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader, [
    'cache' => __DIR__ . '/templates/cache',
    'debug' => true,
]);
$twig->addExtension(new DebugExtension());

// Start of the main processing
Utils::logger()->info('Started SCGB Website Data Checker', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    $issueLogger = new IssueLogger();
    $sqlLogger = new SqlLogger($conn);
    $reporter = new ReportGenerator($sqlLogger);
    //WordPressMediaFolder::loadMediaFolders($conn);
    $t = CountryTaxonomy::getCountriesByPostID(1236, $conn);

    $arrCheckFunctions['sc_resort'] = new resortCheckFunctions($conn, $sqlLogger, $issueLogger);

    /**
     * Create the list of checks to run. For each meta_key there is a list of 0 or more checks to run
     */
    // Read data from the table wp_postmeta
    $sql = "SELECT p.post_type, m.post_id, post_title, post_name, meta_id, meta_key, meta_value " .
        "FROM wp_postmeta m, wp_posts p " .
        "where post_id = id  " .
        "and post_status = 'publish' " .
        "and post_type = 'sc_resort' " .
        "order by p.post_type ASC, p.post_name ASC, meta_key ASC;";
    $result = $conn->query($sql);

    // Output the data as a csv
    $fp = fopen('data/resort-meta.csv', 'w');

    // Output the headers
    fputcsv($fp, array('Post ID', 'Post Title', 'Meta ID', 'Meta Key', 'Meta Value'));

    // We need to collect all the data for a resort before we can run the checks
    // as some checks require data from multiple meta_keys
    $arrMetaKeys = array();
    $current_post_id = -1;
    $resort = '';

    while ($data = $result->fetch_assoc()) {
        $post_type = $data['post_type'];
        $post_id = $data['post_id'];
        $post_title = $data['post_title'];
        $meta_id = $data['meta_id'];
        $meta_key = $data['meta_key'];
        $meta_value = $data['meta_value'];
        $post_name = $data['post_name'];

        // Output the data as a csv as long as the key doesn't start with an underscore
        if (!str_starts_with($meta_key, '_')) {
            fputcsv($fp, array($post_id, $post_title, $meta_id, $meta_key, $meta_value));
            $reporter->buildReports($resort, $meta_key, $meta_value, $post_id, $post_name);
        }

        // First time through
        if ($current_post_id == -1) {
            $current_post_id = $post_id;
            $resort = $post_title;
        }
        // Check if we have a new resort
        if ($post_id != $current_post_id) {
            $arrCheckFunctions['sc_resort']->runChecks($resort, $arrMetaKeys);
            $current_post_id = $post_id;
            $resort = $post_title;
            $arrMetaKeys = null;
        }
        $arrMetaKeys[$meta_key] = $data;
    }
    // Run the checks for the last resort
    $arrCheckFunctions['sc_resort']->runChecks($resort, $arrMetaKeys);

    fclose($fp);

    $consistencyReport = $reporter->getReport('Consistency - Issues By Resort');
    $consistencyReport->addReportData($issueLogger->getReportsByResort());

    // Create the Weather Report
    $weatherReport = $reporter->getReport('Report - Weather and Conditions');
    $weatherReport->addWeatherData($conn);

    // Set the Data for the report by Field
    $reportByResort = $reporter->getReport('Consistency - Issues By Resort');
    $reportByField = $reporter->getReport('Consistency - Issues By Field');
    $reportByField->addReportData($reportByResort->getReportData());

    $reporter->renderReports($twig);
    $reporter->buildIndexPage($twig);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Exception processing Resort Data: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

Utils::logger()->info('End of SCGB Website Data Checker', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED

