cat << EOF | mysql | sed -n '2,$p'  > /tmp/stu
show tables;
EOF

for i in $(cat /tmp/stu | sort -r )
do
	echo "drop table $i;"
done | mysql

cat << EOF | mysql
drop procedure if exists sp_update_wp_postmeta;
drop procedure if exists sp_copy_wp_postmeta_to_audit;
drop procedure if exists sp_delete_resort_section;
EOF

ssh scgb-data mysqldump --no-tablespaces wordpress > wordpress.sql
mysql < wordpress.sql
rm wordpress.sql
mysql < storedProcs.sql
