<?php

namespace SCGB;

use Exception;
use SCGB\Holiday\Utilities\Utils;


const arr05MetaKey = array('resort_rating_advanced', 'resort_rating_apres', 'resort_rating_beginner',
    'resort_rating_fast_lifts', 'resort_rating_intermediate', 'resort_rating_resort_charm', 'resort_rating_snow',
    'resort_rating_value');
const arrYNMeteKey = array('resort_freshtrack_holidays', 'resort_green_credentials',
    'resort_instructor_led_guiding_resort', 'resort_rep_resort', );


require_once (__DIR__ . '/vendor/autoload.php');

require_once (__DIR__ . '/Utilities/Utils.php');

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Start of the main processing
Utils::logger()->info('Started SCGB Website Data Checker', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Take a CSV, Parse it, and load it into the database
 */

// File specified in the first argument
$ratingsFile = $argv[1];
$defaultsFile = $argv[2];

// Open the file
//$fp = fopen($ratingsFile, 'r');
//
//// skip to the third line
//fgets($fp);
//fgets($fp);
//
//// In this line columns with content the names of the meta_keys
//$line = fgets($fp);
//$arrMetaKeys = explode(',', $line);
//
//// Now iterate through the rest of the file and pick out the columns we want
//$arrResults = array();
//while ($line = fgets($fp)) {
//    $arrData = explode(',', $line);
//    $post_id = null;
//    for ($i = 0; $i < count($arrMetaKeys)-1; $i++) {
//        $meta_key = $arrMetaKeys[$i];
//        $meta_value = $arrData[$i];
//        if ($meta_key == 'post_id') {
//            $post_id = $meta_value;
//        } elseif ($meta_key != '' && $post_id >= 0) {
//            if (in_array($meta_key, arr05MetaKey)) {
//                if (!in_array($meta_value, array('0', '1', '2', '3', '4', '5'))) {
//                    $meta_value = '0';
//                }
//            } elseif (in_array($meta_key, arrYNMeteKey)) {
//                if ($meta_value == 'P') {
//                    $meta_value = 'yes';
//                } else {
//                    $meta_value = 'no';
//                }
//            }
//
//            // Get rid of blanks - except for overall default
//            if ($post_id != 0 && $meta_value == '') {
//                continue;
//            }
//
//
//            if ($meta_key == 'resort_transfer_time_from_airport' || $meta_key == 'resort_ski_area_size') {
//                // Transfer time and ski area size should be a number - up to 1 decimal place.
//                // Strip off any characters not digits or a decimal point
//                $meta_value = preg_replace('/[^0-9.]/', '', $meta_value);
//            }
//
//            if (str_ends_with($meta_key,'component_resort_map_embed_code')) {
//                // strip leading and trailing quotes if they exist
//                if (str_starts_with($meta_value, '"')) {
//                    $meta_value = substr($meta_value, 1);
//                }
//                if (str_ends_with($meta_value, '"')) {
//                    $meta_value = substr($meta_value, 0, -1);
//                }
//
//                // Replace !!! with , and escape " or '
//                $meta_value = str_replace('!!!', ',', $meta_value);
//                $meta_value = str_replace('""', '"', $meta_value);
//                $meta_value = str_replace("'", "\'", $meta_value);
//            }
//            if (str_ends_with($meta_key, 'section_display_status')) {
//                if ($meta_value == 'P') {
//                    $meta_value = 'all';
//                } else {
//                    $meta_value = 'hide';
//                }
//            }
//
//            $social = false;
//            // Special treatment for social links
//            if (str_starts_with($meta_key, 'Social') and str_ends_with($meta_key, '0_button_link')
//                && $meta_value != '')
//            {
//                $meta_value = 'a:3:{s:5:"title";s:14:"facebook group";s:3:"url";s:' . strlen($meta_value) . ':"' . $meta_value . '";s:6:"target";s:6:"_blank";}';
//                $social = true;
//            }elseif (str_starts_with($meta_key, 'Social') && str_ends_with($meta_key, '1_button_link') && $meta_value != '')
//            {
//                $meta_value = 'a:3:{s:5:"title";s:12:"Ski Club App";s:3:"url";s:' . strlen($meta_value) . ':"' . $meta_value . '";s:6:"target";s:6:"_blank";}';
//            }
//
//            // More special treatment for social links IKON pass
//            if ($meta_key == 'resort_badge') {
//                if ($meta_value == 'P') {
//                    $meta_value = '629';
//                } else {
//                    $meta_value = '';
//                }
//            }
//            $arrResults[$post_id][$meta_key] = $meta_value;
//
//            if ($social) {
//                $arrResults[$post_id]['Social!section_display_status'] = 'all';
//            }
//        }
//    }
//}
//
try {
    $sql = "delete from scgb_wp_postmeta_defaults";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to delete from scgb_wp_postmeta_defaults: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
//
//try {
//    foreach ($arrResults as $post_id => $arrData) {
//        foreach($arrData as $meta_key => $meta_value) {
//            $sql = "insert into scgb_wp_postmeta_defaults (post_id, meta_key, meta_key_is_regex, decode, meta_value, enabled) values (" . $post_id . ", '" . $meta_key . "', 0, '', '" . $meta_value . "', 1)";
//            $stmt = $conn->prepare($sql);
//            $stmt->execute();
//        }
//    }
//} catch (Exception $e) {
//    Utils::logger()->emergency(
//        'Failed to insert into scgb_wp_postmeta_defaults: ' . $e->getMessage(),
//        array('file' => basename(__FILE__), 'line' => __LINE__)
//    );
//    die(1);
//}


// Open the file - first line has the meta_keys
$fp = fopen($defaultsFile, 'r');

// skip to the second line
fgets($fp);

while ($line = fgets($fp)) {
    $arrData = explode(',', $line);
    $post_id = $arrData[0];
    $meta_key = $arrData[1];
    $meta_value = $arrData[2];
    $meta_key_is_regex = $arrData[3];

    // strip line feed and carriage return from meta_key_is_regex
    $meta_key_is_regex = str_replace("\n", '', $meta_key_is_regex);
    $meta_key_is_regex = str_replace("\r", '', $meta_key_is_regex);

    if ($meta_key_is_regex == 'Y') {
        $meta_key_is_regex = 1;
    } else {
        $meta_key_is_regex = 0;
    }

    try {
        // Defaults first
        $sql = "insert into scgb_wp_postmeta_defaults (post_id, meta_key, meta_key_is_regex, decode, meta_value, enabled) values (" . $post_id . ", '" . $meta_key . "'," . $meta_key_is_regex . ", '', '" . $meta_value . "', 1)";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
    } catch (Exception $e) {
        Utils::logger()->emergency(
            'Failed to insert into scgb_wp_postmeta_defaults: ' . $e->getMessage(),
            array('file' => basename(__FILE__), 'line' => __LINE__)
        );
        die(1);
    }
}
// Now update the decodes
try {
    $sql = "update scgb_wp_postmeta_defaults d " .
        "join scgb_decodes dc " .
        "on d.meta_key = dc.field ".
        "set d.decode = dc.decode;";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update meaning in  scgb_wp_postmeta_defaults: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
