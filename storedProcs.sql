-- Mysql Stored Procedures supporting the data maintenance of the website data

-- Stored procedure called on first use
-- Copy the contents of the table wp_postmeta to the table scgb_postmeta_audit
-- This is a one time copy of the data
-- The table scgb_postmeta_audit is used to track changes to the wp_postmeta table

DROP PROCEDURE IF EXISTS sp_copy_wp_postmeta_to_audit;

DELIMITER //

CREATE PROCEDURE sp_copy_wp_postmeta_to_audit()
BEGIN
    DECLARE last_sequence int;
    START TRANSACTION;
    -- Check that the tables scgb_test_sequence and scgb_wp_postmeta_audit are empty
    IF (SELECT count(*) FROM scgb_wp_postmeta_audit) > 0 THEN
    BEGIN
        -- Can't continue if the table is not empty
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The table scgb_wp_postmeta_audit is not empty';
    END;
    END IF;

    IF (SELECT count(*) FROM scgb_test_sequence) > 0 THEN
    BEGIN
        -- Can't continue if the table is not empty
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The table scgb_test_sequence is not empty';
    END;
    END IF;

    INSERT INTO scgb_test_sequence (runtime) values(now());
    SET last_sequence = LAST_INSERT_ID();

    INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new)
    SELECT last_sequence, post_id, meta_key, null, meta_value
    FROM wp_postmeta m, wp_posts p
    where post_id = id
    and post_type = 'sc_resort';

    IF (SELECT count(*) FROM scgb_wp_postmeta_audit) = 0 THEN
    BEGIN
        -- Can't continue if the table is empty
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Initial population of scgb_wp_postmeta_audit failed';
    END;
    END IF;

    COMMIT;
END //

-- Stored procedure to change an entry in wp_postmeta
-- The change is recorded in the table scgb_wp_postmeta_audit
-- use of meta_id is belt and braces to ensure only one row is changed

DROP PROCEDURE IF EXISTS sp_update_wp_postmeta //

CREATE PROCEDURE sp_update_wp_postmeta(
    IN p_sequence INT,
    IN p_meta_id INT,
    IN p_post_id int,
    IN p_meta_key varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
    IN p_meta_value longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
)
BEGIN
    START TRANSACTION;

    INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new)
    SELECT p_sequence, p_post_id, p_meta_key, meta_value, p_meta_value
    FROM wp_postmeta
    WHERE post_id = p_post_id
    AND meta_key = p_meta_key
    AND meta_id = p_meta_id;

    -- Make Sure only one row changed
    IF (SELECT ROW_COUNT()) <> 1 THEN
    BEGIN
        -- Can't continue if the table is empty
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of scgb_wp_postmeta_audit failed - <> 1 row impacted';
    END;
    END IF;

    UPDATE wp_postmeta
    SET meta_value = p_meta_value
    WHERE post_id = p_post_id
    AND meta_key = p_meta_key
    AND meta_id = p_meta_id;

    -- Make Sure only one row changed
    IF (SELECT ROW_COUNT()) <> 1 THEN
    BEGIN
        -- Can't continue if the table is empty
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of wp_postmeta failed - <> 1 row impacted';
    END;
    END IF;

    COMMIT;
END //



-- Stored procedure to delete a section in wp_postmeta
-- The change is recorded in the table scgb_wp_postmeta_audit
-- use of meta_id is belt and braces to ensure only one row is changed

DROP PROCEDURE IF EXISTS sp_delete_resort_section //

CREATE PROCEDURE sp_delete_resort_section(
    IN p_sequence INT,
    IN p_post_id int,
    IN p_section varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
)
BEGIN
    START TRANSACTION;

    INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new)
    SELECT p_sequence, post_id, meta_key, meta_value, null
    FROM wp_postmeta
    WHERE post_id = p_post_id
    AND (meta_key like CONCAT('resort_tabs_',p_section,'_%') OR
	meta_key like CONCAT('_resort_tabs_',p_section,'_%'));

    -- Make sure rows have changed
    IF (SELECT ROW_COUNT()) = 0  THEN
    BEGIN
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of scgb_wp_postmeta_audit failed - 0 rows impacted';
    END;
    END IF;

    delete from  wp_postmeta
    WHERE post_id = p_post_id
    AND (meta_key like CONCAT('resort_tabs_',p_section,'_%') OR
	meta_key like CONCAT('_resort_tabs_',p_section,'_%'));

	select ROW_COUNT();

    -- Make Sure rows change
    IF (SELECT ROW_COUNT()) = 0 THEN
    BEGIN
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of wp_postmeta failed - 0 rows impacted';
    END;
    END IF;

    COMMIT;
END //



-- Stored procedure to delete a section in wp_postmeta
-- The change is recorded in the table scgb_wp_postmeta_audit
-- use of meta_id is belt and braces to ensure only one row is changed

DROP PROCEDURE IF EXISTS sp_delete_resort_section //

CREATE PROCEDURE sp_delete_resort_section(
    IN p_sequence INT,
    IN p_post_id int,
    IN p_section varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
)
BEGIN
    START TRANSACTION;

    INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new)
    SELECT p_sequence, post_id, meta_key, meta_value, null
    FROM wp_postmeta
    WHERE post_id = p_post_id
    AND (meta_key like CONCAT('resort_tabs_',p_section,'_%') OR
	meta_key like CONCAT('_resort_tabs_',p_section,'_%'));

    -- Make sure rows have changed
    IF (SELECT ROW_COUNT()) = 0  THEN
    BEGIN
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of scgb_wp_postmeta_audit failed - 0 rows impacted';
    END;
    END IF;

    delete from  wp_postmeta
    WHERE post_id = p_post_id
    AND (meta_key like CONCAT('resort_tabs_',p_section,'_%') OR
	meta_key like CONCAT('_resort_tabs_',p_section,'_%'));

	select ROW_COUNT();

    -- Make Sure rows change
    IF (SELECT ROW_COUNT()) = 0 THEN
    BEGIN
        ROLLBACK;
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Update of wp_postmeta failed - 0 rows impacted';
    END;
    END IF;

    COMMIT;
END //


DELIMITER ;
