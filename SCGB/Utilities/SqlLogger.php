<?php
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class SqlLogger
{
    private ?int $sequence = null;
    private string $sqlDir;

    /**
     * @throws Exception
     */
    public function __construct(mysqli $mysqli)
    {
        $this->sqlDir = Utils::getConfigItem('dirSQL');

        // delete any sql files in the sql and sql/dupes directory
        $files = glob($this->sqlDir . '/*.sql');
        foreach ($files as $file) {
            unlink($file);
        }$files = glob($this->sqlDir . '/dupes/*.sql');
        foreach ($files as $file) {
            unlink($file);
        }

        // Check for first run. if the tables scgb_test_sequence and scgb_wp_postmeta_audit
        // empty then run the stored procedure sp_copy_wp_postmeta_to_audit
        $sql = "select count(*) as count from scgb_test_sequence";
        $result = $mysqli->query($sql);
        $data = $result->fetch_assoc();
        $scgb_test_sequence_count = $data['count'];

        $sql = "select count(*) as count from scgb_wp_postmeta_audit";
        $result = $mysqli->query($sql);
        $data = $result->fetch_assoc();
        $scgb_wp_postmeta_audit_count = $data['count'];

        // If both 0 call the stored procedure. If one is 0 and the other isn't then there is a problem
        if ($scgb_test_sequence_count == 0 && $scgb_wp_postmeta_audit_count == 0) {
            // First run
            $sql = "CALL sp_copy_wp_postmeta_to_audit()";
            $mysqli->prepare($sql)->execute();
        } elseif (($scgb_test_sequence_count == 0 && $scgb_wp_postmeta_audit_count != 0) || ($scgb_test_sequence_count != 0 && $scgb_wp_postmeta_audit_count == 0)) {
            // One table is empty and the other isn't - this is a problem
            throw new Exception("One of the tables scgb_test_sequence and scgb_wp_postmeta_audit is empty and the other isn't");
        }

        // Get the next sequence number - start by inserting a row into scgb_test_sequence
        $sql = "insert into scgb_test_sequence (runtime) values(now())";
        $stmt = $mysqli->prepare($sql);
        $stmt->execute();

        // Get the sequence number
        $sql = "select max(sequence) as sequence from scgb_test_sequence";
        $result = $mysqli->query($sql);

        while ($data = $result->fetch_assoc()) {
            $this->sequence = intval($data['sequence']);
        }
    }

    public function getSequence() : int
    {
        return $this->sequence;
    }

    public function makeMetaChange(string $resort, int $meta_id, int $post_id, string $meta_key, string $new_meta_value) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($resort);

        // Construct the stored Procedure Call and write it to the file
        // escape any single quotes in the meta_value
        $new_meta_value = str_replace("'", "\\'", $new_meta_value);
        $sql = "CALL sp_update_wp_postmeta($this->sequence, $meta_id, $post_id, '$meta_key', '$new_meta_value');";
        fwrite($fp, $sql . "\n");
        fclose($fp);
    }

    public function makeMultipleMetaChanges(array $arrFixes, array $arrMetaKeys) : bool
    {
        $addedMetaKey = false;
        $post_id = intval($arrMetaKeys['resort_tabs']['post_id']);
        $post_name = $arrMetaKeys['resort_tabs']['post_name'];
        foreach($arrFixes as $meta_key => $meta_value) {
            if (!key_exists($meta_key, $arrMetaKeys)) {
                $this->addMetaKey($post_name, $post_id, $meta_key, $meta_value);
                $addedMetaKey = true;
                continue;
            }
            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
            if ($arrMetaKeys[$meta_key]['meta_value'] != $meta_value)
                $this->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $meta_value);
        }
        return $addedMetaKey;
    }

    public function updatePostSlug($slug, $post_id, string $valid_slug) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($slug);
        $sql = "update wp_posts " .
            "set post_name ='" . $valid_slug .
            "' where ID = " . $post_id .
            " and post_name = '" . $slug . "';";
        fwrite($fp, $sql . "\n");
        fclose($fp);
    }

    public function makePostChange(string $post_name, int $post_id, string $field, string $value) : void
    {
        // escape any single quotes in the value
        $value = str_replace("'", "\\'", $value);
        // Get a file handle
        $fp = $this->getFileHandleForResort($post_name);
        $sql = "update wp_posts " .
            "set " . $field . " ='" . $value .
            "' where ID = " . $post_id . ";";
        fwrite($fp, $sql . "\n");
        fclose($fp);
    }

    /**
     * @throws Exception
     */
    public function deleteDuplicateContent(string $meta_key, string $sha1, array $arrResorts) : void
    {
        $dir = Utils::getConfigItem('dirSQL') . '/dupes';
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $filename = $dir . '/' . $this->sequence . "-" . count($arrResorts) . '-' . $meta_key . '-' . $sha1 . '.sql';

        // get the lowest key from the array
        ksort($arrResorts);
        $post_id_lowest = 0;
        $post_id_string = '(';
        foreach ($arrResorts as $post_id => $data) {
            if ($post_id_lowest == 0) {
                $post_id_lowest = $post_id;
                continue;
            }
            if ($post_id < $post_id_lowest) {
                Utils::logger()->error("Post_id $post_id is less than $post_id_lowest");
                throw new Exception("Post_id $post_id is less than $post_id_lowest");
            }
            $post_id_string .= $post_id . ',';
        }

        // remove the last comma
        $post_id_string = substr($post_id_string, 0, -1) . ')';

        $sql = 'start transaction;' . "\n";
        $sql .= 'INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) ' .
            "SELECT " . $this->sequence . ", post_id, meta_key, meta_value, '' " .
            'FROM wp_postmeta ' .
            'where post_id in ' . $post_id_string . ' ' .
            'and meta_key = "' . $meta_key . '";' . "\n";
        $sql .= 'update wp_postmeta ' .
            'set meta_value = "" ' .
            'where post_id in ' . $post_id_string .
            'and meta_key = "' . $meta_key . '";' . "\n";
        $sql .= 'commit;' . "\n";
        $fp = fopen($filename, 'w');

        fwrite($fp, $sql);
        fclose($fp);
    }

    public function deleteSocialSection(int $post_id, $resort, array $arrMetaKeyTargets,
        $meta_key_num_sections, $meta_id_num_sections, $num_sections) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($resort);
        $sql = "CALL sp_update_wp_postmeta($this->sequence, $meta_id_num_sections, $post_id, '$meta_key_num_sections', '$num_sections');";
        fwrite($fp, $sql . "\n");

        foreach ($arrMetaKeyTargets as $meta_key => $data) {
            $meta_id = $data['meta_id'];
            $target_meta_key = $data['meta_key_new'];

            if ($meta_key == $target_meta_key) {
                // Nothing changing
                continue;
            }

            // Either way now changing the meta_key - either deleting or renaming it
            $sql = "CALL sp_update_wp_postmeta($this->sequence, $meta_id, $post_id, '$meta_key', null);";
            fwrite($fp, $sql . "\n");
            if ($target_meta_key === null) {
                //  deleting the meta_key
                $sql = "DELETE FROM wp_postmeta WHERE meta_id = $meta_id and meta_key = '$meta_key';";
            } else {
                // renaming the meta_key
                $sql = "UPDATE wp_postmeta SET meta_key = '$target_meta_key' WHERE meta_id = $meta_id and meta_key = '$meta_key';";
            }
            fwrite($fp, $sql . "\n");
        }
        fclose($fp);
    }



    private function getFileHandleForResort(string $resort)
    {
        // Change any spaces to underscores and / to -
        $resort = str_replace(' ', '_', $resort);
        $resort = str_replace('/', '-', $resort);
        return fopen($this->sqlDir . '/' . $this->sequence . "-" . $resort . '.sql', 'a');
    }

    public function addMetaKey(string $post_name, int $post_id, string $meta_key, string $meta_value) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($post_name);

        // Escape any " or ' in the meta_value
        $meta_value = str_replace('"', '\"', $meta_value);
        $meta_value = str_replace("'", "\'", $meta_value);

        $sql = 'start transaction;' . "\n";
        $sql .= 'INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) ' .
            "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', null, '" . $meta_value . "');\n";
        $sql .= 'INSERT INTO wp_postmeta (post_id, meta_key, meta_value) ' .
            'VALUES( ' . $post_id . ', "' . $meta_key . '", "' . $meta_value . '");' . "\n";
        $sql .= 'commit;' . "\n";

        fwrite($fp, $sql);
        fclose($fp);
    }
    public function updateTaxonomies(string $post_name, int $post_id, string $taxonomy, array $arrTaxonomies) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($post_name);

        $sql = 'start transaction;' . "\n";
        $sql .= 'DELETE FROM wp_term_relationships ' .
            'where object_id = ' . $post_id . ' ' .
            'and term_taxonomy_id in (select term_taxonomy_id from wp_term_taxonomy where taxonomy = "' . $taxonomy . '");' . "\n";
        foreach ($arrTaxonomies as $arrTerms) {
            $sql .= 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) ' .
                    'VALUES( ' . $post_id . ', ' . $arrTerms->getTaxonomyTermID() . ', 0);' . "\n";
        }
        $sql .= 'commit;' . "\n";

        fwrite($fp, $sql);
        fclose($fp);
    }

    public function addTabSection(string $post_name, int $post_id, array $arrMetaKeys, int $overview_position,
                                  string $tab_meta_keys_filename, int $insert_position) : void
    {
        // Get a file handle
        $fp = $this->getFileHandleForResort($post_name);

        // Steps:
        // Determine Meta ID's for everything at or above the current position
        // Delete those meta id's (and audit them)
        // Read the new meta keys from the file and insert them at the correct position (and audit them)
        // Reinsert the meta_keys deleted (and audit them)
        // Update the overview position - meta_key resort_tabs_0_sections
        $arrMetaKeysToDelete = array();
        $meta_start_string = 'resort_tabs_' . $overview_position . '_sections';

        $current_header=$arrMetaKeys[$meta_start_string]['meta_value'];
        $header_meta_id=intval($arrMetaKeys[$meta_start_string]['meta_id']);
        $num_tabs = intval(explode(':', $current_header)[1]);

        // Check not adding to the end!
        if ($insert_position < $num_tabs) {
            // Need to delete the meta_keys at or above the insert position
            foreach ($arrMetaKeys as $meta_key => $data) {
                // Need to include the meta_keys starting with _
                if (!preg_match('/' . $meta_start_string . '_([0-9]+)_/', $meta_key, $matches)) {
                    continue;
                }
                $tab_number = $matches[1];
                if ($tab_number < $insert_position) {
                    continue;
                }
                $arrMetaKeysToDelete[$meta_key] = $data;
            }
        }

        $sql = 'start transaction;' . "\n";
        // Delete the meta_keys at or above the insert position
        foreach ($arrMetaKeysToDelete as $meta_key => $data) {
            $meta_id = $data['meta_id'];
            $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', '" . $data['meta_value'] . "', null);\n";
            $sql .= "DELETE FROM wp_postmeta WHERE meta_id = " . $meta_id . " and post_id = " . $post_id . ";" . "\n";
        }

        // Read the new meta keys from the csv file and insert them at the correct position (and audit them)
        // The keys from the file will have {Overview} and {Tab} in them. Replace with the correct values
        // First line of the file will be the meta_tab we need for the overview position
        $fp_meta_keys = fopen($tab_meta_keys_filename, 'r');
        $meta_stub = fgets($fp_meta_keys);
        // strip off trailing \n
        $meta_stub = rtrim($meta_stub);
        while (($line = fgets($fp_meta_keys)) !== false) {
            $line = explode(',', $line);
            $overview_position = strval($overview_position);
            $insert_position = strval($insert_position);
            $meta_key = str_replace('{Overview}', $overview_position, $line[0]);
            $meta_key = str_replace('{Tab}', $insert_position, $meta_key);
            // Strip spaces and \n from the end of the meta_value
            if (!isset($line[1])) {
                $meta_value = '';
            } else {
                $meta_value = rtrim($line[1]);
                // Escape any ' in the meta_value
                $meta_value = str_replace("'", "\'", $meta_value);
            }
            $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', null, '" . $meta_value . "');\n";
            $sql .= "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) " .
                "VALUES( " . $post_id . ", '" . $meta_key . "', '" . $meta_value . "');" . "\n";
        }
        fclose($fp_meta_keys);

        // Reinsert the meta_keys deleted (and audit them)
        foreach ($arrMetaKeysToDelete as $meta_key => $data) {
            // Get the tab number and increment it
            $meta_key = preg_replace_callback('/_sections_([0-9]+)/', function ($matches) {
                return  '_sections_' . $matches[1] + 1;
            }, $meta_key);
            $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', null, '" . $data['meta_value'] . "');\n";
            $sql .= "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) " .
                "VALUES( " . $post_id . ", '" . $meta_key . "', '" . $data['meta_value'] . "');" . "\n";
        }

        // Update the overview position - meta_key resort_tabs_0_sections
        // The string starts with a:13:{i:0;s:28:"component_ski_club_offerings";i:1;s:25:"component_resort_overview";i:2;s:23:"component_pros_and_cons";
        $arrTabKeys = explode('"', $current_header);
        $arrTabKeys = array_slice($arrTabKeys, 1);

        $num_new_tabs = $num_tabs + 1;
        $section_string = 'a:' . $num_new_tabs . ':{';
        $offset = 0;
        for ($count = 0; $count < $num_tabs; $count++) {
            if ($count == $insert_position) {
                $section_string .= 'i:' . $count . ';s:' . strlen($meta_stub) . ':"' . $meta_stub . '";';
                $offset = 1;
            }
            $section_string .= 'i:' . ($count + $offset) . ';s:' . strlen($arrTabKeys[$count*2]) . ':"' . $arrTabKeys[$count*2] . '";';
        }
        $section_string .= '}';
        $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_start_string . "', '" . $current_header . "', '" . $section_string . "');\n";
        $sql .= "UPDATE wp_postmeta SET meta_value = '" . $section_string . "' WHERE meta_id = " . $header_meta_id . " and post_id = " . $post_id . ";" . "\n";

        // sql all done - commit then write to file
        $sql .= 'commit;' . "\n";
        fwrite($fp, $sql);
    }

    public function deleteUnusedSectionTabs(array $arrOldToNewPositions, array $arrMetaKeys) : void
    {
        $post_name = $arrMetaKeys['resort_tabs']['post_name'];
        $post_id = $arrMetaKeys['resort_tabs']['post_id'];
        $meta_value = $arrMetaKeys['resort_tabs']['meta_value'];
        $meta_id = $arrMetaKeys['resort_tabs']['meta_id'];

        // Work out how many tabs to keep
        $num_tabs_to_keep = 0;
        foreach($arrOldToNewPositions as $data)
        {
            if ($data['new_position'] != -1)
            {
                $num_tabs_to_keep++;
            }
        }

        $sql = 'start transaction;' . "\n";

        // update mata_key resort_tabs with $num_tabs_to_keep
        $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "VALUES (" . $this->sequence . ", " . $post_id . ", 'resort_tabs', '" . $meta_value . "', '" . $num_tabs_to_keep . "');\n";
        $sql .= "UPDATE wp_postmeta SET meta_value = '" . $num_tabs_to_keep . "' WHERE meta_key = 'resort_tabs' AND meta_id = " . $meta_id . " and post_id = " . $post_id . ";" . "\n";


        // Looking for meta_keys that start resort_tabs or _resort_tabs
        // then extract the first number and see where it goes
        foreach ($arrMetaKeys as $meta_key => $data) {
            if (!str_starts_with($meta_key, 'resort_tabs_')  && !str_starts_with($meta_key, '_resort_tabs_')) {
                continue;
            }
            preg_match('/resort_tabs_([0-9]+).*/', $meta_key, $matches);
            $tab_number = $matches[1];
            if (!key_exists($tab_number, $arrOldToNewPositions)) {
                Utils::logger()->error('Tab number ' . $tab_number . ' not found in arrOldToNewPositions for resort ' . $post_name . ' - skipping');
                return;
            }
            $new_position = $arrOldToNewPositions[$tab_number]['new_position'];
            if ($new_position == -1) {
                // Delete the meta_key
                // escape the single quotes
                $meta_value = str_replace("'", "\\'", $meta_value);

                $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                    "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', '" . $meta_value . "', null);\n";
                $sql .= "DELETE FROM wp_postmeta WHERE meta_key = '" . $meta_key . "' AND meta_id = " . $data['meta_id'] . " and post_id = " . $post_id . ";" . "\n";
            } elseif ($new_position != $tab_number) {
                $meta_value = $data['meta_value'];
                // escape the single quotes
                $meta_value = str_replace("'", "\\'", $meta_value);

                // Update the meta_key with the new tab number
                $new_meta_key = preg_replace('/resort_tabs_([0-9]+)/', 'resort_tabs_' . $new_position , $meta_key);

                // Update the database - Delete the old keys first
                $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                    "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', '" . $meta_value . "', null);\n";
                $sql .= "DELETE FROM wp_postmeta WHERE meta_key = '" . $meta_key . "' AND meta_id = " . $data['meta_id'] . " and post_id = " . $post_id . ";" . "\n";
                $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
                    "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $new_meta_key . "', null, '" . $meta_value . "');\n";
                $sql .= "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES (" . $post_id . ", '" . $new_meta_key . "', '" . $meta_value . "');\n";
            }
        }
        $sql .= 'commit;' . "\n";
        $fp = $this->getFileHandleForResort($post_name);
        fwrite($fp, $sql);
        fclose($fp);
    }
    public function deleteMetaKey(string $post_name, int $meta_id, int $post_id, string $meta_key, string $current_meta_value) : void
    {
        $sql = 'start transaction;' . "\n";
        $sql .= "INSERT INTO scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "VALUES (" . $this->sequence . ", " . $post_id . ", '" . $meta_key . "', '" . $current_meta_value . "', null);\n";
        $sql .= "DELETE FROM wp_postmeta WHERE meta_key = '" . $meta_key . "' AND meta_id = " . $meta_id . " and post_id = " . $post_id . ";" . "\n";
        $sql .= 'commit;' . "\n";
        $fp = $this->getFileHandleForResort($post_name);
        fwrite($fp, $sql);
        fclose($fp);
    }
}