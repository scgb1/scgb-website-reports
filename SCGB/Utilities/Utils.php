<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\BufferHandler;
use Monolog\Handler\Handler;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use mysqli;
use Redis;
use RedisException;

/**
 * Contains generic utilities used by other parts of the code.
 *
 * Generic utilities:
 * - logger() - returns the global logging variable
 * - curl - takes an optional token and calls the URL specified
 * - getTimeStampedFilename - takes the passed 'file' and prepends a timestamp on the basename
 * - setupConfig - part of the initialisation. Load the config file and then processes command line overrides
 * - getConfigItem - returns the config item specified
 * - getDataFileName - returns a config item which is file name as an absolute path (if not already absolute)
 * - getDBConnection - returns connection to the database
 */
class Utils
{
    private static mysqli $conn;
    private static ?Redis $redis = null;
    public function __construct()
    {
        return $this;
    }

    /**
     * Initialise the application. Setup logging, read the config file and process command line overrides.
     *
     * If an email address is specified in the config file, then an email handler is added to the logger. This will only
     * be used for ERROR level messages.
     *
     * @return Handler|null - the email handler if created, otherwise null. This used to flush at the end
     */
    static public function initialise(): Handler|null
    {
        // Basic initialisation, including setting up logging. Until this is completed, the code can only exit
        try {
            // Change default error handling to exit on warnings.
            set_error_handler(function ($errno, $errstr, $errfile, $errline) {
                // error suppressed with the @-operator
                if (0 === error_reporting()) {
                    return false;
                }
                self::logger()->emergency(
                    $errstr,
                    array(
                        'file' => $errfile,
                        'line' => $errline,
                    )
                );
                die(1);
            });

            // Read the config file and factor in overrides from the command line
            self::setupConfig();

            // Setup Logging. Start with logging to stdout. Logfile creation is handled by a shell script
            $handler = new StreamHandler("php://stdout", Level::fromName(self::getConfigItem('logLevel')));
            // the last “true” here tells it to remove empty [] from the end of the line
            $formatter = new LineFormatter(null, null, false, true);
            $handler->setFormatter($formatter);
            self::logger()->pushHandler($handler);
            $bulkHandler = null;

            // Add an email handler for ERROR (serious issues which should not occur)
            if ($errorEmail = self::getConfigItem('errorEmail', false)) {
                $handler = new NativeMailerHandler(
                    @$errorEmail,
                    'SkiClub Website Data Loader - Error',
                    'website@skiclub.co.uk',
                    Level::Error
                );

                $bulkHandler = new BufferHandler($handler);
                $bulkHandler->setFormatter($formatter);
                self::logger()->pushHandler($bulkHandler);
            }

            self::logger()->debug(
                'Config: ' . print_r($GLOBALS['config'], true),
                array(
                    'file' => basename(__FILE__),
                    'line' => __LINE__,
                )
            );

        } catch (Exception $e) {
            print('Failed to perform basic initialisation - Cannot continue: ' . $e->getMessage() . "\n");
            die(1);
        }
        return $bulkHandler;
    }

    /**
     * function curl - call the passed URL with a login token if necessary.
     *
     * @param $url
     * @param null $token - optional login token to add to the call
     * @return string
     * @throws Exception
     */
    static public function curl($url, $token = null): string
    {
        self::logger()->debug('curl URL: ' . $url,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_FAILONERROR => true,
            )
        );

        if ($token != null) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $token
            ));
        }

        // If we get an empty response then sleep for 5 seconds and try again
        // retry up to 5 times
        for ($i = 0; $i < 5; $i++) {
            $response = curl_exec($curl);
            if ($response == '') {
                $sleepTime = 5 * ($i + 1);
                self::logger()->warning(
                    'Empty Response - retrying',
                    array(
                        'sleepTime' => $sleepTime,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
                sleep($sleepTime);
            } else {
                break;
            }
        }
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
            self::logger()->emergency('curl error: ' . curl_errno($curl) . "/" . $error_msg,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            throw new Exception('curl error: ' . curl_errno($curl) . '/' . $error_msg);
        }

        curl_close($curl);

        return $response;
    }

    /**
     * Returns a timestamped filename and creates the target directory if it doesn't exist.
     *
     * @param string $basename - the filename to timestamp
     * @return string
     */
    static public function getTimeStampedFilename(string $basename): string
    {
        $dirname = dirname($basename);
        $filename = basename($basename);
        if (!file_exists($dirname)) {
            mkdir($dirname, 0700, true);
        }

        // Add a timestamp to filename
        $filename = date('Ymd.His') . '-' . $filename;
        return ($dirname . '/' . $filename);
    }

    static public function logger()
    {
        // If the logger not already set up, do it now
        if (!isset($GLOBALS['log'])) {
            $GLOBALS['log'] = new Logger('SCGB');
        }
        return $GLOBALS['log'];
    }

    /**
     * Get the default config items (from config.php) and override with any set on the command line.
     *
     * @return void
     */
    static public function setupConfig(): void
    {
        $configFile = dirname($_SERVER['SCRIPT_NAME']) . '/config.php';

        // Check the config file exists
        if (!file_exists($configFile)) {
            print("Config file not found: " . $configFile . ".  Can't continue\n");
            die(1);
        }

        $GLOBALS['config'] = include __DIR__ . "/../../" . 'config.php';

        $argc = $_SERVER['argc'];
        $argv = $_SERVER['argv'];

        // Now iterated through the command line options and override any config items
        for ($i = 1; $i < $argc; $i++) {
            $option = $argv[$i];
            if (str_starts_with($option, '--')) {
                $option = substr($option, 2);
                $optionParts = explode('=', $option);
                $optionName = $optionParts[0];
                $optionValue = $optionParts[1];
                if ($optionValue == "") {
                    $optionValue = null;
                }
                $GLOBALS['config'][$optionName] = $optionValue;
            }
        }
    }

    /**
     * Function to get a config item and handle missing ones gracefully.
     *
     * All options can be specified or overridden from the command line
     *
     * @param string $configItem - the name of the config item to retrieve
     * @param bool $mandatory - whether the config item is mandatory or not. Defaults true
     * @throws Exception
     */
    static public function getConfigItem(string $configItem, bool $mandatory = true)
    {
        $config = $GLOBALS['config'];
        if (isset($config[$configItem])) {
            return $config[$configItem];
        } else {
            if (!$mandatory) {
                return null;
            }
            $GLOBALS['log']->error('Missing config item: ' . $configItem, array('process' => basename(__FILE__)));
            throw new Exception('Missing config item: ' . $configItem);
        }
    }

    /**
     * Function to get a config item which is a file name and return it as an absolute path.
     *
     * @param string $configItem - the name of the config item to retrieve
     * @param bool $mandatory - whether the config item is mandatory or not. Defaults true
     * @return string|null
     * @throws Exception
     */
    static public function getDataFileName(string $configItem, bool $mandatory = true): string|null
    {
        $filename = self::getConfigItem($configItem, $mandatory);
        if ($filename != null) {
            // If $filename doesn't start with a / then it is relative to the data directory
            if (!str_starts_with($filename, '/')) {
                $filename = dirname($_SERVER['argv'][0]) . "Utils.php/" . $filename;
            }
        }
        return $filename;
    }

    /**
     * Function to get a database connection.
     *
     * @return mysqli
     * @throws Exception
     */
    static public function getDBConnection(): mysqli
    {
        $servername = self::getConfigItem('databaseServer');
        $username = self::getConfigItem('databaseUser');
        $password = self::getConfigItem('databasePassword');
        $db = self::getConfigItem('databaseName');

        // Connect to the database
        self::logger()->debug(
            'Connecting to database',
            array(
                'databaseServer' => $servername,
                'databaseName' => $db,
                'databaseUser' => $username,
                'databasePassword' => 'SECRET',
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        self::$conn = new mysqli($servername, $username, $password);
        if (self::$conn->connect_error) {
            die("Connection failed: " . self::$conn->connect_error);
        }

        self::$conn->select_db($db);
        if (self::$conn->connect_error) {
            die("Connection failed: " . self::$conn->connect_error);
        }
        return self::$conn;
    }

    public static function constructWPURLString(array $data) : string
    {
        // string of the form a:3:{s:5:"title";s:14:"Learn more ...";s:3:"url";s:18:"?tab=accommodation";s:6:"target";s:0:"";}
        $string = "a:" . count($data) . ":{";
        foreach ($data as $key => $value) {
            $key=strval($key);
            $value=strval($value);
            $string .= "s:" . strlen($key) . ":\"" . $key . "\";";
            $string .= "s:" . strlen($value) . ":\"" . $value . "\";";
        }
        $string .= "}";
        return $string;
    }

    /**
     */
    public static function flushRedisCacheForPost(int $post_id) : void
    {
        // Redis server is running of the local host
        try {
            if (self::$redis == null) {
                self::$redis = new Redis();
                self::$redis->connect('localhost');
            }

            // get keys ending in post_id and delete them
            $keys = self::$redis->keys('*' . $post_id);
            foreach ($keys as $key) {
                self::$redis->del($key);
            }
        } catch (RedisException $e) {
            Utils::logger()->emergency(
                'Failed to talk to Redis: ' . $e->getMessage(),
                array('file' => basename(__FILE__), 'line' => __LINE__)
            );
            // NOT FATAL
        }
    }

    /**
     * Confirm that the passed value is of the type specified else return null.
     *
     * @param mixed $value
     * @param string $type
     * @return string|int|null|DateTime
     */
    static public function checkType(mixed $value, string $type): string|int|null|DateTime
    {
        if (is_array($value)) {
            return null;
        }
        if ($type == 'int') {
            try {
                return intval($value);
            } catch (Exception) {
                return null;
            }
        } elseif ($type == 'string') {
            if (is_string($value)) {
                return $value;
            } else {
                return null;
            }
        } elseif ($type == 'date') {
            try {
                return new DateTime($value);
            } catch (Exception) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function createWPPostString(array $posts) : string
    {
        // string of the form a:3:{i:0;s:4:"1234";i:1;s:4:"5678";i:2;s:4:"9012";}
        $string = "a:" . count($posts) . ":{";
        $count = 0;
        foreach ($posts as $post) {
            $post=strval($post);
            $string .= "i:" . $count++ . ";";
            $string .= "s:" . strlen($post) . ":\"" . $post . "\";";
        }
        $string .= "}";
        return $string;
    }

    // get count random entries from the source array
    public static function getRandomEntries(array $sourceArray, int $count) : array
    {
        $randomEntries = array();
        $sourceCount = count($sourceArray);
        if ($sourceCount <= $count) {
            $randomEntries = $sourceArray;
        } else {
            $randomKeys = array_rand($sourceArray, $count);
            foreach ($randomKeys as $key) {
                $randomEntries[] = $sourceArray[$key];
            }
        }
        return $randomEntries;
    }
    /**
     * @throws Exception
     */
    public static function updateMetaValue(int $post_id, int $meta_id, string $meta_key, string $meta_value) : void
    {
        // escape any single quotes in the meta_value
        $meta_value = str_replace("'", "\\'", $meta_value);
        $sql = "UPDATE wp_postmeta " .
            "SET meta_value = '" . $meta_value . "' " .
            "WHERE meta_id = " . $meta_id . " " .
            "AND meta_key = '" . $meta_key . "' " .
            "AND post_id = " . $post_id;
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to update meta value: ' . self::$conn->error);
        }
        Utils::flushRedisCacheForPost($post_id);
    }
    /**
     * returns a date as long as the parameter is a string or a DateTime object
     *
     * @param $dtm - the date time to be checked
     * @throws Exception
     */
    static public function getDateTime($dtm): DateTime|null
    {
        if (is_string($dtm)) {
            return new DateTime($dtm);
        } elseif (is_object($dtm) && get_class($dtm) == 'DateTime') {
            return $dtm;
        } else {
            return null;
        }
    }

    static public function create_post_name($resort): string
    {
        // Slug Created as follows:
        // Replace unicode characters with ascii equivalents
        //  - Lower case
        //  - 1 or mode spaces to hyphen
        //  - Remove apostrophes
        //  - Remove ampersands
        //  - Remove brackets
        //  - Remove multiple hyphens
        //  - convert / to -
        $resort = self::remove_accents($resort);
        $resort = iconv('UTF-8', 'ASCII//TRANSLIT', $resort);
        $resort = strtolower($resort);
        $resort = preg_replace('/\s+/', '-', $resort);
        $resort = str_replace("'", '', $resort);
        $resort = str_replace("&", '', $resort);
        $resort = str_replace("(", '', $resort);
        $resort = str_replace(")", '', $resort);
        $resort = str_replace("/", '-', $resort);
        return preg_replace('/-+/', '-', $resort);
    }

    static private function remove_accents($string)
    {
        if (!preg_match('/[\x80-\xff]/', $string))
            return $string;

        $chars = array(
            // Decompositions for Latin-1 Supplement
            // Decompositions for Latin-1 Supplement
            chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
            chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
            chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
            chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
            chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
            chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
            chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
            chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
            chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
            chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
            chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
            chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
            chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
            chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
            chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
            chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
            chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
            chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
            chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
            chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
            chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
            chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
            chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
            chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
            chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
            chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
            chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
            chr(195) . chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
            chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
            chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
            chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
            chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
            chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
            chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
            chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
            chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
            chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
            chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
            chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
            chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
            chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
            chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
            chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
            chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
            chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
            chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
            chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
            chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
            chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
            chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
            chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
            chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
            chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
            chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
            chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
            chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
            chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
            chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
            chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
            chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
            chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
            chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
            chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
            chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
            chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
            chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
            chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
            chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
            chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
            chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
            chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
            chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
            chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
            chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
            chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
            chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
            chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
            chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
            chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
            chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
            chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
            chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
            chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
            chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
            chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
            chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
            chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
            chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
            chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
            chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
            chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's'
        );

        return strtr($string, $chars);
    }
}
