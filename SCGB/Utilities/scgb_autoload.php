<?php

/**
 * Simple Autoloader for SCGB Classes … Just search down the directory tree for the class file
 * ignore the directory vendor
 */

spl_autoload_register('scgb_autoload');
function scgb_autoload($class_name) : void
{
    // Start at the script directory
    $dir = dirname($_SERVER['SCRIPT_NAME']) . '/SCGB';

    // Split the class name into its parts
    $arrParts = explode('\\', $class_name);

    if (count($arrParts) != 2 || $arrParts[0] != 'SCGB') {
        return;
    }

    $file = $arrParts[1] . '.php';
    $search = $dir . '/' . $file;
    if (file_exists($search)) {
        require_once $search;
        return;
    }
    // Find all the directories in the current directory and search them
    // TODO: make this recursive
    $arrDirs = array_filter(glob($dir . '/*'), 'is_dir');
    foreach ($arrDirs as $dir) {
        $search = $dir . '/' . $file;
        if (file_exists($search)) {
            require_once $search;
            return;
        }
    }
}