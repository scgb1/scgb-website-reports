<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

/**
 * Class IssueLogger
 * @package SCGB
 *
 * Collect the results of the checks
 */
class IssueLogger
{
    private array $arrReportsByResort;
    //private array $arrReportsByField;

    public function __construct()
    {
        $this->arrReportsByResort = array();
        //$this->arrReportsByCheck = array();
        return $this;
    }

    //  Getters
    public function getReportsByResort() : array
    {
        return $this->arrReportsByResort;
    }

//    public function getReportsByCheck() : array
//    {
//        return $this->arrReportsByCheck;
//    }

    /**
     * Log an issue for later reporting.
     *
     * @param string $resort
     * @param string $field
     * @param string $check
     * @param string $issue
     * @param string $currentValue
     * @param array $data
     * @throws Exception
     */
    public function report(string $resort, string $field, string $check, string $issue, string $currentValue, array $data) : void
    {
        // Add the resort to the array if it doesn't already exist
        if (key_exists('post_id', $data)) {
            $post_id = $data['post_id'];
        } else if (key_exists('ID', $data)) {
            $post_id = $data['ID'];
        } else {
            Utils::logger()->error('No post_id or ID in data array', $data);
            throw new Exception('No post_id or ID in data array for ' . $resort . ' ' . $field . ' ' . $check . ' ' . $issue);
        }

        if (!array_key_exists($resort, $this->arrReportsByResort)) {
            $this->arrReportsByResort[$resort] = array(
                'name' => $resort, 'post_name' => $data['post_name'], 'post_id' => $post_id, 'fields' => array());
        }
        // Add the field to the array if it doesn't already exist
//        if (!array_key_exists($field, $this->arrReportsByResort[$resort]['fields'])) {
//            $this->arrReportsByResort[$resort]['fields'][$field] = array('name' => $field, 'issues' => array());
//        }

        // Add the issue to the arrays
        $this->arrReportsByResort[$resort]['fields'][$field][] = $issue;
    }
}