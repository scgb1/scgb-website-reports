<?php
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class Resort extends Post
{
    private static array $arrResortMeta = [];
    private static array $arrFreshTracksResorts = [];
    private static array $arrRepResorts = [];
    private static mixed $arrResortElevation;


    public function __construct(string $post_title)
    {
        parent::__construct($post_title);
        return $this;
    }

    public function getResortElevation(bool $upper) : ?int
    {
        $elevation = null;
        if (key_exists('resort_weather_resort_api_id', self::$arrResortMeta[$this->post_id]))
        {
            $api_id = self::$arrResortMeta[$this->post_id]['resort_weather_resort_api_id']['meta_value'];
            if (key_exists($api_id, self::$arrResortElevation))
            {
                $elevation = self::$arrResortElevation[$api_id][$upper ? 'upper' : 'lower'];
            }
        }
        return $elevation;
    }

    public static function getMetaValue(int $post_id, string $meta_key): string | null
    {
        $meta_value = null;
        if (key_exists($post_id, self::$arrResortMeta)) {
            if (key_exists($meta_key, self::$arrResortMeta[$post_id])) {
                $meta_value = self::$arrResortMeta[$post_id][$meta_key]['meta_value'];
            }
        }
        return $meta_value;
    }

    public static function isValidResort(int $post_id): bool
    {
        $isValid = false;
        if (key_exists($post_id, self::$arrPostDataByPostID)) {
            $isValid = true;
        }
        return $isValid;
    }
    public static function getResortPostName(int $post_id): ?string
    {
        $post_name = null;
        if (key_exists($post_id, self::$arrPostDataByPostID)) {
            $post_name = self::$arrPostDataByPostID[$post_id]['post_name'];
        }
        return $post_name;
    }

    /**
     * @throws Exception
     */
    public static function initialise(mysqli $conn): void
    {
        self::$conn = $conn;
        $sql = "SELECT post_id, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta " .
            "WHERE post_id in ( " .
                "SELECT ID " .
                "FROM wp_posts " .
                "WHERE post_type = 'sc_resort' " .
                "AND post_status = 'publish' ) " .
            "ORDER BY post_id, meta_key";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load resort meta data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['post_id']);
            $meta_key = trim($row['meta_key']);
            $meta_value = trim($row['meta_value']);
            $meta_id= intval($row['meta_id']);
            if (!key_exists($post_id, self::$arrResortMeta)) {
                self::$arrResortMeta[$post_id] = array();
            }
            self::$arrResortMeta[$post_id][$meta_key] = array('meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        foreach (self::$arrResortMeta as $post_id => $resortMeta) {
            if (key_exists('resort_freshtrack_holidays', $resortMeta) && $resortMeta['resort_freshtrack_holidays']['meta_value'] == 'yes') {
                self::$arrFreshTracksResorts[$post_id] = $post_id;
            }
            if (key_exists('resort_rep_resort', $resortMeta) && $resortMeta['resort_rep_resort']['meta_value'] == 'yes') {
                self::$arrRepResorts[$post_id] = $post_id;
            }
        }

        $sql = "SELECT ID, post_title, post_name " .
            "FROM wp_posts " .
            "WHERE post_type = 'sc_resort' " .
            "AND post_status = 'publish' " .
            "ORDER BY post_title";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load resort post data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['ID']);
            $post_title = trim($row['post_title']);
            $post_name = trim($row['post_name']);

            self::$arrPostDataByPostID[$post_id]['post_title'] = $post_title;
            self::$arrPostDataByPostID[$post_id]['post_name'] = $post_name;
            self::$arrPostDataByPostName[$post_name]['post_id'] = $post_id;
            self::$arrPostDataByPostName[$post_name]['post_title'] = $post_title;
            self::$arrPostDataByPostTitle[$post_title]['post_id'] = $post_id;
            self::$arrPostDataByPostTitle[$post_title]['post_title'] = $post_name;
        }

        // Get elevation data from the table scgb_resort_data
        $sql = "SELECT intSCGBResortID, intLowerWeatherStationElevation, intUpperWeatherStationElevation " .
            "FROM scgb_resort_data";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load resort data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $scgb_resort_id = intval($row['intSCGBResortID']);
            $lowerElevation = intval($row['intLowerWeatherStationElevation']);
            $upperElevation = intval($row['intUpperWeatherStationElevation']);
            self::$arrResortElevation[$scgb_resort_id] = array('lower' => $lowerElevation, 'upper' => $upperElevation);
        }
    }

    /**
     * We want up to 2 resorts which have holidays or reps and then the rest filled with a random selection
     * from the same continent
     *
     * Also exclude holiday or rep resorts not on the same continent
     *
     * @throws Exception
     */

    public static function updateResortsYouMayLike() : void
    {
        $arrResortsPerContinent = array();
        foreach (self::$arrResortMeta as $post_id => $resortMeta)
        {
            if (!key_exists('sections_0_component_you_may_also_like_posts', $resortMeta)) {
                continue;
            }
            $country = CountryTaxonomy::getCountriesByPostID($post_id, self::$conn, true);
            $continent = $country->getParent()->getName();
            if (!key_exists($continent, $arrResortsPerContinent))
            {
                $arrResortsPerContinent[$continent] = CountryTaxonomy::getPostsOnTheSameContinent($post_id);
            }
            $arrPool = array_unique(array_merge(self::$arrFreshTracksResorts, self::$arrRepResorts));
            // Remove any resorts not on the same continent or if we find the current resort
            foreach ($arrPool as $id)
            {
                $country = CountryTaxonomy::getCountriesByPostID($id, self::$conn, true);
                $resortContinent = $country->getParent()->getName();
                if ($resortContinent != $continent || $id == $post_id)
                {
                    unset($arrPool[$id]);
                }
            }
            $arrResortsToLike = Utils::getRandomEntries($arrPool, 2);
            $done = 0;
            while ($done++ < 100) {
                $arrLikedResorts = array_merge($arrResortsToLike,
                    Utils::getRandomEntries($arrResortsPerContinent[$continent], 4-count($arrResortsToLike)));
                // If this resort is in the list then go round again
                if (key_exists($post_id, $arrLikedResorts))
                    continue;
                // Look for dupes
                foreach ($arrLikedResorts as $id)
                {
                    if (count(array_keys($arrLikedResorts, $id)) > 1)
                        continue 2;
                }
                break;
            }

            shuffle($arrLikedResorts);
            $posts_to_like_meta_id = $resortMeta['sections_0_component_you_may_also_like_posts']['meta_id'];
            $posts_to_like_meta_value = Utils::createWPPostString($arrLikedResorts);
            Utils::updateMetaValue($post_id, $posts_to_like_meta_id,
                'sections_0_component_you_may_also_like_posts',$posts_to_like_meta_value);

            // Check the section is visible
            if ($resortMeta['sections_0_section_display_status']['meta_value'] == 'hide')
            {
                $meta_id = $resortMeta['sections_0_section_display_status']['meta_id'];
                Utils::updateMetaValue($post_id, $meta_id,
                    'sections_0_section_display_status', 'show');
            }
            // Check the button pointing the correct place
            $url = 'a:3:{s:5:"title";s:16:"View all resorts";s:3:"url";s:8:"/resorts";s:6:"target";s:0:"";}';
            if ($resortMeta['sections_0_component_you_may_also_like_view_all_button']['meta_value'] != $url)
            {
                $meta_id = $resortMeta['sections_0_component_you_may_also_like_view_all_button']['meta_id'];
                Utils::updateMetaValue($post_id, $meta_id,
                    'sections_0_component_you_may_also_like_view_all_button', $url);
            }
        }
    }
}