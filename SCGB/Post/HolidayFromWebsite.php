<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use mysqli;

class HolidayFromWebsite extends Holiday
{
    private array $arrSugatiTourIDs= [];
    protected static array $arrWebHolidaysByName = [];
    protected array $holidayMeta = [];
    protected ?PostDataDictionary $postDataDictionary;
    protected ?array $skiStandardTaxonomies = [];

    protected ?Taxonomy $countryTaxonomy = null;
    protected ?Taxonomy $holidayTypeTaxonomy = null;


    /**
     * @throws Exception
     */
    public function __construct(string $holiday, string $post_id, array $holidayMeta, ?PostDataDictionary $holidayDataDictionary)
    {
        parent::__construct($holiday);

        // Check post_id is an integer
        if (!is_numeric($post_id)) {
            throw new Exception('post_id is not numeric: ' . $post_id);
        }
        $post_id = intval($post_id);
        $this->holidayName = $holiday;
        $this->post_id = $post_id;
        $this->holidayMeta = $holidayMeta;
        $this->postDataDictionary = $holidayDataDictionary;

        foreach ($holidayMeta as $meta_key => $data) {
            $meta_value = $data['meta_value'];
            if ($meta_key == 'holiday_resorts') {
                if ($meta_value != '') {
                    // extract the first resort from the list
                    $arrResortPostIDs = explode('"', $meta_value);
                    // if no " then badly formatted data so blank it, so it can be fixed
                    if (count($arrResortPostIDs) >= 2) {
                        $this->resort_post_id = intval($arrResortPostIDs[1]);
                        $this->resort_post_id_string = $meta_value;
                    }
                }
            } elseif ($meta_key == 'holiday_tabs_0_sections_0_component_holiday_intro_text') {
                $this->introText = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_0_component_holiday_trip_highlights') {
                $this->tripHighlights = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_maximum_text') {
                $this->maxStandard = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_minimum_text') {
                $this->minStandard = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_heading_content') {
                $this->skiStandard = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_1_component_pros_and_cons_cons') {
                $this->whatsNotIncluded = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_1_component_pros_and_cons_pros') {
                $this->whatsIncluded = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_0_sections_2_component_central_text_content') {
                $this->onSnow = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_3_sections_0_component_central_text_content') {
                $this->hotelContent = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_3_sections_0_heading') {
                $this->hotelName = $meta_value;
            } elseif ($meta_key == 'holiday_accommodation') {
                $this->accommodationHeader = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_3_sections_2_component_map_content') {
                $this->hotelAddress = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_3_sections_2_component_map_map_url') {
                $this->mapURL = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_4_sections_0_component_central_text_content') {
                $this->travelContent = $meta_value;
            } elseif ($meta_key == 'holiday_tabs_4_sections_0_heading') {
                $this->travelHeading = $meta_value;
            } elseif ($meta_key == 'holiday_tour_id_in_sugati') {
                for ($i=0; $i<intval($meta_value); $i++) {
                    $this->arrSugatiTourIDs[] = $holidayMeta['holiday_tour_id_in_sugati_' . $i . '_id']['meta_value'];
                }
            } elseif ($meta_key == 'holiday_departure_months') {
                $this->processWPDepartureString($meta_value);
            }
        }
        // now do the guides
        for ($i=0; $i<10; $i++) {
            if (!key_exists('holiday_tabs_0_sections_2_component_reps_and_istructors_reps_and_istructors_' . $i . '_position', $holidayMeta)) {
                break;
            }
            $position = $holidayMeta['holiday_tabs_0_sections_2_component_reps_and_istructors_reps_and_istructors_' . $i . '_position'];
            $photo = $holidayMeta['holiday_tabs_0_sections_2_component_reps_and_istructors_reps_and_istructors_' . $i . '_photo'];
            $content = $holidayMeta['holiday_tabs_0_sections_2_component_reps_and_istructors_reps_and_istructors_' . $i . '_content'];
            $this->guideDetails[] = array(
                'position' => $position,
                'photo' => $photo,
                'content' => $content
            );
        }
        // And finish with the questions
        for ($i=0; $i<10; $i++) {
            if (!key_exists('holiday_tabs_5_sections_0_component_full_width_faq_faqs_' . $i . '_question', $holidayMeta)) {
                break;
            }
            $question = $holidayMeta['holiday_tabs_5_sections_0_component_full_width_faq_faqs_' . $i . '_question'];
            $answer = $holidayMeta['holiday_tabs_5_sections_0_component_full_width_faq_faqs_' . $i . '_answer'];
            $this->questions[] = array(
                'question' => $question,
                'answer' => $answer
            );
        }
        $this->skiStandardTaxonomies = StandardTaxonomy::getSkiStandards($this->post_id, self::$conn);
        $this->countryTaxonomy = CountryTaxonomy::getCountriesByPostID($this->post_id, self::$conn, true);
        $this->holidayTypeTaxonomy = HolidayTypeTaxonomy::getHolidayTypeByPostID($this->post_id, self::$conn);
        return $this;
    }

    public function getPostID() : int
    {
        return $this->post_id;
    }
    public function getHolidayByName(string $holidayName): ?HolidayFromWebsite
    {
        return self::$arrWebHolidaysByName[$holidayName] ?? null;
    }
    public function getSugatiTourIDs() : array
    {
        return $this->arrSugatiTourIDs;
    }

    /**
     * @throws Exception
     */
    public function checkForUpdatesFromFile(HolidayFromFile $holidayFile) : void
    {
        // Big Function - check through each attribute of SkiTest and update if necessary
        $this->checkForFieldChange($this->introText, $holidayFile->introText, 'Intro Text');
        $this->checkForFieldChange($this->resort_post_id_string, $holidayFile->resort_post_id_string, 'Resort');
        $this->checkForFieldChange($this->tripHighlights, $holidayFile->tripHighlights, 'Trip Highlights');
        $this->checkForFieldChange($this->skiStandard, $holidayFile->skiStandard, 'Ski Standard');
        $this->checkForFieldChange($this->maxStandard, $holidayFile->maxStandard, 'Max');
        $this->checkForFieldChange($this->minStandard, $holidayFile->minStandard, 'Min');
        $this->checkForFieldChange($this->onSnow, $holidayFile->onSnow, 'On Snow Experience');
        $this->checkForFieldChange($this->whatsIncluded, $holidayFile->whatsIncluded, 'Whats included');
        $this->checkForFieldChange($this->whatsNotIncluded, $holidayFile->whatsNotIncluded, 'Whats not included');
        $this->checkForFieldChange($this->hotelName, $holidayFile->hotelName, 'Hotel Name');
        $this->checkForFieldChange($this->accommodationHeader, $holidayFile->accommodationHeader, 'holiday_accommodation', true);
        $this->checkForFieldChange($this->hotelContent, $holidayFile->hotelContent, 'Hotel Content');
        $this->checkForFieldChange($this->hotelAddress, $holidayFile->hotelAddress, 'Hotel Address');
        $this->checkForFieldChange($this->mapURL, $holidayFile->mapURL, 'Map URL');
        $this->checkForFieldChange($this->travelHeading, $holidayFile->travelHeading, 'Travel Heading');
        $this->checkForFieldChange($this->travelContent, $holidayFile->travelContent, 'Travel Content');

        // Now do the Guides - we will be lazy - if the array sizes the same then iterate through
        // else delete all the guides and add the new ones
        $this->post_name = array_pop($this->holidayMeta)['post_name'];

        $post_id = $this->post_id;
        $i = 0;
        if ($holidayFile->guideDetails !== null) {
            if (count($this->guideDetails) == count($holidayFile->guideDetails)) {
                foreach ($this->guideDetails as $guide) {
                    $this->checkForFieldChange($guide['position'], $holidayFile->guideDetails[$i]['position'], 'Guide Position ' . $i);
                    $this->checkForFieldChange($guide['content'], $holidayFile->guideDetails[$i]['content'], 'Guide Content ' . $i);
                    $this->checkForFieldChange($guide['photo'], $holidayFile->guideDetails[$i]['photo'], 'Guide Photo ' . $i);
                    $i++;
                }
            } else {
                foreach ($this->guideDetails as $guide) {
                    $this->deleteMetaKey('Guide Position', $guide, 'position', $i);
                    $this->deleteMetaKey('Guide Content', $guide, 'content', $i);
                    $this->deleteMetaKey('Guide Photo', $guide, 'photo', $i);
                    $i++;
                }
                // Now add the new ones
                $i = 0;
                $meta_key = $this->postDataDictionary->getMetaKeyFromName('Guide Count');
                $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
                $meta_value = strval(count($holidayFile->guideDetails));
                self::$sqlLogger->makeMetaChange($this->post_name, $meta_id, $post_id, $meta_key, $meta_value);
                foreach ($holidayFile->guideDetails as $guide) {
                    $this->addMetaKey('Guide Position', $guide, 'position', $i);
                    $this->addMetaKey('Guide Content', $guide, 'content', $i);
                    $this->addMetaKey('Guide Photo', $guide, 'photo', $i);
                    $i++;
                }
            }
        }

        // Do the FAQ - Same as Guides
        if ($holidayFile->questions !== null) {
            $i = 0;
            if ($this->questions !== null && count($this->questions) == count($holidayFile->questions)) {
                foreach ($this->questions as $faq) {
                    $this->checkForFieldChange($faq['question']['meta_value'], $holidayFile->questions[$i]['question'],
                        $this->postDataDictionary->getMetaKeyFromName('Question', $i), true);
                    $this->checkForFieldChange($faq['answer']['meta_value'], $holidayFile->questions[$i]['answer'],
                        $this->postDataDictionary->getMetaKeyFromName('Answer', $i), true);
                    $i++;
                }
            } else {
                if ($this->questions !== null) {
                    $meta_key = $this->postDataDictionary->getMetaKeyFromName('FAQ Count');
                    $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
                    $meta_value = strval(count($holidayFile->questions));
                    $old_meta_value = $this->holidayMeta[$meta_key]['meta_value'];
                    if ($meta_value != $old_meta_value) {
                        self::$sqlLogger->makeMetaChange($this->post_name, $meta_id, $post_id, $meta_key, $meta_value);
                    }
                    foreach ($this->questions as $faq) {
                        $this->deleteMetaKey('Question', $faq, 'question', $i);
                        $this->deleteMetaKey('Answer', $faq, 'answer', $i);
                        $i++;
                    }

                    // Now add the new ones
                    $i = 0;
                    foreach ($holidayFile->questions as $faq) {
                        $this->addMetaKey('Question', $faq, 'question', $i);
                        $this->addMetaKey('Answer', $faq, 'answer', $i);
                        $i++;
                    }
                } else {
                    Utils::logger()->warning('FAQs are null for ' . $this->holidayName);
                }
            }
        }

        // Image checks - Cope with resort_post_id null - this may have to be run twice
        if ($this->resort_post_id !== null) {
            $this->checkForFieldChange($this->holidayMeta['holiday_banner_image_desktop']['meta_value'],
                Resort::getMetaValue($this->resort_post_id, 'resort_banner_image_desktop'),
                'holiday_banner_image_desktop', true);
            $this->checkForFieldChange($this->holidayMeta['holiday_banner_image_mobile']['meta_value'],
                Resort::getMetaValue($this->resort_post_id, 'resort_banner_image_mobile'),
                'holiday_banner_image_mobile', true);
            $this->checkForFieldChange($this->holidayMeta['_thumbnail_id']['meta_value'],
                Resort::getMetaValue($this->resort_post_id, 'resort_banner_image_mobile'),
                '_thumbnail_id', true);
        } else {
            Utils::logger()->warning('Resort Post ID is null for ' . $this->holidayName);
        }

        // Check Country and Holiday Type Taxonomy
        if ($holidayFile->holidayType !== null)
        {
            if ($this->holidayTypeTaxonomy != $holidayFile->holidayType) {
                HolidayTypeTaxonomy::updateTaxonomyPostID($post_id,
                    HolidayTypeTaxonomy::getHolidayTypeByName($holidayFile->holidayType));
            }

            // Colours supporting the Standards
            $this->checkForFieldChange($this->holidayMeta['holiday_tabs_0_sections_0_component_holiday_ski_standard_0_maximum_color']['meta_value'],
                $holidayFile->getStandardColour($holidayFile->holidayType, $holidayFile->getMaxStandard()),
                'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_maximum_color', true);
            $this->checkForFieldChange($this->holidayMeta['holiday_tabs_0_sections_0_component_holiday_ski_standard_0_maximum_icon']['meta_value'],
                $holidayFile->getStandardImage($holidayFile->holidayType, $holidayFile->getMaxStandard()),
                'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_maximum_icon', true);
            $this->checkForFieldChange($this->holidayMeta['holiday_tabs_0_sections_0_component_holiday_ski_standard_0_minimum_color']['meta_value'],
                $holidayFile->getStandardColour($holidayFile->holidayType, $holidayFile->getMinStandard()),
                'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_minimum_color', true);
            $this->checkForFieldChange($this->holidayMeta['holiday_tabs_0_sections_0_component_holiday_ski_standard_0_minimum_icon']['meta_value'],
                $holidayFile->getStandardImage($holidayFile->holidayType, $holidayFile->getMinStandard()),
                'holiday_tabs_0_sections_0_component_holiday_ski_standard_0_minimum_icon', true);

            // Check the Ski Standard Taxonomy
            $holidayFileTaxonomies = StandardTaxonomy::createTaxonomyList(
                $holidayFile->holidayType, $holidayFile->minStandard, $holidayFile->maxStandard);
            if ($this->skiStandardTaxonomies != $holidayFileTaxonomies) {
                StandardTaxonomy::updateTaxonomiesPostID($post_id, $holidayFileTaxonomies);
            }
        }

        if ($this->countryTaxonomy != CountryTaxonomy::getCountriesByPostID($holidayFile->resort_post_id, self::$conn, true)) {
            CountryTaxonomy::updateTaxonomyPostID($post_id,
                CountryTaxonomy::getCountriesByPostID($holidayFile->resort_post_id, self::$conn, true));
        }

        // Check the resort link
        if ($holidayFile->resort_post_id !== null) {
            $target_resort_meta_value = Utils::constructWPURLString(array(
                'title' => 'Learn more ...',
                'url' => '/resort/' . Resort::getResortPostName($holidayFile->resort_post_id),
                'target' => ''));

            $resort_meta_key = 'holiday_tabs_0_sections_4_component_3_and_4_columed_icons_icon_boxes_0_link';
            if (key_exists($resort_meta_key, $this->holidayMeta))
                $this->checkForFieldChange($this->holidayMeta[$resort_meta_key]['meta_value'], $target_resort_meta_value, $resort_meta_key, true);
        }

        // Check Links to Dates and prices TODO This is hard coded for now
        $arrCheckMetaKeys = array(
            'holiday_tabs_3_sections_3_component_different_cta_variations_ctas_0_buttons_0_button_link',
            'holiday_tabs_4_sections_2_component_different_cta_variations_ctas_0_buttons_0_button_link',
            'holiday_tabs_5_sections_1_component_different_cta_variations_ctas_0_buttons_0_button_link',
            'holiday_tabs_0_sections_7_component_different_cta_variations_ctas_0_buttons_0_button_link',);

         $url_string = Utils::constructWPURLString(array(
            'title' => 'View Dates and Prices',
            'url' => '/holiday/' . $this->post_name . '/?tab=view-dates-prices',
            'target' => ''));
        foreach($arrCheckMetaKeys as $meta_key) {
            if (key_exists($meta_key, $this->holidayMeta))
                $this->checkForFieldChange($this->holidayMeta[$meta_key]['meta_value'], $url_string, $meta_key, true);
        }

        // Finally see if Defaults need updating
        foreach ($this->postDataDictionary->getMetaDefaults() as $meta_key => $meta_value) {
            if (key_exists($meta_key, $this->holidayMeta))
                $this->checkForFieldChange($this->holidayMeta[$meta_key]['meta_value'], $meta_value, $meta_key, true);
        }
    }

    /**
     * @throws Exception
     */
    public function checkForUpdatesFromSugati() : void
    {
        $lowestPrice = null;
        $arrDepartureMonths = [];
        foreach ($this->arrSugatiTourIDs as $tour_id) {
            $sugatiTour = SugatiTour::getSugatiTour($tour_id);
            if ($sugatiTour == null) {
                Utils::logger()->warning("Sugati Tour '" . $tour_id . "' not found for '" .
                    $this->holidayName . "'" );
                continue;
            }
            // If the start date is before today then skip
            if ($sugatiTour->getSugatiTourDepartureDate() < new DateTime()) {
                Utils::logger()->info('Skipping ' . $sugatiTour->getSugatiTourName() .
                    ' as it has already departed. Start Date -> '.
                    $sugatiTour->getSugatiTourDepartureDate()->format('Y-m-d'));
                continue;
            }
            if ($sugatiTour->getSugatiTourStatus() != 'On Sale') {
                Utils::logger()->info('Skipping ' . $sugatiTour->getSugatiTourName() .
                    ' as it is not on sale. Status -> '.
                    $sugatiTour->getSugatiTourStatus());
                continue;
            }
            $departureMonth = $sugatiTour->getSugatiTourDepartureDate()->format('F');
            if (!in_array($departureMonth, $arrDepartureMonths)) {
                $arrDepartureMonths[] = $departureMonth;
            }
            if ($lowestPrice === null  || $sugatiTour->getSugatiTourPrice() < $lowestPrice) {
                $lowestPrice = $sugatiTour->getSugatiTourPrice();
            }
        }
        if ($lowestPrice !== null) {
            $holidayPrice = 'From £' . $lowestPrice;
        } else {
            $holidayPrice = 'No Holidays Available';
        }
        $arrDepartureMonths = array_map('ucfirst', $arrDepartureMonths);

        $this->checkDepartureMonths($arrDepartureMonths);

        $this->checkWebSiteHolidayPrice($holidayPrice);
    }

    /**
     * @throws Exception
     */
    private function checkWebSiteHolidayPrice(string $holidayPrice) : void
    {
        $post_name = $this->holidayName;
        $post_id = $this->post_id;
        $meta_key = 'holiday_price_note';
        $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
        $meta_value = $holidayPrice;

        $current_value = $this->holidayMeta[$meta_key]['meta_value'];
        if ($current_value != $meta_value) {
            $sql = "UPDATE wp_postmeta " .
                "SET meta_value = '" . $meta_value . "' " .
                "WHERE meta_id = " . $meta_id . " " .
                "AND post_id = " . $post_id . ";";
            $result = self::$conn->query($sql);
            if ($result === false) {
                throw new Exception('Failed to update ' . $post_name . ' ' . $meta_key . ': ' . self::$conn->error);
            }
            Utils::logger()->info("Holiday: '" . $this->holidayName . "' - Updated price from '" . $current_value . "' to '" . $meta_value . "'");
        }
    }

    /**
     * @throws Exception
     */
    private function checkForFieldChange(?string $web_value, ?string $file_value, string $field, bool $have_meta_key = false): void
    {
        // strip off trailing '\n' from the file value - not bothered about those
        if ($file_value === null)
            return;
        $file_value = rtrim($file_value, "\n");
        if ($web_value == $file_value) {
            // No value specified in the file so no change to make
            // or no change to make
            return;
        }

        if (!$have_meta_key)
            $meta_key = $this->postDataDictionary->getMetaKeyFromName($field);
        else
            $meta_key = $field;
        if ($meta_key == null) {
            throw new Exception("Meta Key not found for " . $field);
        }
        // get post_name from first element on this->holidayMeta - pop first element off array
        $post_name = array_pop($this->holidayMeta)['post_name'];
        $post_id = $this->post_id;
        if (!key_exists($meta_key, $this->holidayMeta))
            return;
        $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
        self::$sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $file_value);
    }

    /**
     * @throws Exception
     */
    private function deleteMetaKey(string $field, array $data, string $key, int $i) : void
    {
        $meta_key = $this->postDataDictionary->getMetaKeyFromName($field, $i);
        $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
        $meta_value = $data[$key]['meta_value'];
        // escape any ' or " in the meta_value
        $meta_value = str_replace("'", "\'", $meta_value);
        $meta_value = str_replace('"', '\"', $meta_value);

        $post_name = $data[$key]['post_name'];

        self::$sqlLogger->deleteMetaKey($post_name, $meta_id, $this->post_id,  $meta_key, $meta_value);
        $meta_key = '_' . $meta_key;
        if (!key_exists($meta_key, $this->holidayMeta))
            return;
        $meta_value = $this->holidayMeta[$meta_key]['meta_value'];
        $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
        self::$sqlLogger->deleteMetaKey($post_name, $meta_id, $this->post_id, $meta_key, $meta_value);
    }

    /**
     * @throws Exception
     */
    private function addMetaKey(string $field, array $data, string $key, int $i) : void
    {
        $meta_key = $this->postDataDictionary->getMetaKeyFromName($field, $i);
        $meta_value = $data[$key];
        $_meta_key = '_' . preg_replace('/_[0-9]_([a-z]*)$/', '_0_$1', $meta_key);
        $post_name = $this->holidayMeta[$_meta_key]['post_name'];
        self::$sqlLogger->addMetaKey($post_name, $this->post_id,  $meta_key, $meta_value);
        // Change last number in meta_key holiday_tabs_5_sections_0_component_full_width_faq_faqs_1_question to 0
        $meta_value = $this->holidayMeta[$_meta_key]['meta_value'];
        $_new_meta_key = '_' . preg_replace('/_[0-9]_([a-z]*)$/', '_' . $i . '_$1', $meta_key);
        self::$sqlLogger->addMetaKey($post_name, $this->post_id,  $_new_meta_key, $meta_value);
    }

    public function getHolidayMeta(): array
    {
        return $this->holidayMeta;
    }

    /**
     * @throws Exception
     */
    static public function loadHolidays(mysqli $conn, PostDataDictionary $postDataDictionary = null, ?SqlLogger $sqlLogger = null): array
    {
        self::$conn = $conn;
        self::$sqlLogger = $sqlLogger;

        $arrMetaData = array();

        // Read data from the table wp_postmeta
        // TODO allow specification of status - for now publish only
        $sql = "SELECT m.post_id, p.post_title, post_name, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = id  " .
            "and post_type = 'holiday' " .
            "and post_status = 'publish' " .
            "order by post_title ASC, meta_key ASC;";
        $result = $conn->query($sql);

        // Output the data as a csv
        $fp = fopen('data/holiday-meta.csv', 'w');

        // Output the headers
        fputcsv($fp, array('Post ID', 'Post Title', 'Post Name', 'Meta ID', 'Meta Key', 'Meta Value'));

        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $post_title = $data['post_title'];
            $post_name = $data['post_name'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];

            // Output the data as a csv
            fputcsv($fp, array($post_id, $post_title, $post_name, $meta_id, $meta_key, $meta_value));
            $arrMetaData[$post_title][$meta_key] = array('post_id' => $post_id, 'post_name' => $post_name, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        fclose($fp);

        // Now create the holiday objects
        foreach ($arrMetaData as $holidayName => $holidayMeta) {
            // Pop the first element off the array to get the post_id
            $post_id = array_shift($holidayMeta)['post_id'];
            $holiday = new HolidayFromWebsite($holidayName, $post_id, $holidayMeta, $postDataDictionary);
            self::$arrWebHolidaysByName[$holidayName] = $holiday;
        }

        return self::$arrWebHolidaysByName;
    }

    static public function createEmptyHoliday(string $holiday_name, int $basePostID, mysqli $conn, SqlLogger $sqlLogger) : array
    {
        // create an empty entry in wp_posts and get the post_id
        // This is the one time we interact directly with the database
        // They will go in with menu_order = 0, and we will fix that later
        $sequence = $sqlLogger->getSequence();
        $post_name = Utils::create_post_name($holiday_name);

        // escape any ' in the holiday name
        $holiday_name = str_replace("'", "\'", $holiday_name);

        $sql = "start transaction;" . "\n";
        $conn->query($sql);
        $sql = "INSERT INTO wp_posts (post_author, post_date, post_date_gmt, post_content, post_title, 
                      post_excerpt, post_status, comment_status, ping_status, post_password, 
                      post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, 
                      post_parent, guid, menu_order, post_type, post_mime_type, comment_count) " .
            "VALUES (15, now(), now(), '', '$holiday_name', '', 'publish', 'closed', 'closed', '', '$post_name', 
                    '', '', now(), now(), '', 0, '', 0, 'holiday', '', 0);" . "\n";
        $conn->query($sql);

        // Now iterate through the metadata and add it to the wp_postmeta table
        $post_id = $conn->insert_id;

        // Create the metadata by copying from the base post
        // Create audit entries as well
        $sql = "insert into scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "select $sequence, post_id, meta_key, null, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";
        $sql .= "insert into wp_postmeta (post_id, meta_key, meta_value) " .
            "select $post_id, meta_key, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";

        // Copy Data across from wp_term_relationships
        $sql .= "insert into wp_term_relationships (object_id, term_taxonomy_id, term_order) " .
            "select $post_id, term_taxonomy_id, term_order " .
            "from wp_term_relationships " .
            "where object_id = $basePostID " .";\n";
        $sql .= "commit;" . "\n";

        $conn->multi_query($sql);
        do {
            /* store the result set in PHP */
            if ($result = $conn->store_result()) {
                mysqli_free_result($result);
            }
        } while ($conn->next_result());

        $arrMetaData = array();
        // Get all the metadata for this holiday
        $sql = "SELECT m.post_id, post_name, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = ID  " .
            "and post_id = $post_id " .
            "order by meta_key ASC;";
        $result = $conn->query($sql);
        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $post_name = $data['post_name'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];

            $arrMetaData[$meta_key] = array('post_id' => $post_id, 'post_name' => $post_name, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        return $arrMetaData;
    }

    /**
     * @throws Exception
     */
    public static function updateWebSite() : void
    {
        self::updateHolidaysYouMayLike();
        self::updateHolidaysSortOrder();
    }

    /**
     * @throws Exception
     */
    public static function updateHolidaysYouMayLike() : void
    {
        // Holidays you may like - pick 4 random ones
        $arrHolidays = self::$arrWebHolidaysByName;
        $arrHolidaysYouMayLike = array();
        for ($i = 0; $i < 4; $i++) {
            $rand = rand(0, count($arrHolidays) - 1);
            $likeHoliday = array_slice($arrHolidays, $rand, 1);
            // Get first element of array
            $likeHoliday = array_shift($likeHoliday);
            $arrHolidaysYouMayLike[] = $likeHoliday;
            unset($arrHolidays[$likeHoliday->getHolidayName()]);
        }
        // create the WordPress URL string
        $wordpress_string = 'a:' . count($arrHolidaysYouMayLike) . ':{';
        $i = 0;
        foreach ($arrHolidaysYouMayLike as $holiday) {
            $holidayStr = strval($holiday->getPostID());
            $wordpress_string .= 'i:' . $i . ';s:' . strlen($holidayStr) . ':"' . $holidayStr . '";';
            $i++;
        }
        $wordpress_string .= '}';
        $meta_key = 'sections_2_component_you_may_also_like_posts';
        $post_id = 4030;

        $sql = "UPDATE wp_postmeta " .
            "SET meta_value = '" . $wordpress_string . "' " .
            "WHERE post_id = " . $post_id . " " .
            "AND meta_key = '" . $meta_key . "';";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to update Holidays You May Like: ' . self::$conn->error);
        }
    }

    /**
     * @throws Exception
     */
    public static function updateHolidaysSortOrder() : void
    {
        // Now set the sort order - for each holiday we find the earliest departure and then sort by that
        // slightly complicated before go live as not all holidays in the database have a departure date yet
        // For those we give them a date of 1st Jan 2099
        $arrHolidays = array();
        foreach (self::$arrWebHolidaysByName as $holiday) {
            $earliestDeparture = new DateTime('2099-01-01');
            foreach ($holiday->arrSugatiTourIDs as $tour_id) {
                $sugatiTour = SugatiTour::getSugatiTour($tour_id);
                if ($sugatiTour == null) {
                    Utils::logger()->warning("Sugati Tour '" . $tour_id . "' not found for '" .
                        $holiday->holidayName . "'");
                    continue;
                }
                if ($sugatiTour->getSugatiTourDepartureDate() < $earliestDeparture) {
                    $earliestDeparture = $sugatiTour->getSugatiTourDepartureDate();
                }
            }
            $departureStr = $earliestDeparture->format('Y-m-d');
            if (!key_exists($departureStr, $arrHolidays)) {
                $arrHolidays[$departureStr] = array();
            }
            $arrHolidays[$departureStr][] = $holiday;
        }
        ksort($arrHolidays);

        $count = 0;
        $sql = 'START TRANSACTION;';
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to start transaction: ' . self::$conn->error);
        }
        foreach ($arrHolidays as $arrHoliday) {
            foreach($arrHoliday as $holiday) {
                $sql = "update wp_posts set menu_order = " . $count++ . " where ID = " . $holiday->getPostID() . ";";
                $result = self::$conn->query($sql);
                if ($result === false) {
                    $sql = 'ROLLBACK;';
                    $result = self::$conn->query($sql);
                    if ($result === false) {
                        throw new Exception('Failed to rollback transaction: ' . self::$conn->error);
                    }
                    throw new Exception('Failed to update resort: ' . self::$conn->error);
                }
            }
        }
        $sql = 'COMMIT;';
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to commit transaction: ' . self::$conn->error);
        }
    }

    /*
     * $meta_value is a string of the form a:2:{i:0;s:7:"January";i:1;s:8:"February";} or blank
     */
    private function processWPDepartureString(string $meta_value) : void
    {
        if ($meta_value == '')
            return;

        $arrMonths = unserialize($meta_value);
        $arrMonths = array_map('trim', $arrMonths);
        $arrMonths = array_map('ucfirst', $arrMonths);
        $this->arrDepartureMonths = $arrMonths;
    }

    /**
     * @throws Exception
     */
    private function checkDepartureMonths($arrDepartureMonths) : void
    {
        if ($this->arrDepartureMonths == $arrDepartureMonths)
            return;

        $post_name = $this->holidayName;
        $post_id = $this->post_id;
        $meta_key = 'holiday_departure_months';
        $meta_value = serialize($arrDepartureMonths);
        if (!key_exists($meta_key, $this->holidayMeta)) {
            $current_meta_value = '{NOT SET}';
            // we have to add the meta key
            $sql = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) " .
                "VALUES (" . $post_id . ", '" . $meta_key . "', '" . $meta_value . "');";
        } else {
            $meta_id = intval($this->holidayMeta[$meta_key]['meta_id']);
            $current_meta_value = $this->holidayMeta[$meta_key]['meta_value'];

            $sql = "UPDATE wp_postmeta " .
                "SET meta_value = '" . $meta_value . "' " .
                "WHERE meta_id = " . $meta_id . " " .
                "AND post_id = " . $post_id . ";";
        }
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to update ' . $post_name . ' ' . $meta_key . ': ' . self::$conn->error);
        }
        Utils::logger()->info("Holiday: '" . $this->holidayName . "' - Updated Departure Months from '" . $current_meta_value . "' to '" . $meta_value . "'");
    }
}