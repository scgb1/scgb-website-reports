<?php

namespace SCGB;

use DateTime;
use Exception;

class SugatiTour
{
    const SUGATI_TOURS_URL = 'services/apexrest/sugati/api/tours';
    private string $sugati_tour_id;
    private string $sugati_tour_name;
    private float $sugati_tour_price;
    private string $sugati_tour_status;
    private DateTime $sugati_tour_departure_date;
    private DateTime $sugati_tour_return_date;

    static private array $arrSugatiTours = [];

    /**
     * @throws Exception
     */
    public function __construct(string $sugati_tour_id, string $sugati_tour_name, string $sugati_holiday_price,
                                string $sugati_tour_status, string $sugati_tour_departure_date, string $sugati_tour_return_date)
    {
        $this->sugati_tour_id = $sugati_tour_id;
        $this->sugati_tour_name = $sugati_tour_name;
        $this->sugati_tour_price = floatval($sugati_holiday_price);
        $this->sugati_tour_status = $sugati_tour_status;
        $this->sugati_tour_departure_date = new DateTime($sugati_tour_departure_date);
        $this->sugati_tour_return_date = new DateTime($sugati_tour_return_date);

        return $this;
    }

    public function getSugatiTourID() : string
    {
        return $this->sugati_tour_id;
    }
    static public function getSugatiTourIDs() : array
    {
        return self::$arrSugatiTours;
    }
    public function getSugatiTourName() : string
    {
        return $this->sugati_tour_name;
    }
    public function getSugatiTourPrice() : float
    {
        return $this->sugati_tour_price;
    }
    public function getSugatiTourStatus() : string
    {
        return $this->sugati_tour_status;
    }

    /**
     * @return DateTime
     */
    public function getSugatiTourDepartureDate() : DateTime
    {
        return $this->sugati_tour_departure_date;
    }
    public function getSugatiTourReturnDate() : DateTime
    {
        return $this->sugati_tour_return_date;
    }

    static public function getSugatiTour(string $sugati_tour_id) : ?SugatiTour
    {
        if (key_exists($sugati_tour_id, self::$arrSugatiTours)) {
            return self::$arrSugatiTours[$sugati_tour_id];
        }
        return null;
    }

    /**
     * @throws Exception
     */
    public static function authenticate(): array
    {
        Utils::logger()->debug('Authenticating with Salesforce',
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__));

        $grant_type = 'password';
        $sugati_username = Utils::getConfigItem('sugati_username');
        $sugati_password = Utils::getConfigItem('sugati_password');
        $sugati_client_id = Utils::getConfigItem('sugati_client_id');
        $sugati_client_secret = Utils::getConfigItem('sugati_client_secret');

        $sugati_url = Utils::getConfigItem('sugati_url');

        // generate a POST request with curl, body containing the parameters for the request
        $curl = curl_init($sugati_url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=$grant_type&client_id=$sugati_client_id&" .
            "client_secret=$sugati_client_secret&username=$sugati_username&password=$sugati_password");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // execute the request
        $response = curl_exec($curl);

        // any errors?
        if (curl_errno($curl)) {
            Utils::logger()->error('Failed to authenticate with Salesforce: ' . curl_error($curl),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__));
            throw new Exception('Failed to authenticate with Salesforce: ' . curl_error($curl));
        }

        // close the connection
        curl_close($curl);

        // parse the output to get the access token. Of the form:
        // {"access_token":"{access token}","instance_url":"{url}}","id":"{id}}","token_type":"Bearer","issued_at":"1695618463356","signature":"signature"}
        // we want the access token and the instance url
        $json = json_decode($response);
        $access_token = "Bearer " . $json->access_token;
        $instance_url = $json->instance_url;

        return array(
            'access_token' => $access_token,
            'url' => $instance_url);
    }

    /**
     * @throws Exception
     */
    public static function loadToursFromSugati(): void
    {
        $arrAuthDetails = SugatiTour::authenticate();

        $arrSugatiToursRaw = self::getSugatiTours($arrAuthDetails['access_token'], $arrAuthDetails['url']);

        // Dump to a csv
        $fp = fopen('data/sugati-tours.csv', 'w');
        fputcsv($fp, array('Name', 'ID', 'Status', 'Departure Date', 'Return Date', 'Price',));
        foreach ($arrSugatiToursRaw as $tour) {
            $sugati_tour_name = $tour['tour_detail']['Name'];
            $sugati_tour_id = $tour['tour_detail']['Id'];
            $sugati_tour_status = $tour['tour_detail']['sugati__O_Status__c'];
            $sugati_tour_departure_date = $tour['tour_detail']['sugati__O_Departure_Date__c'];
            $sugati_tour_return_date = $tour['tour_detail']['sugati__O_Return_Date__c'];
            $sugati_tour_price = $tour['tour_detail']['sugati__S_O_Tour_Price_Per_Person__c'];
            fputcsv($fp, array(
                $sugati_tour_name,
                $sugati_tour_id,
                $sugati_tour_status,
                $sugati_tour_departure_date,
                $sugati_tour_return_date,
                $sugati_tour_price,
            ));

            self::$arrSugatiTours[$sugati_tour_id] = new SugatiTour($sugati_tour_id, $sugati_tour_name, $sugati_tour_price,
                $sugati_tour_status, $sugati_tour_departure_date, $sugati_tour_return_date);

        }
        fclose($fp);
    }

    /**
     * @throws Exception
     */
    public static function getSugatiTours(string $access_token, string $instance_url): array
    {
        // Construct the full url
        $url = $instance_url . '/' . SugatiTour::SUGATI_TOURS_URL;

        // generate a GET request with curl, body containing the parameters for the request
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: $access_token"));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // execute the request
        $response = curl_exec($curl);

        // any errors?
        if (curl_errno($curl)) {
            Utils::logger()->error('Failed to get Tours from Sugati: ' . curl_error($curl),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__));
            throw new Exception('Failed to get Tours from Sugati: ' . curl_error($curl));
        }

        // close the connection
        curl_close($curl);

        // get the output and turn into an array
        return json_decode($response, true)['tours'];
    }
}