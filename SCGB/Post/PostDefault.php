<?php

namespace SCGB;

use Exception;

class PostDefault
{
    private const THIS_CONTEXT = 'All';

    protected const VALID_CONTEXT = array(
        'All',
        'Resort',
        'Holiday',
        'SkiTest',
        'Rep'
    );

    private static array $arrPostDefaults = [];
    private static array $arrPostDefaultsRegEx = [];

    private string $context;
    private string $name;
    private string $details;
    private int $post_id;
    private string $meta_key;
    private string $meta_value;
    private bool $is_reg_ex;
    public function __construct(string $context, string $name, string $details, int $post_id, string $meta_key, string $meta_value)
    {
        $this->context = $context;
        $this->name = $name;
        $this->details = $details;
        $this->post_id = $post_id;
        $this->meta_key = $meta_key;
        $this->$meta_value = $meta_value;

        if (str_starts_with($meta_key, '/'))
            $this->is_reg_ex = true;
        else
            $this->is_reg_ex = false;
        return $this;
    }

    public function isRegEx() : bool {
        return $this->is_reg_ex;
    }

    /**
     * @throws Exception
     */
    static public function loadDefaults(string $defaultsFile) : void
    {
        // Defaults file is a csv with 4 columns:
        // - context - Which kind of Post does this apply to:  All, Resort, Holiday, SkiTest, ...
        // - Name
        // - Details
        // - post_id - 0 indicates applies to all - else specific
        // - meta_key - regex begins with /
        //	meta_value
        $fp = fopen($defaultsFile, 'r');

        // ignore the first line
        fgets($fp);

        // Now iterate through the rest of the file
        while ($line = fgets($fp)) {
            $arrData = explode(',', $line);
            if (count($arrData) != 6) {
                throw new Exception('Invalid line in Defaults file -> ' . $line);
            }
            $context = $arrData[0];
            if (!in_array($context, self::VALID_CONTEXT)) {
                throw new Exception('Invalid context in Defaults file ' . $line);
            }
            $name = $arrData[1];
            $details = $arrData[2];
            $post_id = intval($arrData[3]);
            $meta_key = $arrData[4];
            $meta_value = $arrData[5];

            $default = new PostDefault($context, $name, $details, $post_id, $meta_key, $meta_value);
            if ($default->isRegEx())
                self::$arrPostDefaultsRegEx[$context][$meta_key][$post_id] = $default;
            else
                self::$arrPostDefaults[$context][$meta_key][$post_id] = $default;
        }
    }
}