<?php

namespace SCGB;

use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Exception\CommonMarkException;
use mysqli;

class Post
{
    protected static ?SqlLogger $sqlLogger;
    protected static mysqli $conn;
    protected static array $arrPostDataByPostName = [];
    protected static array $arrPostDataByPostID = [];
    protected static array $arrPostDataByPostTitle = [];

    protected ?int $post_id = null;
    protected ?string $post_title = null;
    protected ?string $post_name = null;

    private CommonMarkConverter $converter;

    public function __construct(string $post_title)
    {
        $this->post_title = $post_title;
        $this->converter = new CommonMarkConverter();
        return $this;
    }

    public static function getPostIDFromName(string $name): ?int
    {
        $post_id = null;
        if (key_exists($name, self::$arrPostDataByPostName)) {
            $post_id = intval(self::$arrPostDataByPostName[$name]['post_id']);
        } elseif (key_exists($name, self::$arrPostDataByPostTitle)) {
            $post_id = intval(self::$arrPostDataByPostTitle[$name]['post_id']);
        }
        return $post_id;
    }
    public static function getPostNameFromID(int $id): ?string
    {
        $post_name = null;
        if (key_exists($id, self::$arrPostDataByPostID)) {
            $post_name = self::$arrPostDataByPostID[$id]['post_name'];
        }
        return $post_name;
    }
    public static function getPostTitleFromID(int $id): ?string
    {
        $post_title = null;
        if (key_exists($id, self::$arrPostDataByPostID)) {
            $post_title = self::$arrPostDataByPostID[$id]['post_title'];
        }
        return $post_title;
    }
    public static function getMetaData(int $post_id) : ?array
    {
        if (!key_exists($post_id, self::$arrPostDataByPostID)) {
            return null;
        }
        return self::$arrPostDataByPostID[$post_id];
    }

    /**
     * @throws CommonMarkException
     */
    protected function convertToHTML(string $dataValue) : string
    {
        $dataValue = $this->converter->convert($dataValue)->getContent();
        $dataValue = preg_replace('/^<p>/', '', $dataValue);
        return preg_replace('/<\/p>$/', '', $dataValue);
    }
}