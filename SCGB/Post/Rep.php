<?php
declare(strict_types=1);
namespace SCGB;

class Rep extends Post
{
    protected ?string $introText = null;
    protected ?string $rep_role = null;
    protected ?string $rep_picture_id = null;
    protected array $arrRepResorts = array();

    protected array $arrRepHolidays = array();
    protected ?PostDataDictionary $repDataDictionary;

    public function __construct(string $repName)
    {
        parent::__construct($repName);
        return $this;
    }

    // Setters
    protected function setPostId(int $post_id): self
    {
        $this->post_id = $post_id;
        return $this;
    }
}