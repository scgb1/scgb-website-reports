<?php /** @noinspection PhpUnusedParameterInspection */
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class ResortCheckFunctions
{
    private SqlLogger $sqlLogger;
    private IssueLogger $issueLogger;
    private array $arrCheckFunctions = array();
    private array $arrChecksByField;
    private array $arrResortPosts;
    private array $arrAttachmentPosts;
    private array $arrSectionMap;
    private array $arrOverviewTabMap;
    private array $arrOverviewTabMapByName = array();
    private array $arrMetaDataDefaultsPerResort = array('post_id' => null, 'checks' => array(),);
    private array $arrMetaDataDefaults = array();
    private array $arrMetaDataDefaultsRegex = array();
    private array $arrMetaDataDecodes = array();
    private array $arrCountries;
    private array $arrResortCountries;
    private array $arrResortsByWeatherAPI;

    const arrSections = array (
        'Overview',
        'Community',
        'Maps',
        'Weather',
        'Instructor-led guiding',
        'Reps',
        'Discounts',
        'Freshtracks',
    );

    const arrOverviewData = array(
        'component_ski_club_offerings' => array('name' => 'Ski Club Offerings', 'position' => 0, ),
        'component_resort_overview' => array('name' => 'Resort Overview', 'position' => 1, ),
        'component_pros_and_cons' => array('name' => 'Pros & Cons', 'position' => 2, ),
        'component_coloured_bg_sections' => array(
            'Skiing And Snowboarding' => array('position' => 3, 'text' => 'Skiing And Snowboarding',),
            'Getting There' => array('position' => 4, 'text' => 'Getting There',),
        ),
        'component_resort_lift_passes' => array('name' => 'Lift Passes', 'position' => 5, ),
        'component_image_left_and_right_with_benefit_list' => array('name' => 'Ikon Pass', 'position' => 6, ),
        'component_image_left_and_right' => array(
            'Social' => array('position' => 7, 'text' => 'Become part of.*community',),
            'Sustainability' => array('position' => 13, 'text' => 'Sustainability',),
        ),
        'component_review_summary' => array('name' => 'Review Summary', 'position' => 8, ),
        'component_resort_holidays' => array('name' => 'Holiday', 'position' => 9, ),
        'component_gallery' => array('name' => 'Gallery', 'position' => 10, ),
        'component_full_width_faq' => array('name' => 'FAQ', 'position' => 11, ),
        'component_resort_map' => array('name' => 'Resort Map', 'position' => 12, ),
    );
    private array $arrTargetTabMapByName;

    const SNOW_OVERVIEW = array(
        'Switzerland' => '7088',
        'Italy' => '7086',
        'France' => '7081',
        'Germany' => '7090',
        'Austria' => '7087',
        'Czech Republic' => '7089',
        'Slovakia' => '7089',
        'Poland' => '7089',
        'Scotland' => '7091',
        'Slovenia' => '7089',
        'Romania' => '7089',
        'Bulgaria' => '7089',
        'Andorra' => '7085',
        'Spain' => '7085',
        'Norway' => '7103',
        'Sweden' => '7103',
        'Finland' => '7103',
        'USA' => '7092',
        'Canada' => '7093',
        'Chile' => '-',
        'Argentina' => '-',
        'Japan' => '7094',
        'Australia' => '-',
        'New Zealand' => '-',
    );


    /**
     * @throws Exception
     */
    public function __construct(mysqli $mysqli, SqlLogger $sqlLogger, IssueLogger $issueLogger)
    {
        $this->sqlLogger = $sqlLogger;
        $this->issueLogger = $issueLogger;
        // Iterate through all the methods in this class and add the ones that start with 'check' to the array
        // $arrCheckFunctions
        $arrMethods = get_class_methods($this);
        foreach ($arrMethods as $method) {
            if (str_starts_with($method, 'check')) {
                $this->arrCheckFunctions[$method] = $method;
            }
        }

        // Load the country data
        $this->arrCountries = $this->getCountries($mysqli);
        $this->arrResortCountries = $this->getResortCountries($mysqli);
        $this->arrResortsByWeatherAPI = $this->getResortsByWeatherAPI($mysqli);

        // Create the post arrays
        $this->arrResortPosts = $this->getPosts($mysqli, 'sc_resort');
        $this->arrAttachmentPosts = $this->getPosts($mysqli, 'attachment');

        foreach (self::arrOverviewData as $data) {
            if (key_exists('name', $data)) {
                $this->arrTargetTabMapByName[$data['name']] = $data['position'];
            }else {
                foreach ($data as $key2 => $data2) {
                    $this->arrTargetTabMapByName[$key2] = $data2['position'];
                }
            }
        }

        // Make sure the sections valid or the checks will not run!
        $this->arrChecksByField = $this->createListOfChecks();
        foreach ($this->arrChecksByField as $field) {
            if (key_exists('tab', $field)) {
                if (!$this->confirmTabMapName($field['tab'])) {
                    Utils::logger()->error('Invalid Tab name: ' . $field['tab'], array('file' => basename(__FILE__), 'line' => __LINE__));
                    die(1);
                }
            }
        }
        $this->arrMetaDataDefaults = $this->getMetaDataDefaults($mysqli);
        $this->arrMetaDataDefaultsRegex = $this->getMetaDataDefaults($mysqli, true);
        return $this;
    }

    public function getCheckFunctions(): array
    {
        return $this->arrCheckFunctions;
    }

    /**
     * @throws Exception
     */
    public function createListOfChecks(): array
    {
        $arrChecks = array(
            'Resort Lift Passes Content' => array(
                'tab' => 'Lift Passes',
                'meta_key_check' => '_component_resort_lift_passes_content',
                'checks' => array(
                    array('checkURLValid'),
                    array('checkCommaNoSpace'),
                    array('checkFullStop'),
                    array('checkRegex', array(
                        array('([^>])020 8410 2009[^<]', '$1<a href="tel:+442084102009">020 8410 2009</a>'),
                    ))
                ),
            ),
            'Text over Resort Header Image' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => 'resort_intro_text',
                'checks' => array(
                    array('checkURLValid'),
                    array('checkStringLength', 'min_length' => 30, 'max_length' => 300),
                    array('checkCommaNoSpace'),
                    array('checkFullStop'),
                    array('checkRegex', array(
                        array('(^.*[^.])$', '$1.'),
                    ))
                ),
            ),
            'Mobile Image' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => 'resort_banner_image_mobile',
                'checks' => array(array('checkImage', 'resort_banner_image_desktop'),
                ),
            ),
            'Desktop Image' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => 'resort_banner_image_desktop',
                'checks' => array(
                    array('checkImage'),
                ),
            ),
            'Resort Featured Image' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => 'resort_banner_image_mobile',
                'checks' => array(array('checkFeaturedImage', ),
                ),
            ),
            'Resort Overview - Heading' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => '_heading',
                'checks' => array(
                    array('checkStringLength', 'min_length' => 10, 'max_length' => 190),
                ),
            ),
            'Resort Overview - Content' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => '_component_resort_overview_content',
                'checks' => array(
                    array('checkURLValid'),
                    array('checkCommaNoSpace'),
                    array('checkFullStop'),
                    array('checkMalformedLink'),
                    array('checkRegex', array(
                        array('Resort website: https:\/\/([a-z0-9\-\.\/:]+)', 'Resort website: <a href="https://$1"$1</a>'),
                        array('Resort website: http:\/\/([a-z0-9\-\.\/:]+)', 'Resort website: <a href="https://$1"$1</a>'),
                        array('Resort website: ([a-z0-9\-\.\/:]+)', 'Resort website: <a href="https://$1">$1</a>'),
                    )),
                ),
            ),
            'Pros & Cons - Status' => array(
                'tab' => 'Pros & Cons',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_pros_and_cons_cons')
                    ),
                ),
            ),
            'Gallery - Status' => array(
                'tab' => 'Gallery',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_gallery_gallery'),
                    ),
                ),
            ),
            'Social Links - Status' => array(
                'tab' => 'Social',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent',
                        array('component_image_left_and_right_buttons_0_button_link',
                            'component_image_left_and_right_buttons_1_button_link'),
                    ),
                ),
            ),
            'Resort Overview - Enable if Content' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_resort_overview_content'),
                    ),
                ),
            ),
            'Skiing And Snowboarding - Enable if Content' => array(
                'tab' => 'Skiing And Snowboarding',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_coloured_bg_sections_content'),
                    ),
                ),
            ),
            'Skiing And Snowboarding - Content' => array(
                'tab' => 'Skiing And Snowboarding',
                'meta_key_check' => '_component_coloured_bg_sections_content',
                'checks' => array(
                    array('checkCommaNoSpace'),
                    array('checkFullStop'),
                    array('checkMalformedLink')
                ),
            ),
            'Getting There - Content' => array(
                'tab' => 'Getting There',
                'meta_key_check' => '_component_coloured_bg_sections_content',
                'checks' => array(
                    array('checkURLValid'),
                    array('checkCommaNoSpace'),
                    array('checkFullStop'),
                    array('checkMalformedLink')
                ),
            ),
            'Getting There - Enable if Content' => array(
                'tab' => 'Getting There',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_coloured_bg_sections_content'),
                    ),
                ),
            ),
            'Ski Pass Prices - Enable if Content' => array(
                'tab' => 'Lift Passes',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkEnabledSectionHasContent', array('component_resort_lift_passes_content'),
                    ),
                ),
            ),
            'Weather API ID - Check Set' => array(
                'tab' => 'Resort Overview',
                'meta_key_check' => 'resort_weather_resort_api_id',
                'checks' => array(
                    array('checkStringLength', 'min_length' => 1),
                ),
            ),
            'Ski Club Offerings - Confirm Visibility' => array(
                'tab' => 'Ski Club Offerings',
                'meta_key_check' => '_section_display_status',
                'checks' => array(
                    array('checkSkiClubOfferings'),
                ),
            ),
            'Gallery - Hide & Empty Stock Images' => array(
                'tab' => 'Gallery',
                'meta_key_check' => '_component_gallery_gallery',
                'checks' => array(
                    array('checkStockGallery'),
                ),
            ),
        );

        // See if any checks to be suppressed
        $suppressChecksStr = Utils::getConfigItem('suppressChecks', false);
        if ($suppressChecksStr) {
            foreach ($arrChecks as $checkName => $arrCheck) {
                foreach ($arrCheck['checks'] as $checkKey => $arrCheckDetails) {
                    if (in_array($arrCheckDetails[0], explode(',', $suppressChecksStr))) {
                        unset($arrChecks[$checkName]['checks'][$checkKey]);
                    }
                }
            }
        }

        return $arrChecks;
    }

    /**
     * @throws Exception
     */
    public function runChecks(string $resort, array $arrMetaKeys): void
    {
        $post_id = $arrMetaKeys['sections']['post_id'];
        Utils::logger()->info('Processing ' . $post_id . '/' . $resort);
        // Need to determine which sections where
        if (!$this->buildMetaDataMap($arrMetaKeys)) {
            Utils::logger()->info('No checks run for ' . $resort);
            return;
        }

        // Run the resort level checks - can do in one go
        $this->runResortChecks($resort, $arrMetaKeys);

        foreach ($arrMetaKeys as $meta_key => $data) {
            if (str_starts_with($meta_key, '_')) {
                continue;
            }
            // Check Defaults for everything
            $this->checkDefaults($arrMetaKeys, $meta_key);

            foreach($this->arrChecksByField as $field => $checkData)
            {
                if (key_exists('tab', $checkData)) {
                    $meta_key_check = $checkData['meta_key_check'];
                    $tab = $checkData['tab'];

                    // if meta_data_check starts with a _ then it needs turning into a full meta_key
                    if (str_starts_with($meta_key_check, '_')) {
                        // strip off the leading _
                        $meta_key_check = substr($meta_key_check, 1);
                        $full_meta_key = $this->constructFullMetaKey('Overview', $tab, $meta_key_check);
                        if ($full_meta_key === null) {
                            continue;
                        }
                    } else {
                        $full_meta_key = $meta_key_check;
                    }

                    if (str_ends_with($meta_key, $meta_key_check) && $meta_key == $full_meta_key) {
                        // Checks to run
                        $arrChecks = $checkData['checks'];
                        foreach ($arrChecks as $check) {
                            // Run the check
                            $func = array_shift($check);
                            $params = $check;
                            if (method_exists($this, $func)) {
                                $this->$func($arrMetaKeys, $tab, $field, $meta_key, $params);
                            } else {
                                throw new Exception('Unknown check ' . $func);
                            }
                        }
                    }
                }
            }
        }
    }

    private function getPosts(mysqli $conn, string $type): array
    {
        $sql = "select * " .
            "from wp_posts " .
            "where post_type = '" . $type . "';";
        $result = $conn->query($sql);

        $arrPosts = array();
        while ($data = $result->fetch_assoc()) {
            $arrPosts[$data['ID']] = $data;
        }
        return $arrPosts;
    }

    private function getCountries(mysqli $conn) : array
    {
        $sql = "select a.term_id,  a.name, b.parent " .
            "from wp_terms a, wp_term_taxonomy b " .
            "where a.term_id = b.term_taxonomy_id " .
            "and b.taxonomy = 'country'";

        $arrCountries = array();
        $result = $conn->query($sql);
        while ($data = $result->fetch_assoc()) {
            $id = $data['term_id'];
            $name = $data['name'];
            $parent = $data['parent'];

            $arrCountries[$id] = array('name' => $name, 'parent' => $parent);

        }
        return $arrCountries;
    }

    private function getResortCountries(mysqli $conn) : array
    {
        $sql = 'select object_id, term_taxonomy_id ' .
            'from wp_term_relationships ' .
            'where term_taxonomy_id in ' .
                '(select term_taxonomy_id ' .
                'from wp_term_taxonomy ' .
                'where taxonomy = "country" ' .
                'and parent != 0) ' .
            'order by object_id;';
        $result = $conn->query($sql);

        $arrResortCountries = array();

        while ($data = $result->fetch_assoc()) {
            $post_id = $data['object_id'];
            $country_id = $data['term_taxonomy_id'];

            $arrResortCountries[$post_id][] = $country_id;
        }

        return $arrResortCountries;
    }

    private function getResortsByWeatherAPI(mysqli $conn) : array
    {
        $sql = "select intSCGBResortID, strResortCountry, strResortName, " .
            "intLowerWeatherStationElevation, intUpperWeatherStationElevation, " .
            "intSnowLowerPisteInCentimeters, intSnowUpperPisteInCentimeters, " .
            "fltAmountOfLastSnowInCentimeters, dtmLastSnowed, strOffPisteSnowConditions, " .
            "intLiftsOpen, intLiftsTotal, dtmResortOpening, dtmResortClosing " .
            "from scgb_resort_data " .
            "order by strResortCountry, strResortName;";
        $result = $conn->query($sql);

        $arrResortsByWeatherAPI = array();

        while ($data = $result->fetch_assoc()) {
            $resort_id = $data['intSCGBResortID'];
            $country = $data['strResortCountry'];
            $name = $data['strResortName'];
            $lower_elevation = $data['intLowerWeatherStationElevation'];
            $upper_elevation = $data['intUpperWeatherStationElevation'];
            $snow_lower_piste = $data['intSnowLowerPisteInCentimeters'];
            $snow_upper_piste = $data['intSnowUpperPisteInCentimeters'];
            $amount_last_snow = $data['fltAmountOfLastSnowInCentimeters'];
            $last_snowed = $data['dtmLastSnowed'];
            $off_piste_snow_conditions = $data['strOffPisteSnowConditions'];
            $lifts_open = $data['intLiftsOpen'];
            $lifts_total = $data['intLiftsTotal'];
            $resort_opening = $data['dtmResortOpening'];
            $resort_closing = $data['dtmResortClosing'];


            if (array_key_exists($resort_id, $arrResortsByWeatherAPI)) {
                Utils::logger()->error('Duplicate Weather API',
                    array('resort_id' => $resort_id, 'name' => $name, 'country' => $country));

            }
            $got_api_data=false;
            if ($last_snowed !== null || $lifts_open !== null || $lifts_total !== null ||
                $off_piste_snow_conditions !== null || $amount_last_snow !== null ||
                $snow_lower_piste !== null || $snow_upper_piste !== null ||
                $resort_opening !== null || $resort_closing !== null) {
                $got_api_data = true;
            }
            $arrResortsByWeatherAPI[$resort_id] = array('name' => $name, 'country' => $country,
                'lower' => $lower_elevation, 'upper' => $upper_elevation,
                'snow_lower_piste' => $snow_lower_piste, 'snow_upper_piste' => $snow_upper_piste,
                'amount_last_snow' => $amount_last_snow, 'last_snowed' => $last_snowed,
                'off_piste_snow_conditions' => $off_piste_snow_conditions,
                'lifts_open' => $lifts_open, 'lifts_total' => $lifts_total,
                'resort_opening' => $resort_opening, 'resort_closing' => $resort_closing,
                'got_api_data' => $got_api_data,);
        }

        return $arrResortsByWeatherAPI;
    }

    private function getMetaDataDefaults(mysqli $mysqli, bool $meta_key_is_regex = false): array
    {
        $meta_key_is_regex = ($meta_key_is_regex) ? 1 : 0;

        $sql = "select id, post_id, meta_key, meta_value " .
            "from scgb_wp_postmeta_defaults " .
            "where enabled = 1 " .
            "and meta_key_is_regex = " . $meta_key_is_regex . " " .
            "order by post_id, meta_key";
        $result = $mysqli->query($sql);

        $retArray = array();

        while ($data = $result->fetch_assoc()) {
            $id = $data['id'];
            $post_id = $data['post_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];

            if (!isset($retArray[$meta_key])) {
                $retArray[$meta_key] = array();
            }
            $retArray[$meta_key][$post_id]['meta_value'] = $meta_value;
            $retArray[$meta_key][$post_id]['id'] = $id;
        }
        return $retArray;
    }

    private function getMetaDataDecodes(mysqli $mysqli): void
    {
        $sql = "select context, field, decode " .
            "from scgb_decodes " .
            "order by context, field";
        $result = $mysqli->query($sql);

        while ($data = $result->fetch_assoc()) {
            //$context = $data['context'];
            $field = $data['field'];
            $decode = $data['decode'];
            $this->arrMetaDataDecodes[$field] = $decode;
        }
    }

    /**
     * @throws Exception
     */
    public function buildMetaDataMap($arrMetaKeys): bool
    {

        $this->arrSectionMap = array();
        $this->arrOverviewTabMap = array();
        $this->arrOverviewTabMapByName = array();

        // We can get the resort name from the first element of arrMetaKeys
        $resort_name = array_values($arrMetaKeys)[0]['post_title'];
        $post_id = array_values($arrMetaKeys)[0]['post_id'];

        // First thing to do is to build the section map
        // We know that there will be contiguous sections starting at 0
        // We also know that they might not all be there - but we do want them in the right order
        for ($i=0; $i < 10; $i++) {
            $meta_key = 'resort_tabs_' . $i . '_tab_name';
            if (!array_key_exists($meta_key, $arrMetaKeys)) {
                break;
            }
            $section_name = $arrMetaKeys[$meta_key]['meta_value'];
            if ($section_name == '') {
                $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Empty section name at section ' . $i,
                    '', array('post_id' => $post_id, 'post_name' => $resort_name));
            } elseif (!in_array($section_name, self::arrSections)){
                $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Unknown section ' . $section_name,
                    '', array('post_id' => $post_id, 'post_name' => $resort_name));
            } else {
                $this->arrSectionMap[$section_name] = $i;
            }
        }

        // Now build the overview tab map -
        if (!key_exists('Overview', $this->arrSectionMap)) {
            $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Overview section not found for post_id ' . $post_id,
                '', array('post_id' => $post_id, 'post_name' => $resort_name));
            return false;
        }
        $overview_section = $this->arrSectionMap['Overview'];

        $meta_key = 'resort_tabs_' . $overview_section . '_sections';
        if (!key_exists($meta_key, $arrMetaKeys)) {
            $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Overview sections not found for post_id ' . $post_id,
                '', array('post_id' => $post_id, 'post_name' => $resort_name));
            return false;
        }
        $meta_value = $arrMetaKeys[$meta_key]['meta_value'];

        // Work through the list of sections - number is between the first and second : of meta_value
        $arrTabs = explode(':', $meta_value);
        $numTabs = $arrTabs[1];

        // Tab names between the quotes in the meta_value
        $arrTabKeys = explode('"', $meta_value);
        $arrTabKeys = array_slice($arrTabKeys, 1);

        for ($i=0; $i < $numTabs * 2; $i+=2) {
            $tab_key = $arrTabKeys[$i];
            if (!key_exists($tab_key, self::arrOverviewData)) {
                $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Unknown overview tab ' . $tab_key,
                    '', array('post_id' => $post_id, 'post_name' => $resort_name));
                continue;
            }
            $data = self::arrOverviewData[$tab_key];
            if (key_exists('name', $data)) {
                $tab_name = $data['name'];
                $found_tab_position = $i;
                $found_tab_position = intval($found_tab_position/2);
                $this->arrOverviewTabMap[$found_tab_position] = array('tab' => $tab_name, );
                $this->arrOverviewTabMapByName[$tab_name] = $found_tab_position;
            } else {
                foreach ($data as $tab_name => $tab_data) {
                    $tab_text = $tab_data['text'];

                    foreach($arrMetaKeys as $meta_key => $data) {
                        if (str_starts_with($meta_key, '_') || !str_ends_with($meta_key, '_heading')) {
                            continue;
                        }
                        $meta_value = $data['meta_value'];
                        if (preg_match('/' . strtolower($tab_text) . '/', strtolower($meta_value))) {
                            // extract the tab number from the meta_key - second number
                            if (preg_match('/^resort_tabs_\d+_sections_(\d+)_/', $meta_key, $matches)) {
                                $found_tab_position = $matches[1]*2;
                                $found_tab_position = intval($found_tab_position/2);
                                $this->arrOverviewTabMap[$found_tab_position] = array('tab' => $tab_name, );
                                $this->arrOverviewTabMapByName[$tab_name] = $found_tab_position;
                                break;
                            } else {
                                throw new Exception($resort_name . ' - Unable to get tab number from ' . $meta_key);
                            }
                        }
                    }
                }
            }
        }

        // Now looking to do two things - Check tabs in the right order and to highlight any missing tabs
        // First thing to do is see what tabs missing
        $arrMissingTabs = array();
        foreach ($this->arrTargetTabMapByName as $tab_name => $tab_position) {
            if (!key_exists($tab_name, $this->arrOverviewTabMapByName)) {
                $this->checkMissingTab($arrMetaKeys, $resort_name, $post_id, $tab_name);
                $arrMissingTabs[] = $tab_name;
            }
        }
        $offset = 0;
        foreach ($this->arrTargetTabMapByName as $tab_name => $tab_position) {
            if (in_array($tab_name, $arrMissingTabs)) {
                $offset++;
                continue;
            }
            $diff = $this->arrOverviewTabMapByName[$tab_name] - $tab_position + $offset;
            if ($diff) {
                $this->issueLogger->report($resort_name, 'Resort Checks', __FUNCTION__, 'Tab ' . $tab_name . ' is in the wrong position',
                    '', array('post_id' => $post_id, 'post_name' => $resort_name));
                break;
            }

        }
        return true;
    }

    public function confirmTabMapName(string $tab_name): bool
    {
        return key_exists($tab_name, $this->arrTargetTabMapByName);
    }

    /**
     * @throws Exception
     */
    public function constructFullMetaKey($section, $tab, $meta_key_check): string|null
    {
        if (!key_exists($section, $this->arrSectionMap)) {
            Utils::logger()->emergency('Unable to find section => ' . $section . ' in arrSectionMap',
                array('function' => __FUNCTION__, 'section' => $section, 'tab' => $tab, 'meta_key_check' => $meta_key_check));
            throw new Exception('Unable to find section => ' . $section . ' in arrSectionMap',);
        }
        $section_id = $this->arrSectionMap[$section];

        if (!key_exists($tab, $this->arrOverviewTabMapByName)) {
            return null;
        }
        $tab_id = $this->arrOverviewTabMapByName[$tab];
        return 'resort_tabs_' . $section_id . '_sections_' . $tab_id . '_' . $meta_key_check;
    }

    /**
     * @throws Exception
     */
    private function checkDefaults($arrMetaKeys, $meta_key): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];
        $post_id = intval($data['post_id']);
        $meta_id = intval($data['meta_id']);
        $post_name = $data['post_name'];

        $default = null;

        // This function is called once per entry in wp_postmeta selected using the criteria in the mail SQL
        // it should be ordered by post_id

        $meta_key_check = $meta_key;

        // See whether this is a meta key with a section and a tab
        // these of the form resort_tabs_{overview}_sections_{section}_{meta_key}
        // For the defaults function (for now) we only want to check the overview section
        if (preg_match('/^resort_tabs_(\d+)_sections_(\d+)_(.*)$/', $meta_key, $matches)) {
            $section = intval($matches[1]);
            $tab = intval($matches[2]);
            $meta_stub = $matches[3];
            if ($section == $this->arrSectionMap['Overview']) {
                if (!key_exists($tab, $this->arrOverviewTabMap)) {
                    Utils::logger()->error('Unable to find tab ' . $tab . ' for ' . $meta_key . ', post_id ' . $post_id,
                        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__));
                    return;
                }
                $tab = $this->arrOverviewTabMap[$tab]['tab'];
                if ($tab === null) {
                    return;
                }
                $meta_key_check = $tab . '!' . $meta_stub;
            }
        }

        // See whether we Need to create an array of default checks - bear in mind that the section and tab numbers
        // depend on order of the tabs in the database and the order of the sections in the database
        // This is expensive, so we only do it when a post_id changes
        if ($this->arrMetaDataDefaultsPerResort['post_id'] != $post_id) {
            $this->arrMetaDataDefaultsPerResort['post_id'] = $post_id;
            $this->arrMetaDataDefaultsPerResort['checks'] = array();

            foreach ($this->arrMetaDataDefaults as $key => $arrDefaultData) {
                if (key_exists($post_id, $arrDefaultData)) {
                    $arrDefaults = $arrDefaultData[$post_id];
                }elseif (key_exists(0, $arrDefaultData)) {
                    $arrDefaults = $arrDefaultData[0];
                } else {
                    break;
                }
                $x = explode('!', $key);
                // Check whether this a meta key we need construct
                if (count($x) == 2) {
                    $tab = $x[0];
                    $meta_stub = $x[1];
                    $full_meta_key = $this->constructFullMetaKey('Overview', $tab, $meta_stub);
                    $this->arrMetaDataDefaultsPerResort['checks'][$full_meta_key] = $arrDefaults;
                } else {
                    $this->arrMetaDataDefaultsPerResort['checks'][$key] = $arrDefaults;
                }
            }
        }
        $arrDefaultChecks = $this->arrMetaDataDefaultsPerResort['checks'];

        $check_applied = false;
        // Check Against the fixed default values
        if (key_exists($meta_key, $arrDefaultChecks)) {
            // Defaults for this key - any for this resort?
            $default = $arrDefaultChecks[$meta_key]['meta_value'];
            $check_applied = true;
        }

        $wasregex = false;
        if (!$check_applied) {
            // Check against the regex defaults second and only if another test not found
            foreach ($this->arrMetaDataDefaultsRegex as $regex => $arrRegex) {
                if (preg_match($regex, $meta_key)) {

                    // We have a match - see if there is a resort level default
                    if (key_exists($post_id, $arrRegex)) {
                        $default = $arrRegex[$post_id]['meta_value'];
                        $wasregex = true;
                    } else {
                        // No resort specific - lets see if any defaults for all resorts
                        if (key_exists(0, $arrRegex)) {
                            $default = $arrRegex[0]['meta_value'];
                            $wasregex = true;
                        }
                    }
                }
            }
        }

        // See if we need to check
        if ($default !== null) {
            if ($meta_value != $default) {
                $field = $meta_key_check;
                if (key_exists($meta_key_check, $this->arrMetaDataDecodes)) {
                    $field = $this->arrMetaDataDecodes[$meta_key_check];
                }
                $checkType = $wasregex ? '(RX)' : '(DF)';
                $issue = $checkType . ' Value of "' . $meta_value . '" different to default "' . $default . '" - default applied';
                $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $meta_value, $data);

                // We can schedule an update
                $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $default);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function checkStringLength($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];


        // Check the length of the string
        if (key_exists('max_length', $params) && strlen($meta_value) > $params['max_length']) {
            $max_length = $params['max_length'];
            if ($max_length == 0) {
                $issue = 'Needs to be blank';
            } else {
                $issue = 'Too long (max ' . $max_length . ', now ' . strlen($meta_value) . ' )';
            }

            // if max_length > 32 Truncate the current value to 32 characters and add ellipsis
            if ($max_length > 32) {
                $current_value = substr($meta_value, 0, 32) . '...';
            } else {
                $current_value = $meta_value;
            }

            $this->issueLogger->report($resort, $tab . '-' . $field, 'checkStringLength', $issue, $current_value, $data);
        } elseif (key_exists('min_length', $params) && strlen($meta_value) < $params['min_length']) {
            $min_length = $params['min_length'];

            if ($meta_value == '') {
                $issue = 'Text is missing';
            } else {
                $issue = 'Too short (min ' . $min_length . ' characters)';
            }

            $this->issueLogger->report($resort, $tab . '-' . $field, __FUNCTION__, $issue, $meta_value, $data);
        }
    }

    /**
     * Looking for a comma followed by no space
     * e.g. “This is a sentence,This is another sentence”
     *
     * @param $arrMetaKeys
     * @param $tab
     * @param $field
     * @param $meta_key
     * @param $params
     * @return void
     * @throws Exception
     */
    private function checkCommaNoSpace($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];

        // Look for comma followed by something other than a space - except if it's a number
        if (preg_match('/,[^ 0-9]/', $meta_value)) {
            $issue = 'Comma with no space/number after it';
            if (strlen($meta_value) > 32) {
                $current_value = substr($meta_value, 0, 32) . '...';
            } else {
                $current_value = $meta_value;
            }

            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $current_value, $data);
        }
    }

    /**
     * Look for a full stop which does have a space after it or is not at the end of the string
     * e.g. “This is a sentence. This is another sentence"
     * Also check for full stop with more than one space after it
     * @throws Exception
     */
    private function checkFullStop($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];

        // Look for full stop followed by something other than a space or tab or more than one space
        if (preg_match('/[^0-9]\.[^ \t\r\n]</', $meta_value) || preg_match('/\. {2}/', $meta_value)) {
            $issue = 'Full stop with incorrect spacing';
            if (strlen($meta_value) > 32) {
                $current_value = substr($meta_value, 0, 32) . '...';
            } else {
                $current_value = $meta_value;
            }

            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $current_value, $data);
        }
    }

    /**
     * Check for Blank image or stock image
     * @param $arrMetaKeys
     * @param $tab
     * @param $field
     * @param $meta_key
     * @param $params
     * @return void
     * @throws Exception
     */
    private function checkImage($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $meta_value = $data['meta_value'];
        $post_name = $data['post_name'];
        $other_image_name = '';

        // Check for blank image, stock image or mobile image that is not a copy of the desktop image
        if ($meta_value == '') {
            $issue = 'Image missing';
        } elseif ($meta_value == '477' || $meta_value == '478') {
            $issue = 'Stock image';
        } elseif (sizeof($params) > 0) {
            $image_name = $this->arrAttachmentPosts[$meta_value]['post_title'];
            // get the first part of the image name - the string before t
            // Checking for duplicate images
            foreach ($params as $keyToCheck) {
                if ($meta_value == $arrMetaKeys[$keyToCheck]['meta_value']) {
                    $issue = 'Duplicate image with ' . $keyToCheck . ' (' . $this->arrAttachmentPosts[$meta_value]['post_title'] . ')';
                    break;
                }

                $other_image_name = $this->arrAttachmentPosts[$arrMetaKeys[$keyToCheck]['meta_value']]['post_title'];
                // We need both images names to be of the form xx-{post_name}-Header or xx-{post_name}-Mobile
                // If not them we need to fix
                if (substr($image_name, 0, 2) != substr($other_image_name, 0, 2)) {
                    $issue = 'Incorrect Mobile Image - name does not match ' . $keyToCheck . ' (' . $this->arrAttachmentPosts[$meta_value]['post_title'] . ')';
                    break;
                }
            }
            if (!isset($issue)) {
                return;
            }
        } else {
            return;
        }

        // see if we can fix this - here because there is a problem
        // The solution is to look for an image which has the same name and type
        // so 20-Header-{post_name}-Header or 20-{post_name}-Mobile
        if ($meta_key == 'resort_banner_image_desktop') {
            $arr_target_image_name[] = '20-' . $post_name . '-Header';
        } elseif ($meta_key == 'resort_banner_image_mobile') {
            $prefix = 20;
            if ($other_image_name != '') {
                $prefix = substr($other_image_name, 0, 2);
            }
            $arr_target_image_name[] = $prefix . '-' . $post_name . '-Mobile';
        } else {
            Utils::logger()->error('Unexpected keyToCheck in checkImage: ' . $meta_key);
            return;
        }

        $post_id = intval($data['post_id']);

        // Determine the generic image for the country - used if we cant find a specific image
        $countryID = $this->arrResortCountries[$post_id][0];
        $countryName = str_replace(' ', '-', strtolower($this->arrCountries[$countryID]['name']));
        if ($meta_key == 'resort_banner_image_desktop') {
            $arr_target_image_name[] = '20-' . $countryName . '-generic-Header';
        } else {
            $arr_target_image_name[] = '20-' . $countryName . '-generic-Mobile';
        }

        // Search for replacements
        $target_image_post_id = false;
        foreach($arr_target_image_name as $target_image_name) {
            foreach ($this->arrAttachmentPosts as $attachment_post) {
                if ($attachment_post['post_title'] == $target_image_name) {
                    $target_image_post_id = $attachment_post['ID'];
                    break;
                }
            }
            if ($target_image_post_id !== false) {
                break;
            }
        }

        $resort = $data['post_title'];
        if ($target_image_post_id !== false) {
            // Can fix it
            $issue .= ' - fixed with change to ' . $target_image_name;
            $meta_id = intval($data['meta_id']);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, strval($target_image_post_id));
        }
        $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $meta_value, $data);
    }

    /**
     * @throws Exception
     */
    private function checkFeaturedImage($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $meta_value = $data['meta_value'];
        $post_name = $data['post_name'];
        $featured_image_meta_key = '_thumbnail_id';

        // We want featured image to be the same as the mobile image
        if ($meta_key != 'resort_banner_image_mobile') {
            throw new Exception('Unexpected keyToCheck in checkFeaturedImage: ' . $meta_key);
        }

        $resort = $data['post_title'];
        if (!key_exists($featured_image_meta_key, $arrMetaKeys)) {
            $issue = 'Featured image missing';
            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $featured_image_meta_key, $data);
            return;
        }

        $featured_image_meta_value = $arrMetaKeys[$featured_image_meta_key]['meta_value'];
        if ($featured_image_meta_value == $meta_value) {
            return;
        }

        $issue = 'Featured image does not match mobile image';
        $featured_image_meta_id = intval($arrMetaKeys[$featured_image_meta_key]['meta_id']);
        $post_id = intval($data['post_id']);

        $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $meta_value, $data);
        $this->sqlLogger->makeMetaChange($post_name, $featured_image_meta_id, $post_id,
            $featured_image_meta_key, $meta_value);
    }

    /**
     * @throws Exception
     */
    private function checkMalformedLink($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];

        // Look for full stop followed by something other than a space or tab or more than one space
        if (preg_match('/[a-zA-Z0-9]_[a-zA-Z0-9]+_/', $meta_value)) {
            $issue = 'Malformed web link';

            if (strlen($meta_value) > 32) {
                $current_value = substr($meta_value, 0, 32) . '...';
            } else {
                $current_value = $meta_value;
            }

            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $current_value, $data);
        }
    }

    /**
     * @throws Exception
     */
    private function checkURLValid($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];

        // Find URLS embedded in meta_value
        $arrMatches = [];
        if (!preg_match_all('/href="(https?:\/\/\S+)"/', $meta_value, $arrMatches)) {
            return;
        }

        for ($i=1; $i<count($arrMatches); $i++) {
            $url = $arrMatches[$i][0];
            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                $issue = 'Invalid URL';
                $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $url, $data);
            }

            // Now check the URL is not a 404
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_exec($ch);
            $rc = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($rc != 200 && $rc != 301 && $rc != 302 && $rc != 403 && $rc != 307) {
                $issue = 'URL (' . $url . ') returns ' . $rc;
                $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, $url, $data);
            }
        }
    }
    /**
     * @throws Exception
     */
    private function checkEnabledSectionHasContent($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        // params contains array of tags to look at for content
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];
        $target_keys = $params[0];


        $content_value = '';
        foreach ($target_keys as $target_key) {
            $full_target_key = $this->constructFullMetaKey('Overview', $tab, $target_key);
            if ($target_key === null) {
                continue;
            }
            if (!key_exists($full_target_key, $arrMetaKeys)) {
                continue;
            }
            $content = $arrMetaKeys[$full_target_key]['meta_value'];
            // Get rid of any html tags in the text
            $content = preg_replace('/<[^>]+>/', '', $content);
            $content = preg_replace('/&nbsp;/', '', $content);
            // strip \r and \n
            $content = str_replace("\r", '', $content);
            $content = str_replace("\n", '', $content);

            $content_value .= $content;
            if ($content_value != '') {
                break;
            }
        }

        $issue = '';
        $target = '';
        if (($meta_value == 'show' || $meta_value == 'all') && $content_value == '') {
            $issue = 'Section enabled but has no content';
            $target = 'hide';
        } elseif ($meta_value == 'hide' && $content_value != '') {
            // Section has content but is hidden
            $issue = 'Section has content but is hidden -> length(' . strlen($content_value) . ')' . substr($content_value, 0, 150) . '...';
            $target = 'all';
        }


        if ($issue != '') {
            $issue .= ' - fix with "' . $target . '"';
            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, '', $data);

            // We can issue a fix for this - set the section to hide
            $post_id = intval($data['post_id']);
            $post_name = $data['post_name'];
            $meta_id = intval($data['meta_id']);

            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $target);
        }
    }

    /**
     * @throws Exception
     */
    private function checkMoveField($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        // params contains array of tags to look at for content
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];
        $target_field = $params[0];

        // If no value for this field, then we can't do anything
        if ($meta_value == '') {
            return;
        }
        $full_target_key = $this->constructFullMetaKey('Overview', $tab, $target_field);
        if (key_exists($full_target_key, $arrMetaKeys)) {
            $target_meta_id = $arrMetaKeys[$full_target_key]['meta_id'];
            $target_value = $arrMetaKeys[$full_target_key]['meta_value'];
        } else {
            // Shouldn't happen
            return;
        }

        if ($target_value == '') {
            $target_value = $meta_value;
        } else {
            // We have a value in the target field, so we can ignore this
            return;
        }

        $issue = 'Moving value of "' . $target_value . '" to "' . $target_field . '", and resetting source';

        $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, '', $data);

        // We can issue a fix for this - set the section to hide
        $post_id = intval($data['post_id']);
        $post_name = $data['post_name'];
        $meta_id = intval($data['meta_id']);

        $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, '');
        $this->sqlLogger->makeMetaChange($post_name, intval($target_meta_id), $post_id, $full_target_key, $target_value);
    }

    /**
     * @throws Exception
     */
    private function checkStockGallery($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        // params contains array of tags to look at for content
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];

        // If no value for this field, then we can't do anything
        if ($meta_value != 'a:4:{i:0;s:3:"521";i:1;s:3:"522";i:2;s:3:"523";i:3;s:3:"524";}') {
            return;
        }

        // Get Section display status
        $display_status_meta_key = $this->constructFullMetaKey('Overview', $tab, 'section_display_status');
        if (key_exists($display_status_meta_key, $arrMetaKeys)) {
            $display_status_meta_id = $arrMetaKeys[$display_status_meta_key]['meta_id'];
            $display_status_value = $arrMetaKeys[$display_status_meta_key]['meta_value'];
        } else {
            // Shouldn't happen
            return;
        }

        if ($display_status_value != 'hide') {
            $display_status_target_value = 'hide';
        } else {
            $display_status_target_value = '';
        }

        $issue = 'Removing Stock Gallery images';
        if ($display_status_target_value != '') {
            $issue .= ' and hiding section';
        }

        $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, '', $data);

        // We can issue a fix for this - set the section to hide
        $post_id = intval($data['post_id']);
        $post_name = $data['post_name'];
        $meta_id = intval($data['meta_id']);

        $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, '');
        if ($display_status_target_value != '') {
            $this->sqlLogger->makeMetaChange($post_name, intval($display_status_meta_id), $post_id, $display_status_meta_key, $display_status_target_value);
        }
    }


    /**
     * @throws Exception
     */
    private function checkSkiClubOfferings($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        // params contains array of tags to look at for content
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];
        $post_id = intval($data['post_id']);
        $post_name = $data['post_name'];

        // Check the section Status
        $arrSocialSectionStatus = $this->getSocialSectionStatus($arrMetaKeys);

        // Check each section again the resort status - we also handle the Holiday tab here
        $holiday_tab_meta_key = $this->constructFullMetaKey('Overview', 'Holiday', 'section_display_status');
        $holiday_tab_status = $arrMetaKeys[$holiday_tab_meta_key]['meta_value'];
        $show = false;
        $sectionsToDelete = 0;
        if ($arrMetaKeys['resort_freshtrack_holidays']['meta_value'] == 'yes') {
            if (key_exists('Freshtracks holiday', $arrSocialSectionStatus)) {
                $arrSocialSectionStatus['Freshtracks holiday']['keep'] = true;
                $show = true;
            } else {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Social - Freshtracks section missing', '', $data);
            }
            if ($holiday_tab_status == 'hide') {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Holiday Tab - not visible change to "show"', '', $data);
                $holiday_tab_meta_id = intval($arrMetaKeys[$holiday_tab_meta_key]['meta_id']);
                $this->sqlLogger->makeMetaChange($post_name, $holiday_tab_meta_id, $post_id, $holiday_tab_meta_key, 'show');
            }
            // Check whether the link is valid - format "a:3:{s:5:"title";s:13:"Find holidays";s:3:"url";s:1:"#";s:6:"target";s:0:"{url}";}
            $holiday_search_link_target_meta_value = $this->getHolidaySearchMetaValue($post_id);
            $holiday_search_link_current_meta_value = $arrSocialSectionStatus['Freshtracks holiday']['link']['meta_value'];
            if ($holiday_search_link_current_meta_value != $holiday_search_link_target_meta_value) {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Freshtracks holiday link incorrect', $holiday_search_link_current_meta_value, $data);
                $this->sqlLogger->makeMetaChange($post_name, intval($arrSocialSectionStatus['Freshtracks holiday']['link']['meta_id']),
                    $post_id, $arrSocialSectionStatus['Freshtracks holiday']['link']['meta_key'], $holiday_search_link_target_meta_value);
            }
        } elseif (key_exists('Freshtracks holiday', $arrSocialSectionStatus)) {
            $arrSocialSectionStatus['Freshtracks holiday']['keep'] = false;
            $sectionsToDelete++;
            if ($holiday_tab_status != 'hide') {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Holiday Tab - visible change to "hide"', '', $data);
                $holiday_tab_meta_id = intval($arrMetaKeys[$holiday_tab_meta_key]['meta_id']);
                $this->sqlLogger->makeMetaChange($post_name, $holiday_tab_meta_id, $post_id, $holiday_tab_meta_key, 'hide');
            }
        }

        $rep_link='';
        if ($arrMetaKeys['resort_rep_resort']['meta_value'] == 'yes') {
            if (key_exists('This is a Rep Resort', $arrSocialSectionStatus)) {
                $arrSocialSectionStatus['This is a Rep Resort']['keep'] = true;
                $show = true;
                $rep_link='/resort/' . $post_name . '/?tab=reps';
            } else {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Social - Rep section missing', '', $data);
            }
        } elseif (key_exists('This is a Rep Resort', $arrSocialSectionStatus)) {
            $arrSocialSectionStatus['This is a Rep Resort']['keep'] = false;
            $sectionsToDelete++;
        }

        if ($arrMetaKeys['resort_instructor_led_guiding_resort']['meta_value'] == 'yes') {
            if (key_exists('This is an ILG resort', $arrSocialSectionStatus)) {
                $arrSocialSectionStatus['This is an ILG resort']['keep'] = true;
                $show = true;
            } else {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Social - ILG section missing', '', $data);
            }
        } elseif (key_exists('This is an ILG resort', $arrSocialSectionStatus)) {
            $arrSocialSectionStatus['This is an ILG resort']['keep'] = false;
            $sectionsToDelete++;
        }


        $issue = '';
        $target_value = '';
        // Hide the section if its visible and all 3 subsections need hiding
        if (!$show && ($meta_value == 'show' || $meta_value == 'all')) {
            $issue .= "Ski Club Offerings Section not used - hiding";
            $target_value = 'hide';
        } else if ($show && $meta_value == 'hide') {
            $issue .= "Ski Club Offerings Section used but not visible - showing";
            $target_value = 'all';
        }

        if ($issue != '') {
            $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, '', $data);
        }
        if ($target_value != '') {
            // We can issue a fix for this - set the section to hide
            $meta_id = intval($data['meta_id']);
            $post_id = intval($data['post_id']);

            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id,
                $this->constructFullMetaKey('Overview', 'Ski Club Offerings', 'section_display_status'), $target_value);
        }

        // See whether we need to delete any of the sections - we only do this if the meta_value or target_value is not 'hide'
        if ($sectionsToDelete && $meta_value != 'hide' || ($target_value != '' && $target_value != 'hide')) {
            $this->deleteSocialSections($post_id, $resort, $arrSocialSectionStatus, $arrMetaKeys);
        }

        if ($rep_link != '') {
            // get the correct meta_key
            $meta_key = $arrSocialSectionStatus['This is a Rep Resort']['link']['meta_key'];
            $current_meta_value = $arrSocialSectionStatus['This is a Rep Resort']['link']['meta_value'];
            $new_meta_value = 'a:3:{s:5:"title";s:9:"Find Reps";s:3:"url";s:' . strlen($rep_link) . ':"' . $rep_link . '";s:6:"target";s:0:"";}';
            if ($current_meta_value != $new_meta_value) {
                $this->issueLogger->report($resort, $field, __FUNCTION__, 'Rep Link incorrect', $current_meta_value, $data);
                $this->sqlLogger->makeMetaChange($post_name, intval($arrSocialSectionStatus['This is a Rep Resort']['link']['meta_id']),
                    $post_id, $meta_key, $new_meta_value);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function checkMissingTab($arrMetaKeys, $resort_name, $post_id, $tab_name) : void
    {
        // For now just the Ikon pass - and we know if that should be there because the resort_badge has a value
        if ($tab_name != 'Ikon Pass') {
            return;
        }

        if (key_exists('resort_badge', $this->arrMetaDataDefaults)) {
            if (key_exists($post_id, $this->arrMetaDataDefaults['resort_badge'])) {
                $resort_badge = $this->arrMetaDataDefaults['resort_badge'][$post_id];
                if ($resort_badge['meta_value'] != '') {
                    // We should have an Ikon Pass tab
                    $data = $arrMetaKeys['resort_badge'];
                    $post_name = $data['post_name'];
                    $this->issueLogger->report($resort_name, 'resort_badge', __FUNCTION__, 'Ikon Pass tab missing - FIX', '', $data);
                    $this->sqlLogger->addTabSection($post_name, intval($post_id), $arrMetaKeys,
                        $this->arrSectionMap['Overview'], 'data/ikon_meta.csv',
                        $this->arrTargetTabMapByName['Ikon Pass']);
                }
            }
        }
    }
    /**ƒ
     * @throws Exception
     */
    private function deleteSocialSections(int $post_id, string $resort, array $arrSocialSectionStatus, array $arrMetaKeys) : void
    {
        // This is messy as we need to then rename remaining meta keys so the section count starts at 0
        $section = 0;
        $arrMetaKeysTargets = array();
        foreach ($arrSocialSectionStatus as $section_name => $section_data) {
            if ($section_data['keep']) {
                $arrSocialSectionStatus[$section_name]['target_section'] = $section++;
            } else {
                $arrSocialSectionStatus[$section_name]['target_section'] = -1;
            }

            foreach(array('content', 'heading', 'heading_size', 'heading_tag', 'image', 'link') as $meta_key) {
                $tab = $section_data['tab'];
                $meta_key_now = $this->constructFullMetaKey(
                    'Overview',
                    'Ski Club Offerings',
                    'component_ski_club_offerings_teasers_' . $tab . '_' .$meta_key);
                if ($arrSocialSectionStatus[$section_name]['target_section'] >= 0) {
                    $meta_key_new = $this->constructFullMetaKey(
                        'Overview',
                        'Ski Club Offerings',
                        'component_ski_club_offerings_teasers_' . $arrSocialSectionStatus[$section_name]['target_section'] . '_' .$meta_key);
                    $arrMetaKeys[$meta_key_new] = $arrMetaKeys[$meta_key_now];
                } else {
                    $meta_key_new = null;
                }
                $arrMetaKeysTargets[$meta_key_now] = array(
                    'meta_id' => $arrMetaKeys[$meta_key_now]['meta_id'],
                    'meta_key_new' => $meta_key_new,
                );

                // Add the meta_key for the _ version of the meta_key
                $meta_key_now = '_'. $meta_key_now;
                if ($meta_key_new) {
                    $meta_key_new = '_' . $meta_key_new;
                }
                $arrMetaKeysTargets[$meta_key_now] = array(
                    'meta_id' => $arrMetaKeys[$meta_key_now]['meta_id'],
                    'meta_key_new' => $meta_key_new,
                );
            }
        }

        // Set the meta_key indicating the number sections - resort_tabs_0_sections_0_component_ski_club_offerings_teasers
        $meta_key_teasers = $this->constructFullMetaKey('Overview', 'Ski Club Offerings', 'component_ski_club_offerings_teasers');
        $meta_id_teasers = $arrMetaKeys[$meta_key_teasers]['meta_id'];

        $this->sqlLogger->deleteSocialSection($post_id, $resort, $arrMetaKeysTargets,
            $meta_key_teasers, $meta_id_teasers, $section);
    }
    /**
     * @throws Exception
     */
    private function getSocialSectionStatus(array $arrMetaKeys): array
    {
        $arrSocialSectionStatus = array();
        // Iterate through the appropriate social section and see if any of the fields set
        $base_meta_key = $this->constructFullMetaKey('Overview', 'Ski Club Offerings', 'component_ski_club_offerings_teasers');

        foreach(array('Freshtracks holiday', 'This is a Rep Resort', 'This is an ILG resort') as $social_section_type) {
            for ($i=0; $i<=2; $i++) {
                $meta_key = $base_meta_key . '_' . $i . '_heading';
                if (key_exists($meta_key, $arrMetaKeys)) {
                    $heading =  $arrMetaKeys[$meta_key]['meta_value'];
                    if ($heading == $social_section_type) {
                        $arrSocialSectionStatus[$social_section_type]['tab'] = $i;
                        $arrSocialSectionStatus[$social_section_type]['content'] = $arrMetaKeys[$base_meta_key . '_' . $i . '_content'];
                        $arrSocialSectionStatus[$social_section_type]['link'] = $arrMetaKeys[$base_meta_key . '_' . $i . '_link'];
                    }
                }
            }
        }
        return $arrSocialSectionStatus;

    }

    /**
     * @param $arrMetaKeys
     * @param $tab
     * @param $field
     * @param $meta_key
     * @param $params - array of regular expressions to apply to the field
     * @throws Exception
     */
    private function checkRegEx($arrMetaKeys, $tab, $field, $meta_key, $params): void
    {
        // params contains array of tags to look at for content
        $data = $arrMetaKeys[$meta_key];
        $resort = $data['post_title'];
        $meta_value = $data['meta_value'];
        $target_meta_value = trim($meta_value);

        $arrRegEx = $params[0];

        $total_changes = 0;
        foreach ($arrRegEx as $regEx) {
            $findPattern = '/' . $regEx[0] . '/';
            $replacePattern = $regEx[1] ;
            $count = 0;
            $target_meta_value = preg_replace($findPattern, $replacePattern, $target_meta_value, -1, $count);
            $total_changes += $count;
        }

        // Has anything changed
        if ($total_changes == 0) {
            return;
        }

        $issue = $total_changes . ' changes made to "' . $field . '"';

        $this->issueLogger->report($resort, $field, __FUNCTION__, $issue, '', $data);

        // We can issue a fix for this - set the section to hide
        $post_id = intval($data['post_id']);
        $post_name = $data['post_name'];
        $meta_id = intval($data['meta_id']);

        $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $target_meta_value);
    }

    /**
     * Check:
     * - The resort has the correct country (using the Weather API details)
     * - The resort has (only 1) the country rather than region set as yeoast primary location
     * @throws Exception
     */
    private function checkCountryDetails(string $resort, array $arrMetaKeys) : void
    {
        // Get the first element of the array arrMetaKeys
        $first_element = reset($arrMetaKeys);
        $post_id = $first_element['post_id'];
        $post_name = $first_element['post_name'];
        $data = $this->arrResortPosts[$post_id];
        $weatherAPIID = $arrMetaKeys['resort_weather_resort_api_id']['meta_value'];
        $yoast_wpseo_primary_country = $arrMetaKeys['_yoast_wpseo_primary_country']['meta_value'];

        // Check one and only one country set
        if (!key_exists($post_id, $this->arrResortCountries))
        {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                'CountryTaxonomy not set.', '', $data);
            // Cant continue
            return;
        } elseif (count($this->arrResortCountries[$post_id]) !== 1)
        {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                'Incorrect number of countries set -> ' . count($this->arrResortCountries[$post_id]), '', $data);
            // Cant continue
            return;
        } else
        {
            $country = $this->arrResortCountries[$post_id][0];

            if ($country != $yoast_wpseo_primary_country)
            {
                if ($yoast_wpseo_primary_country != '')
                {
                    $issue = 'CountryTaxonomy ' . $this->arrCountries[$yoast_wpseo_primary_country]['name'] .
                        ' set as  primary location.  Change to ' .
                        $this->arrCountries[$country]['name'];
                } else {
                    $issue = 'No primary location set.  Set to ' .
                        $this->arrCountries[$country]['name'];
                }
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, $issue , '', $data);
                $meta_id = intval($arrMetaKeys['_yoast_wpseo_primary_country']['meta_id']);
                $post_id = intval($arrMetaKeys['_yoast_wpseo_primary_country']['post_id']);

                $this->sqlLogger->makeMetaChange(
                    $post_name, $meta_id, $post_id, '_yoast_wpseo_primary_country', $country);
            }
        }

        if ($weatherAPIID != '')
        {
            if (key_exists($weatherAPIID, $this->arrResortsByWeatherAPI))
            {
                if ($this->arrResortsByWeatherAPI[$weatherAPIID]['name'] != $resort)
                {
                    // Names don't match
                    $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                        'Weather API ID ' . $weatherAPIID . ' set for ' .
                        $this->arrResortsByWeatherAPI[$weatherAPIID]['name'], '', $data);
                } elseif ($this->arrResortsByWeatherAPI[$weatherAPIID]['country'] != $this->arrCountries[$country]['name'])
                {
                    // CountryTaxonomy doesn't match
                    $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                        'Weather API ' . $weatherAPIID . ' set for ' .
                        $this->arrResortsByWeatherAPI[$weatherAPIID]['name'] . '/' .
                        $this->arrResortsByWeatherAPI[$weatherAPIID]['country'] .
                        ' but country is set to ' .
                        $this->arrCountries[$country]['name'], '', $data);
                }
            } else
            {
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                    'Invalid Weather API ID ' . $weatherAPIID, '', $data);
            }
        } else {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'No Weather API ID set', '', $data);
        }
    }

    /**
     * @throws Exception
     */
    public function runResortChecks($resort, $arrMetaKeys) : void
    {
        // Get the first element of the array arrMetaKeys
        $first_element = reset($arrMetaKeys);
        $post_id = $first_element['post_id'];
        $post_name = $first_element['post_name'];
        $resort_data = $this->arrResortPosts[$post_id];

        $slug = $resort_data['post_name'];
        $valid_slug = Utils::create_post_name($resort_data['post_title']);
        if ($slug != $valid_slug) {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__,
                'slug is not valid. Change from "' . $slug . '" to "' . $valid_slug . '" ',
                '',
                $resort_data
            );
            $this->sqlLogger->updatePostSlug($slug, $post_id, $valid_slug);

        }

        // Check there is content in the resort overview - look in the following tags 'resort_intro_text',
        // '_heading', '_component_resort_overview_content',
        $content = false;
        $meta_key_resort_overview = $this->constructFullMetaKey('Overview', 'Resort Overview', 'heading');
        $meta_key_resort_overview_content = $this->constructFullMetaKey('Overview', 'Resort Overview', 'component_resort_overview_content');
        foreach(array('resort_intro_text', $meta_key_resort_overview, $meta_key_resort_overview_content)
                as $meta_key) {
            if (key_exists($meta_key, $arrMetaKeys) && $arrMetaKeys[$meta_key]['meta_value'] != '') {
                $content = true;
                break;
            }
        }
        if (!$content) {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'No content in the Resort - Can make private', '',
                $resort_data
            );
        }

        // Hide Resort you may also like - for now - should always be section_0 - but just to be sure
        $status_key = '';
        for ($i = 0; $i< 5; $i++) {
            $header_key = 'sections_' . $i . '_heading';

            if (key_exists($header_key, $arrMetaKeys) && $arrMetaKeys[$header_key]['meta_value'] == 'Resorts You may also like') {
                $status_key = 'sections_' . $i . '_section_display_status';
                if ($arrMetaKeys[$status_key]['meta_value'] == 'all') {
                    break;
                }
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Resort You May Like - Section not visible', '', $resort_data);

                $status_meta_id = intval($arrMetaKeys[$status_key]['meta_id']);
                $this->sqlLogger->makeMetaChange($post_name, $status_meta_id, intval($post_id), $status_key, 'all');
                break;
            }
        }

        if ($status_key == '') {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'No section "Resort you may also like"', '',
                array('post_id' => $post_id, 'post_name' => $resort)
            );
        }

        // Do the Tab Order Checks - Need to be careful here. These can only be run as long as nothing else has changed
        // TODO Implement Block if meta_keys updated or deleted
        //$this->checkTabOrder($reporter, $resort, $arrMetaKeys);

        // Check CountryTaxonomy Details
        $this->checkCountryDetails($resort, $arrMetaKeys);

        // Check The Maps Section
        $this->checkMapsSection($resort, $arrMetaKeys);

        // Delete unused section tabs (At the top of the resort screen
        $this->deleteUnusedSectionTabs($resort, $arrMetaKeys);

        // Make sure Weather Elevations correct
        $this->checkWeatherTab($resort, $arrMetaKeys);

        // Make sure Snow Overview correct
        $this->checkSnowOverview($resort, $arrMetaKeys);


        //$this->oneShotWeatherAdd($resort, $arrMetaKeys);

        // Check the Media Folders correctly populated
        //$this->checkMediaFolders($reporter, $resort, $arrMetaKeys);
    }

    private function oneShotWeatherAdd(string $resort, array $arrResortMeta) : void
    {
        if (key_exists('resort_tabs_2_sections_0_component_resort_weather_layout', $arrResortMeta)) {
            return;
        }
        Utils::logger()->info('One Shot weather add for ' . $resort);

        $post_id = $arrResortMeta['resort_tabs']['post_id'];
        if ($post_id == '1270') {
            // This is the model
            return;
        }

        // Clean up fatmap data
        $sql = "START TRANSACTION;\n";

        // Clean up stray fat map data
        if (key_exists('resort_tabs_2_sections_0_component_resort_map_images', $arrResortMeta)) {
            $sql .= "DELETE FROM wp_postmeta WHERE meta_key = 'resort_tabs_2_sections_0_component_resort_map_images' AND post_id = " . $post_id . ";\n";
            $sql .= "DELETE FROM wp_postmeta WHERE meta_key = '_resort_tabs_2_sections_0_component_resort_map_images' AND post_id = " . $post_id . ";\n";
        }
        if (key_exists('resort_tabs_2_sections_1_component_resort_map_embed_code', $arrResortMeta)) {
            $sql .= "DELETE FROM wp_postmeta WHERE meta_key = 'resort_tabs_2_sections_1_component_resort_map_embed_code' AND post_id = " . $post_id . ";\n";
            $sql .= "DELETE FROM wp_postmeta WHERE meta_key = '_resort_tabs_2_sections_1_component_resort_map_embed_code' AND post_id = " . $post_id . ";\n";
        }

        // update the section map
        $sql .= "UPDATE wp_postmeta set meta_value = 'a:5:{i:0;s:24:\"component_resort_weather\";i:1;s:24:\"component_resort_weather\";i:2;s:24:\"component_resort_weather\";i:3;s:8:\"template\";i:4;s:24:\"component_resort_weather\";}' ";
        $sql .= "WHERE meta_key = 'resort_tabs_2_sections' AND post_id = " . $post_id . ";\n";

        // finally copy across the
        $sql .= "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) select " . $post_id . ", meta_key, meta_value from wp_postmeta where post_id = 1270 " .
            "and (meta_key like 'resort_tabs_2_sections_%' or meta_key like '\_resort_tabs_2_sections_%');\n";
        $sql .= "COMMIT;\n";

        // write the sql to a file 'add_weather.sql
        $file = fopen('add_weather.sql', 'a');
        fwrite($file, $sql);
        fclose($file);
    }


    /**
     * @throws Exception
     */
    private function checkMapsSection($resort, $arrMetaKeys) : void
    {
        // Do we have anything to do - looking for three things:
        // An Attachment with the name 'Map-Web-${post_name}.jpg'
        // Whether there is a FatMap for this resort
        // Whether there is a PDF - name pf the form 'Map-${post_name}.pdf'
        $data = $arrMetaKeys['resort_tabs'];
        $post_name = $data['post_name'];
        $post_id = $data['post_id'];
        $map_name = 'Map-Web-' . $post_name;
        $map_image_post_id = $this->findMedia($map_name, 'image/jpeg');
        $pdf_doc_name = 'Map-' . $post_name;
        $map_pdf_post_id = $this->findMedia($pdf_doc_name, 'application/pdf');

        // Check if there is a fat map
        $overview_map_meta_key = $this->constructFullMetaKey('Overview', 'Resort Map', 'component_resort_map_embed_code');
        $fatmap = '';
        if (key_exists($overview_map_meta_key, $arrMetaKeys)) {
            $fatmap = $arrMetaKeys[$overview_map_meta_key]['meta_value'];
            if ($fatmap != '' && !str_contains($fatmap, 'fatmap.com')) {
                $fatmap = '';
            }
        }


        // Now look for a meta_key of the form resort_tabs_%d_tab_name with meta_value 'Maps'
        $maps_tab_key = '';
        foreach ($arrMetaKeys as $meta_key => $data) {
            if (str_starts_with($meta_key, 'resort_tabs_') && str_ends_with($meta_key, '_tab_name') &&
                $data['meta_value'] == 'Maps') {
                $maps_tab_key = $meta_key;
                break;
            }
        }

        // get the section number from the meta_key
        if ($maps_tab_key != '') {
            $section_number = intval(substr($maps_tab_key, 12, 1));
        } else {
            //$this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'No Maps Tab', '', $data);
            // This does not have to be there
            return;
        }

        // Do some checks - not going to fix anything as this should all be OK!
        $check_meta_key = 'resort_tabs_' . $section_number . '_sections';
        if (!key_exists($check_meta_key, $arrMetaKeys) || $arrMetaKeys[$check_meta_key]['meta_value'] != 'a:3:{i:0;s:20:"component_resort_map";i:1;s:20:"component_resort_map";i:2;s:20:"component_resort_map";}') {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Maps Section Not Fully Formed - Missing elements', '', $data);
        }

        if ($map_image_post_id === null && $map_pdf_post_id === null) {
            // Check whether any sections to hide
            $changed = false;
            for ($i=0; $i<3; $i++) {
                $section_key = 'resort_tabs_' . $section_number . '_sections_' . $i . '_section_display_status';
                if (key_exists($section_key, $arrMetaKeys) && $arrMetaKeys[$section_key]['meta_value'] != 'hide') {
                    $status_meta_id = intval($arrMetaKeys[$section_key]['meta_id']);
                    $this->sqlLogger->makeMetaChange($post_name, $status_meta_id, intval($post_id), $section_key, 'hide');
                    $changed = true;
                }
            }
            if ($changed) {
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Maps Section - Sections to be hidden', '', $data);
            }
            return;

        }

        // Setup section 0 - the map image - we may have to create the meta keys so this could take two runs
        $arrFixes = array();
        $map_image_meta_value = 'a:1:{i:0;s:' . strlen(strval($map_image_post_id)) . ':"' . $map_image_post_id . '";}';
        $map_image_meta_key = 'resort_tabs_' . $section_number . '_sections_0_component_resort_map_images';
        $cont = false;
        if (!key_exists($map_image_meta_key, $arrMetaKeys)) {
            $cont = true;
        }

        if ($cont || $arrMetaKeys[$map_image_meta_key]['meta_value'] != $map_image_meta_value) {
            $arrFixes[$map_image_meta_key] = $map_image_meta_value;
            $arrFixes['resort_tabs_' . $section_number . '_sections_0_component_resort_map_layout'] = '2';
            $arrFixes['resort_tabs_' . $section_number . '_sections_0_heading'] = $resort . ' Piste Map';
            $arrFixes['resort_tabs_' . $section_number . '_sections_0_section_display_status'] = 'all';
            $cont = false;
        }

        if ($fatmap != '') {
            $fatmap_meta_key = 'resort_tabs_' . $section_number . '_sections_1_component_resort_map_embed_code';
            if (!key_exists($fatmap_meta_key, $arrMetaKeys)) {
                $cont = true;
            }
            if ($cont || $arrMetaKeys[$fatmap_meta_key]['meta_value'] != $fatmap) {
                $arrFixes[$fatmap_meta_key] = $fatmap;
                $arrFixes['resort_tabs_' . $section_number . '_sections_1_component_resort_map_layout'] = '1';
                $arrFixes['resort_tabs_' . $section_number . '_sections_1_heading'] = $resort . ' FATMAP';
                $arrFixes['resort_tabs_' . $section_number . '_sections_1_section_display_status'] = 'all';
                $cont = false;
            }
        } elseif ($arrMetaKeys['resort_tabs_' . $section_number . '_sections_1_section_display_status']['meta_value'] != 'hide') {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Maps Section - Fatmap Not Found - Hide It', '', $data);
            $arrFixes['resort_tabs_' . $section_number . '_sections_1_section_display_status'] = 'hide';
        }
        // Now check the PDF & strip off everything up until /wp-content
        $map_pdf_guid = $this->arrAttachmentPosts[$map_pdf_post_id]['guid'];
        $map_pdf_guid = substr($map_pdf_guid, strpos($map_pdf_guid, '/wp-content'));
        $map_pdf_name = $resort . ' PDF';

        // Construct the meta_value - a:3:{s:5:"title";s:18:"Bad Hofgastein PDF";s:3:"url";s:83:"https://newsite.skiclub.co.uk/wp-content/uploads/2023/09/Gastein-Piste-Map-2022.pdf";s:6:"target";s:6:"_blank";}
        $map_pdf_meta_key = 'resort_tabs_' . $section_number . '_sections_2_component_resort_map_pdf_link';
        $map_pdf_meta_value = 'a:3:{s:5:"title";s:' . strlen($map_pdf_name) . ':"' . $map_pdf_name .
            '";s:3:"url";s:' . strlen($map_pdf_guid) . ':"' . $map_pdf_guid . '";s:6:"target";s:6:"_blank";}';

        if (!key_exists($map_pdf_meta_key, $arrMetaKeys)) {
            $cont = true;
        }
        if ($cont || $arrMetaKeys[$map_pdf_meta_key]['meta_value'] != $map_pdf_meta_value) {
            $arrFixes[$map_pdf_meta_key] = $map_pdf_meta_value;
            $arrFixes['resort_tabs_' . $section_number . '_sections_2_component_resort_map_layout'] = '3';
            $arrFixes['resort_tabs_' . $section_number . '_sections_2_heading'] = 'Access the map in PDF format';
            $arrFixes['resort_tabs_' . $section_number . '_sections_2_component_resort_map_sub_heading'] = 'Download, print and share';
            $arrFixes['resort_tabs_' . $section_number . '_sections_2_section_display_status'] = 'all';
        }

        if (count($arrFixes) > 0) {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Maps Section - Things To Do', '', $data);
            if ($this->sqlLogger->makeMultipleMetaChanges($arrFixes, $arrMetaKeys)) {
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Maps Section - Meta Keys Need Generating - Run SQL and run again', '', $data);
            }
        }
    }

    private function findMedia(string $post_title, string $mime_type) : ?int
    {
        $post_id = null;

        foreach ($this->arrAttachmentPosts as $attachment_post) {
            if ($attachment_post['post_title'] == $post_title && $attachment_post['post_mime_type'] == $mime_type)  {
                $post_id = intval($attachment_post['ID']);
                break;
            }
        }
        return $post_id;
    }

    /**
     * Create WordPress URL String from the resort name
     * url is /holidays/holidays-result/?date=25%2F09%2F2023&resort={post_id}
     * return string is a:3:{s:5:"title";s:13:"Find holidays";s:3:"url";s:1:"{url}";s:6:"target";s:0:"";}
     * @throws Exception
     */
    private function getHolidaySearchMetaValue($post_id) : string
    {
        // Get today's date in format YY/MM/DD and convert it to url format
        date_default_timezone_set('Europe/London');
        $url = '/holidays/holidays-result/?resort=' . $post_id;

        return Utils::constructWPURLString(array(
            'title' => 'Find holidays',
            'url' => $url,
            'target' => ''
        ));
    }

/**
     * @throws Exception
     */
    private function deleteUnusedSectionTabs(string $resort, array $arrMetaKeys) : void
    {
        // Iterate through the section map

        $arrSectionsToKeep = $this->arrSectionMap;
        $sections_being_deleted = '';

        foreach($this->arrSectionMap as $section_name => $current_position) {
            if ($section_name == 'Overview' || $section_name == 'Weather' || $section_name == 'Discount') {
                // These will always be here
                continue;
            } elseif ($section_name == 'Maps') {
                // If the file maps.txt exists then it contains resorts with Map sections to delete
                if (file_exists('maps.txt')) {
                    $arrMaps = file('maps.txt', FILE_IGNORE_NEW_LINES);
                    if (in_array($resort, $arrMaps)) {
                        unset($arrSectionsToKeep[$section_name]);
                        $sections_being_deleted .= $section_name . ', ';

                    }
                }
            } elseif ($section_name == 'Community') {
                // This always goes for now - can be re-added later - delete from the array
                unset($arrSectionsToKeep[$section_name]);
                $sections_being_deleted .= $section_name . ', ';
            } elseif ($section_name == 'Instructor-led guiding' && $arrMetaKeys['resort_instructor_led_guiding_resort']['meta_value'] == 'no') {
                // The rest depend on the resort
                unset($arrSectionsToKeep[$section_name]);
                $sections_being_deleted .= $section_name . ', ';
            } elseif ($section_name == 'Reps' && $arrMetaKeys['resort_rep_resort']['meta_value'] == 'no') {
                unset($arrSectionsToKeep[$section_name]);
                $sections_being_deleted .= $section_name . ', ';
            } elseif ($section_name == 'Freshtracks' && $arrMetaKeys['resort_freshtrack_holidays']['meta_value'] == 'no') {
                unset($arrSectionsToKeep[$section_name]);
                $sections_being_deleted .= $section_name . ', ';
            }
        }

        if ($sections_being_deleted != '') {
            // Remove the trailing comma
            $sections_being_deleted = substr($sections_being_deleted, 0, -2);
            $report_data = $arrMetaKeys['resort_tabs'];

            // Create an array of old to new positions
            $arrOldToNewPositions = array();
            foreach ($this->arrSectionMap as $section_name => $current_position) {
                if (key_exists($section_name, $arrSectionsToKeep)) {
                    $arrOldToNewPositions[$current_position] = array('name' => $section_name, 'new_position' => $current_position);
                } else {
                    $arrOldToNewPositions[$current_position] = array('name' => $section_name, 'new_position' => -1);
                }
            }
            ksort($arrOldToNewPositions);
            $offset = 0;
            foreach ($arrOldToNewPositions as $current_position => $data) {
                if ($data['new_position'] == -1) {
                    // This is one to delete
                    $offset++;
                } elseif ($current_position != $data['new_position'] - $offset) {
                    // This is one to move
                    $arrOldToNewPositions[$current_position]['new_position'] = $data['new_position'] - $offset;
                }
            }
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Delete unused section Tabs - ' . $sections_being_deleted, '', $report_data);
            $this->sqlLogger->deleteUnusedSectionTabs($arrOldToNewPositions, $arrMetaKeys);
        }
    }

    /**
     * @throws Exception
     */
    private function confirmTabOrder($resort, $arrMetaKeys) : void
    {
        $arrMoves = array();

        // This will all be done in one transaction in SQLLogger. The challenge is that not all tabs will need to be
        // updated. To avoid index clashes we will do things in the following order - once we have determined what needs
        // changing
        // 1. Create entries in the audit table for the changes
        // 2. Delete the existing meta_keys - using meta_id, meta_key and post_id - just to be sure!
        // 3. Create the new meta_keys - this will generate new meta_ids
        // We now have to iterate through the meta_keys and see if the tab changes
        foreach ($arrMetaKeys as $meta_key => $data) {

            // only looking for meta_keys that start with resort_tabs or _resort_tabs
            if (!str_starts_with($meta_key, 'resort_tabs') && !str_starts_with($meta_key, '_resort_tabs')) {
                continue;
            }

            // Get the section number and tab number
            preg_match('/^_*resort_tabs_(\d)_sections_(\d+)_/', $meta_key, $matches);
            if (count($matches) != 3) {
                continue;
            }

            $section = intval($matches[1]);
            $tab = intval($matches[2]);

            if ($section != 0) {
                continue;
            }

            if (!key_exists($tab, $this->arrOverviewTabMap)) {
                Utils::logger()->emergency($resort . ' - Unexpected format for meta_key ' . $meta_key . '. Tab ' . $tab . ' not found in arrTargetForTab');
                throw new Exception('Unexpected format for meta_key ' . $meta_key . '. Tab ' . $tab . ' not found in arrTargetForTab');
            }

            if (!key_exists('target_pos', $this->arrOverviewTabMap[$tab])) {
                continue;
            }
            $target_pos = $this->arrOverviewTabMap[$tab]['target_pos'];
            if ($target_pos == $tab) {
                // Nothing to do
                continue;
            }

            if (!key_exists($tab, $arrMoves)) {
                $arrMoves[$tab] = true;
                $this->issueLogger->report(
                    $resort,
                    'Resort Checks',
                    __FUNCTION__,
                    'Tab ' . $this->arrOverviewTabMap[$tab]['tab'] . ' needs to be moved to ' . $this->arrOverviewTabMap[$target_pos]['tab'] .
                        ' (' . array_search($this->arrOverviewTabMap[$target_pos]['tab'], array_keys(self::arrOverviewData))+1 . ')',
                    '',
                    $data
                );
            }

            // TODO - Add to SQLLogger
        }
    }

    /**
     * @throws Exception
     */
    private function checkWeatherTab($resort, $arrMetaKeys) :void
    {
        $weather_api_id = $arrMetaKeys['resort_weather_resort_api_id']['meta_value'];
        $post_name = $arrMetaKeys['resort_tabs']['post_name'];
        $post_id = intval($arrMetaKeys['resort_tabs']['post_id']);

        if ($weather_api_id == '') {
            return;
        }
        if (!key_exists($weather_api_id, $this->arrResortsByWeatherAPI)) {
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Invalid Weather API ID ' . $weather_api_id, '', $arrMetaKeys);
            return;
        }
        // Check the positioning is correct - Weather Tab may not exist
        if (!key_exists('resort_tabs_2_tab_name', $arrMetaKeys) ||
            $arrMetaKeys['resort_tabs_2_tab_name']['meta_value'] != 'Weather') {
            return;
        }
        $lower_elevation = $this->arrResortsByWeatherAPI[$weather_api_id]['lower'];
        $lower_elevation_string = 'Resort (' .$lower_elevation . 'm)';
        $upper_elevation = $this->arrResortsByWeatherAPI[$weather_api_id]['upper'];
        $upper_elevation_string = 'Top (' .$upper_elevation . 'm)';
        $got_api_data = $this->arrResortsByWeatherAPI[$weather_api_id]['got_api_data'];

        if (!key_exists('resort_tabs_2_sections_0_heading', $arrMetaKeys) ||
            !key_exists('resort_tabs_2_sections_1_heading', $arrMetaKeys)) {
            Utils::logger()->error('Resort: ' . $resort . ' - Missing Weather Elevation Meta Keys');
            return;
        }

        $current_meta_value = $arrMetaKeys['resort_tabs_2_sections_0_section_display_status']['meta_value'];
        if ($upper_elevation - $lower_elevation < 300) {
            if ($current_meta_value != 'hide') {
                $data = $arrMetaKeys['resort_tabs_2_sections_0_section_display_status'];
                $meta_id = intval($arrMetaKeys['resort_tabs_2_sections_0_section_display_status']['meta_id']);
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Elevation difference too small - ' . $upper_elevation . ' - ' . $lower_elevation, '', $data);
                $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, 'resort_tabs_2_sections_0_section_display_status', 'hide');
            }
        } else {
            if ($current_meta_value == 'hide') {
                $meta_id = intval($arrMetaKeys['resort_tabs_2_sections_0_section_display_status']['meta_id']);
                $data = $arrMetaKeys['resort_tabs_2_sections_0_section_display_status'];
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Show Upper Elevation', '', $data);
                $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, 'resort_tabs_2_sections_0_section_display_status', 'all');
            }
        }

        $current_lower_elevation_string = $arrMetaKeys['resort_tabs_2_sections_1_heading']['meta_value'];
        $current_upper_elevation_string = $arrMetaKeys['resort_tabs_2_sections_0_heading']['meta_value'];

        if ($current_lower_elevation_string != $lower_elevation_string) {
            $meta_id = intval($arrMetaKeys['resort_tabs_2_sections_1_heading']['meta_id']);
            $data = $arrMetaKeys['resort_tabs_2_sections_1_heading'];
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Lower Elevation - ' . $current_lower_elevation_string . ' should be ' . $lower_elevation_string, '', $data);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, 'resort_tabs_2_sections_1_heading', $lower_elevation_string);
        }
        if ($current_upper_elevation_string != $upper_elevation_string) {
            $meta_id = intval($arrMetaKeys['resort_tabs_2_sections_0_heading']['meta_id']);
            $data = $arrMetaKeys['resort_tabs_2_sections_0_heading'];
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Upper Elevation - ' . $current_upper_elevation_string . ' should be ' . $upper_elevation_string, '', $data);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, 'resort_tabs_2_sections_0_heading', $upper_elevation_string);
        }

        // Finally see if we show the snow report
        $meta_key = 'resort_tabs_2_sections_2_section_display_status';
        $current_meta_value = $arrMetaKeys[$meta_key]['meta_value'];
        if ($current_meta_value == 'hide' && $got_api_data) {

            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
            $data = $arrMetaKeys[$meta_key];
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Show Snow Report', '', $data);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, 'all');
        } else {
            if ($current_meta_value != 'hide' && !$got_api_data) {
                $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
                $data = $arrMetaKeys[$meta_key];
                $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Hide Snow Report', '', $data);
                $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, 'hide');
            }
        }
        $meta_key='resort_tabs_2_sections_2_heading';
        $current_meta_value = $arrMetaKeys[$meta_key]['meta_value'];
        $snow_report_string = 'Resort Conditions';
        if ($current_meta_value != $snow_report_string) {
            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
            $data = $arrMetaKeys[$meta_key];
            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Snow Report Heading - ' . $current_meta_value . ' should be ' . $snow_report_string, '', $data);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $snow_report_string);
        }
//        // Make sure the remaining 2 sections hidden for now
//        $meta_key = 'resort_tabs_2_sections_3_section_display_status';
//        $current_meta_value = $arrMetaKeys[$meta_key]['meta_value'];
//        if ($current_meta_value != 'hide') {
//            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
//            $data = $arrMetaKeys[$meta_key];
//            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Hiding weather section', '', $data);
//            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, 'hide');
//        }
//        $meta_key = 'resort_tabs_2_sections_4_section_display_status';
//        $current_meta_value = $arrMetaKeys[$meta_key]['meta_value'];
//        if ($current_meta_value != 'hide') {
//            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
//            $data = $arrMetaKeys[$meta_key];
//            $this->issueLogger->report($resort, 'Resort Checks', __FUNCTION__, 'Hiding weather section', '', $data);
//            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, 'hide');
//        }
     }

    /**
     * @throws Exception
     */
    private function checkSnowOverview($resort, $arrMetaKeys) : void
    {
        $post_id = intval($arrMetaKeys['resort_tabs']['post_id']);
        $post_name = $arrMetaKeys['resort_tabs']['post_name'];

        if ($post_id == 1236) {
            // This is the model
            return;
        }

        if (!key_exists('Weather', $this->arrSectionMap)) {
            return;
        }
        $weather_section = $this->arrSectionMap['Weather'];

        // be paranoid and check the order
        $meta_key = 'resort_tabs_' . $weather_section . '_sections_0_heading';
        if (!str_starts_with($arrMetaKeys[$meta_key]['meta_value'], 'Top')) {
            Utils::logger()->error('Resort: ' . $resort . ' - Weather - Section 0 in bad position');
            return;
        }
        $meta_key = 'resort_tabs_' . $weather_section . '_sections_1_heading';
        if (!str_starts_with($arrMetaKeys[$meta_key]['meta_value'], 'Resort')) {
            Utils::logger()->error('Resort: ' . $resort . ' - Weather - Section 1 in bad position');
        }
        $meta_key = 'resort_tabs_' . $weather_section . '_sections_2_heading';
        if (!str_starts_with($arrMetaKeys[$meta_key]['meta_value'], 'Resort Conditions')) {
            Utils::logger()->error('Resort: ' . $resort . ' - Weather - Section 2 in bad position');
            return;
        }

        // Get the appropriate post id for the section overview - based on our country
        $country = CountryTaxonomy::getCountryByPostID($post_id)->getTaxonomyName();
        $overview_post_id = self::SNOW_OVERVIEW[$country];
        $overview_post_status = 'all';
        if ($overview_post_id == "-") {
            $overview_post_id = '';
            $overview_post_status = 'hide';
        }

        $meta_key = 'resort_tabs_' . $weather_section . '_sections_3_template_id';
        if ($arrMetaKeys[$meta_key]['meta_value'] == $overview_post_id) {
            return;
        }


        // OK good - delete the existing section 4 and replace it with the one from Zermatt
        $meta_key = 'resort_tabs_' . $weather_section . '_sections';
        $meta_value = 'a:5:{i:0;s:24:"component_resort_weather";i:1;s:24:"component_resort_weather";i:2;s:24:"component_resort_weather";i:3;s:8:"template";i:4;s:8:"template";}';
        $current_meta_value = $arrMetaKeys[$meta_key]['meta_value'];
        if ($current_meta_value != $meta_value) {
            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $meta_value);
        }

        $meta_key_start = 'resort_tabs_' . $weather_section . '_sections_4_';
        $meta_key = $meta_key_start . 'section_display_status';
        $data = $arrMetaKeys[$meta_key];
        $this->issueLogger->report($post_name, 'Resort Checks', __FUNCTION__, 'Adding Snow Overview', '', $data);

        foreach($arrMetaKeys as $meta_key => $data) {
            if (str_starts_with($meta_key, $meta_key_start) || str_starts_with($meta_key, '_' . $meta_key_start)) {
                $meta_id = intval($data['meta_id']);
                $meta_value = $data['meta_value'];
                $this->sqlLogger->deleteMetaKey($post_name, $meta_id, $post_id, $meta_key, $meta_value);
            }
        }

        // Display Snow Overview
        $meta_key = 'resort_tabs_' . $weather_section . '_sections_3_template_id';
        $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
        $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $overview_post_id);
        $meta_key = 'resort_tabs_' . $weather_section . '_sections_3_section_display_status';
        if ($arrMetaKeys[$meta_key]['meta_value'] != $overview_post_status) {
            $meta_id = intval($arrMetaKeys[$meta_key]['meta_id']);
            $this->sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $overview_post_status);
        }

        // Add back in last section which is sign of for snow reports - but hide it
        $this->sqlLogger->addMetaKey($post_name, $post_id, $meta_key_start . 'section_display_status', 'hide');
        $this->sqlLogger->addMetaKey($post_name, $post_id, $meta_key_start . 'section_extra_class', '');
        $this->sqlLogger->addMetaKey($post_name, $post_id, $meta_key_start . 'section_id', '');
        $this->sqlLogger->addMetaKey($post_name, $post_id, $meta_key_start . 'template_id', '6141');
        $this->sqlLogger->addMetaKey($post_name, $post_id, '_' . $meta_key_start . 'section_display_status', 'field_63d76b8af352b_field_6346147142a18');
        $this->sqlLogger->addMetaKey($post_name, $post_id, '_' . $meta_key_start . 'section_extra_class', 'field_63d76b8af3536_field_634615aee8139');
        $this->sqlLogger->addMetaKey($post_name, $post_id, '_' . $meta_key_start . 'section_id', 'field_63d76b8af3536_field_6346158564140');
        $this->sqlLogger->addMetaKey($post_name, $post_id, '_' . $meta_key_start . 'template_id', 'field_63d76b8af3534');

    }
}

