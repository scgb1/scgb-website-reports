<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class PostDataDictionary
{
    private array $arrMetaKeys;
    private array $arrMetaDefaults;
    private array $fileKeys;

    /**
     * @throws Exception
     */
    public function __construct(string $meta_file)
    {
        // open the csv file and read into the array
        $fp = fopen($meta_file, "r");
        if ($fp === false) {
            throw new Exception('Failed to open meta file: ' . $meta_file);
        }

        // skip the first line
        fgetcsv($fp, 0);

        while (($data = fgetcsv($fp, 0)) !== FALSE) {
            /// replace !!! with ,
            $meta_key = $data[0];
            $meta_value = str_replace('!!!', ',', $data[1]);
            $this->arrMetaKeys[$meta_key] = $meta_value;
            $type = '';

            if (key_exists(2, $data)) {
                $type = $data[2];
                if ($type == 'Default') {
                    $this->arrMetaDefaults[$meta_key] = $meta_value;
                }
            }
            if (key_exists(3, $data)) {
                $spreadsheet_key = $data[3];
                if ($spreadsheet_key != '') {
                    $this->fileKeys[$spreadsheet_key] = array('meta_key' => $meta_key, 'type' => $type);
                }
            }
        }
        fclose($fp);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function getMetaValue(string $key, int $count = null): ?string
    {
        if ($count == null) {
            return $this->arrMetaKeys[$key] ?? null;
        }
        $meta_key = $this->arrMetaKeys[$key];

        // need to change the third number in the meta_key to be $count
        // meta_key of the format holiday_tabs_5_sections_0_component_central_faq_faqs_0_answer
        $meta_key = preg_replace('/\d+(_[a-z]+)$/', $count . '$1' , $meta_key);
        If ($meta_key == null) {
            throw new Exception('Failed to find meta key for ' . $key . ' - ' . $count);
        }
        return $this->arrMetaKeys[$meta_key] ?? null;
    }

    /**
     * @throws Exception
     */
    public function getMetaKeyFromName(string $name, int $count = null): ?string
    {
        if ($count === null) {
            return $this->fileKeys[$name]['meta_key'] ?? null;
        }
        $meta_key = $this->fileKeys[$name]['meta_key'];

        // need to change the last number in the meta_key to be $count
        // meta_key of the rep_slots_0_rep_resort or rep_slots_0_rep_resort_0_resort or
        // rep_slots_0_rep_resort_0_resort_0_resort
        $meta_key = preg_replace('/\d+(_[a-z_]+)$/', $count . '$1' , $meta_key);
        If ($meta_key == null) {
            throw new Exception('Failed to find meta key for ' . $name . ' - ' . $count);
        }
        return $meta_key;
    }

    public function hasDefault(string $key): ?array
    {
        return $this->arrMetaDefaults[$key] ?? null;
    }

    public function getMetaDefaults() : array
    {
        return $this->arrMetaDefaults;
    }
    public function getMetaKeys() : array
    {
        return $this->arrMetaKeys;
    }
}