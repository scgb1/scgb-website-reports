<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use mysqli;

class RepFromWebsite extends Rep
{
    const START_DATE = 0;
    const END_DATE = 1;
    protected static array $arrWebRepsByName = [];
    protected static array $arrHolidayToReps = [];
    protected array $repMeta = [];
    protected string $website_date_string;
    /**
     * @throws Exception
     */
    public function __construct(string $rep, string $post_id, string $post_name,
                                string $post_content, array $repMeta, ?PostDataDictionary $repDataDictionary)
    {
        parent::__construct($rep);

        // Check post_id is an integer
        if (!is_numeric($post_id)) {
            throw new Exception('post_id is not numeric: ' . $post_id);
        }
        $post_id = intval($post_id);
        $this->post_title = $rep;
        $this->post_name = $post_name;
        $this->introText = $post_content;
        $this->post_id = $post_id;
        $this->repMeta = $repMeta;
        $this->repDataDictionary = $repDataDictionary;

        foreach ($repMeta as $meta_key => $data) {
            $meta_value = $data['meta_value'];
            if ($meta_key == 'rep_role') {
                $this->rep_role = $meta_value;
            } elseif ($meta_key == 'rep_slots') {
                $num_slots = intval($meta_value);
                for ($i=0; $i< $num_slots; $i++) {
                    $register_key = 'rep_slots_' . $i . '_rep_register_link';
                    $resort_key = 'rep_slots_' . $i . '_rep_resort';
                    $date_key = 'rep_slots_' . $i . '_repping_date';
                    $date_value = $repMeta[$date_key]['meta_value'];
                    $this->arrRepResorts[] = array(
                        'rep_register_link' => $repMeta[$register_key]['meta_value'],
                        'resort_id' =>  $repMeta[$resort_key]['meta_value'],
                        'dates' => $date_value,
                        'start_date' => RepFromWebsite::getDate($date_value, self::START_DATE),
                        'end_date' => RepFromWebsite::getDate($date_value, self::END_DATE),
                    );
                }
                usort($this->arrRepResorts, array($this, 'custom_sort'));
                $this->website_date_string = $this->getWebsiteDateString();
            } elseif ($meta_key == '_thumbnail_id') {
                $this->rep_picture_id = $meta_value;
            }
        }
        return $this;
    }

//    public function getRepByName(string $repName): ?repFromWebsite
//    {
//        return self::$arrWebRepsByName[$repName] ?? null;
//    }

    /**
     * @throws Exception
     */
    public function checkForUpdates(RepFromFile $repFile) : void
    {
        // Big Function - check through each attribute of SkiTest and update if necessary
        if ($repFile->introText !== null && $this->introText != $repFile->introText) {
            Post::$sqlLogger->makePostChange($this->post_name, $this->post_id, 'post_content', $repFile->introText);
        }
        $this->checkForFieldChange($this->rep_role, $repFile->rep_role, 'rep_role', true);
        if (!key_exists('_thumbnail_id', $this->repMeta)) {
            Post::$sqlLogger->addMetaKey($this->post_name, $this->post_id, '_thumbnail_id', $repFile->rep_picture_id);
        } else {
            $this->checkForFieldChange($this->rep_picture_id, $repFile->rep_picture_id, '_thumbnail_id', true);
        }

        $post_id = $this->post_id;
        $post_name = $this->post_name;
        // Check for changes to the resorts
        // Do the FAQ - Same as Guides
        $i = 0;
        if (isset($this->arrRepResorts) && count($this->arrRepResorts) == count($repFile->arrRepResorts)) {
            foreach ($this->arrRepResorts as $resort) {
                $this->checkForFieldChange($resort['resort_id'], strval($repFile->arrRepResorts[$i]['resort_id']),
                    $this->repDataDictionary->getMetaKeyFromName('Resort ID', $i), true);
                $this->checkForFieldChange($resort['dates'], $repFile->arrRepResorts[$i]['dates'],
                    $this->repDataDictionary->getMetaKeyFromName('Rep Dates', $i), true);
                $this->checkForFieldChange($resort['rep_register_link'], $repFile->arrRepResorts[$i]['rep_register_link'],
                    $this->repDataDictionary->getMetaKeyFromName('Register Link', $i), true);
                $i++;
            }
        } else {
            $meta_key = $this->repDataDictionary->getMetaKeyFromName('Rep Slot Count');
            $meta_id = intval($this->repMeta[$meta_key]['meta_id']);
            $meta_value = strval(count($repFile->arrRepResorts));
            $old_meta_value = $this->repMeta[$meta_key]['meta_value'];
            if ($meta_value != $old_meta_value) {
                self::$sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $meta_value);
            }
            if (isset($this->arrRepResorts)) {
                foreach ($this->arrRepResorts as $rep) {
                    $this->deleteMetaKey('Resort ID', $rep, 'resort_id', $i);
                    $this->deleteMetaKey('Register Link', $rep, 'rep_register_link', $i);
                    $this->deleteMetaKey('Rep Dates', $rep, 'dates', $i);
                    $i++;
                }
            }



            // Now add the new ones
            $i = 0;
            foreach ($repFile->arrRepResorts as $rep) {
                $this->addMetaKey('Resort ID', $rep, 'resort_id', $i);
                $this->addMetaKey('Register Link', $rep, 'rep_register_link', $i);
                $this->addMetaKey('Rep Dates', $rep, 'dates', $i);
                $i++;
            }
        }

        // Temporary hack to cope with a bug in the website - also need to set a register link at the rep level
        $register_link = '';
        if (count($repFile->arrRepResorts) > 0) {
            $register_link = $repFile->arrRepResorts[0]['rep_register_link'];
        }
        if (key_exists('rep_register_link', $this->repMeta))
        {
            $current_register_link = $this->repMeta['rep_register_link']['meta_value'];
            $this->checkForFieldChange($current_register_link, $register_link, 'rep_register_link', true);
        } else {
            Post::$sqlLogger->addMetaKey($this->post_name, $this->post_id, 'rep_register_link', $register_link);
        }

        // Check the meta key rep_resorts - this drives the visibility of the reps on the resort pages
        $arrResorts = array();
        for ($i=0; $i<count($repFile->arrRepResorts); $i++) {
            $arrResorts[] = $repFile->arrRepResorts[$i]['resort_id'];
        }
        $rep_resorts_mata_value = Utils::createWPPostString($arrResorts);
        $meta_key = 'rep_resorts';
        if (!key_exists($meta_key, $this->repMeta)) {
            Post::$sqlLogger->addMetaKey($this->post_name, $this->post_id, $meta_key, $rep_resorts_mata_value);
        } else {
            $old_meta_value = $this->repMeta[$meta_key]['meta_value'];
            $this->checkForFieldChange($old_meta_value, $rep_resorts_mata_value, $meta_key, true);
        }

        // We the master - so overwrite whatever is there, if changed
        foreach (self::$arrHolidayToReps as $holiday_post_id => $meta) {
            if (str_contains($meta['meta_value'], '"' . $this->post_id . '"')) {
                if (!in_array($holiday_post_id, $repFile->arrRepHolidays)) {
                    if (!key_exists('to_delete', self::$arrHolidayToReps[$holiday_post_id])) {
                        self::$arrHolidayToReps[$holiday_post_id]['to_delete'] = array();
                    }
                    self::$arrHolidayToReps[$holiday_post_id]['to_delete'][] = $post_id;
                }
            }
        }
        foreach ($repFile->arrRepHolidays as $holiday_post_id) {
            if (!in_array($holiday_post_id, $this->arrRepHolidays)) {
                if (!key_exists('to_add', self::$arrHolidayToReps[$holiday_post_id])) {
                    self::$arrHolidayToReps[$holiday_post_id]['to_add'] = array();
                }
                self::$arrHolidayToReps[$holiday_post_id]['to_add'][] = $post_id;
            }
        }
    }

    /**
     * @throws Exception
     */
    private function deleteMetaKey(string $field, array $data, string $key, int $i) : void
    {
        $meta_key = $this->repDataDictionary->getMetaKeyFromName($field, $i);
        $meta_id = intval($this->repMeta[$meta_key]['meta_id']);
        $meta_value = $this->repMeta[$meta_key]['meta_value'];
        $post_name = $this->post_name;

        self::$sqlLogger->deleteMetaKey($post_name, $meta_id, $this->post_id,  $meta_key, $meta_value);
        $meta_key = '_' . $meta_key;
        $meta_value = $this->repMeta[$meta_key]['meta_value'];
        $meta_id = intval($this->repMeta[$meta_key]['meta_id']);
        self::$sqlLogger->deleteMetaKey($post_name, $meta_id, $this->post_id, $meta_key, $meta_value);
    }

    /**
     * @throws Exception
     */
    private function addMetaKey(string $field, array $data, string $key, int $i) : void
    {
        $meta_key = $this->repDataDictionary->getMetaKeyFromName($field, $i);
        $meta_value = strval($data[$key]);
        $post_name = $this->post_name;
        self::$sqlLogger->addMetaKey($post_name, $this->post_id,  $meta_key, $meta_value);
        $meta_key = '_' . $meta_key;
        $_meta_key = '_' . $this->repDataDictionary->getMetaKeyFromName($field, 0);
        $meta_value = $this->repDataDictionary->getMetaValue($_meta_key);
        if ($meta_value != null)
            self::$sqlLogger->addMetaKey($post_name, $this->post_id,  $meta_key, $meta_value);
    }

    /**
     * Convert From To string to DateTime.
     * if type is START then use the first date, if type is END then use the second date
     *
     * @param string $date_string
     * @param int $type
     * @return DateTime|null
     * @throws Exception
     */
    private function getDate(string $date_string, int $type) : ?DateTime
    {
        // Check the date is in the correct format
        // Should be of the form 2021/01/01 - 2021/01/08
        $ret_date = null;
        $date_string = strtolower($date_string);
        $date_string = str_replace('to', '-' , $date_string);
        $arrDates = explode('-', $date_string);
        if (count($arrDates) != 2) {
            utils::logger()->error('Rep . ' . $this->post_title . 'has slot with invalid date format: ' . $date_string);
        } else{
            $date = trim($arrDates[$type]);
            $date = str_replace('/', '-', $date);
            $ret_date = new DateTime($date);
        }
        return $ret_date;
    }

    public static function finaliseHolidays() : void
    {
        foreach (self::$arrHolidayToReps as $holiday_post_id => $data)
        {
            $post_name = Post::getPostNameFromID($holiday_post_id);
            // check the rep section visibility
            $meta_key = 'holiday_tabs_0_sections_3_section_display_status';
            $meta_value = $data['meta_value'];
            $metaData = Post::getMetaData($holiday_post_id);
            if ($metaData === null)
                continue;
            $current_display_status = $metaData[$meta_key]['meta_value'];
            $meta_id = $metaData[$meta_key]['meta_id'];
            if ($meta_value != '') {
                $new_display_status = 'all';
            } else {
                $new_display_status = 'hide';
            }
            if ($current_display_status != $new_display_status) {
                self::$sqlLogger->makeMetaChange($post_name, $meta_id, $holiday_post_id, $meta_key, $new_display_status);
            }

            if (!key_exists('to_add', $data) && !key_exists('to_delete', $data)) {
                continue;
            }

            // get the current Reps associated with this holiday
            $arrTemp = explode('"', $meta_value);
            $arrReps = [];
            for ($i=1; $i< count($arrTemp); $i+=2) {
                $rep_post_id = intval($arrTemp[$i]);
                if (key_exists('to_delete', $data) && in_array($rep_post_id, $data['to_delete'])) {
                    // Delete this rep from the list
                    continue;
                }
                $arrReps[] = $rep_post_id;
            }
            if (key_exists('to_add', $data)) {
                foreach ($data['to_add'] as $rep_post_id) {
                    $arrReps[] = $rep_post_id;
                }
            }
            $arrReps = array_unique($arrReps);
            // Now construct the new meta_value - of the form a:5:{i:0;s:4:"5862";i:1;s:4:"5880";i:2;s:4:"5879";i:3;s:4:"5856";i:4;s:4:"5185";}
            if (count($arrReps) == 0) {
                $new_meta_value = '';
            } else {
                $new_meta_value = 'a:' . count($arrReps) . ':{';
                $i = 0;
                foreach ($arrReps as $rep_post_id) {
                    $new_meta_value .= 'i:' . $i . ';s:' . strlen(strval($rep_post_id)) . ':"' . $rep_post_id . '";';
                    $i++;
                }
                $new_meta_value .= '}';
            }

            if ($meta_value != $new_meta_value) {
                $meta_id = intval($data['meta_id']);
                $meta_key = 'holiday_reps';
                self::$sqlLogger->makeMetaChange($post_name, $meta_id, $holiday_post_id, $meta_key, $new_meta_value);
            }
        }
    }
    /**
     * @throws Exception
     */
    static public function loadReps(mysqli $conn, ?PostDataDictionary $repDataDictionary, SqlLogger $sqlLogger): array
    {
        self::$conn = $conn;
        self::$sqlLogger = $sqlLogger;

        $arrMetaData = array();

        // Read data from the table wp_postmeta
        $sql = "SELECT m.post_id, p.post_name, p.post_title, p.post_content, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = id  " .
            "and post_type = 'sc_rep' " .
            "and (post_status = 'publish') " .
            "order by post_title ASC, meta_key ASC;";
        $result = $conn->query($sql);

        // Output the data as a csv
        $fp = fopen('data/rep-meta.csv', 'w');

        // Output the headers
        fputcsv($fp, array('Post ID', 'Post Title', 'Meta ID', 'Meta Key', 'Meta Value'));

        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $post_title = $data['post_title'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];
            $post_content = $data['post_content'];
            $post_name = $data['post_name'];

            // Output the data as a csv
            fputcsv($fp, array($post_id, $post_title, $meta_id, $meta_key, $meta_value));

            $arrMetaData[$post_title][$meta_key] = array(
                'post_id' => $post_id, 'post_name' => $post_name, 'post_content' => $post_content, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        fclose($fp);

        // Now create the rep objects
        foreach ($arrMetaData as $repName => $repMeta) {
            // Pop the first element off the array to get the post_id
            $post_id = array_shift($repMeta)['post_id'];
            $post_name = array_shift($repMeta)['post_name'];
            $post_content = array_shift($repMeta)['post_content'];

            $rep = new RepFromWebsite($repName, $post_id, $post_name, $post_content, $repMeta, $repDataDictionary);
            self::$arrWebRepsByName[$repName] = $rep;
            $rep->arrRepHolidays = Holiday::loadHolidaysForRep(intval($post_id));
        }

        RepFromWebsite::getHolidayToReps();

        return self::$arrWebRepsByName;
    }

    /**
     * @throws Exception
     */
    private static function getHolidayToReps() : void
    {
        $sql = "SELECT post_id, meta_id, meta_value " .
            "FROM wp_postmeta " .
            "WHERE meta_key = 'holiday_reps' " .
            "ORDER BY post_id";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load holiday to rep mapping: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['post_id']);
            $meta_id = intval($row['meta_id']);
            $meta_value = $row['meta_value'];

            self::$arrHolidayToReps[$post_id]['meta_id'] = $meta_id;
            self::$arrHolidayToReps[$post_id]['meta_value'] = $meta_value;
        }
    }

    private function custom_sort(array $a, array $b) : int
    {
        if ($a['start_date'] == $b['start_date']) {
            return 0;
        }
        return ($a['start_date'] < $b['start_date']) ? -1 : 1;
    }

    /**
     * @throws Exception
     */
    private function checkForFieldChange(?string $web_value, ?string $file_value, string $field, bool $have_meta_key = false): void
    {
        if ($file_value === null || ($web_value == $file_value)) {
            // No value specified in the file so no change to make
            // or no change to make
            return;
        }

        if (!$have_meta_key)
            $meta_key = $this->repDataDictionary->getMetaKeyFromName($field);
        else
            $meta_key = $field;
        if ($meta_key == null) {
            throw new Exception("Meta Key not found for " . $field);
        }
        $post_name = $this->post_name;
        $post_id = $this->post_id;
        $meta_id = intval($this->repMeta[$meta_key]['meta_id']);
        self::$sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $file_value);
    }

    static public function createEmptyRep(string $rep_name, int $basePostID, mysqli $conn, SqlLogger $sqlLogger) : array
    {
        // create an empty entry in wp_posts and get the post_id
        // This is the one time we interact directly with the database
        // They will go in with menu_order = 0, and we will fix that later
        $sequence = $sqlLogger->getSequence();
        $post_name = Utils::create_post_name($rep_name);

        // escape any ' in the holiday name
        $rep_name = str_replace("'", "\'", $rep_name);

        $sql = "start transaction;" . "\n";
        $conn->query($sql);
        $sql = "INSERT INTO wp_posts (post_author, post_date, post_date_gmt, post_content, post_title, 
                      post_excerpt, post_status, comment_status, ping_status, post_password, 
                      post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, 
                      post_parent, guid, menu_order, post_type, post_mime_type, comment_count) " .
            "VALUES (15, now(), now(), '', '$rep_name', '', 'publish', 'closed', 'closed', '', '$post_name', 
                    '', '', now(), now(), '', 0, '', 0, 'sc_rep', '', 0);" . "\n";
        $conn->query($sql);

        // Now iterate through the metadata and add it to the wp_postmeta table
        $post_id = $conn->insert_id;

        // Create the metadata by copying from the base post
        // Create audit entries as well
        $sql = "insert into scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "select $sequence, post_id, meta_key, null, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";
        $sql .= "insert into wp_postmeta (post_id, meta_key, meta_value) " .
            "select $post_id, meta_key, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";

        // Copy Data across from wp_term_relationships
//        $sql .= "insert into wp_term_relationships (object_id, term_taxonomy_id, term_order) " .
//            "select $post_id, term_taxonomy_id, term_order " .
//            "from wp_term_relationships " .
//            "where object_id = $basePostID " .";\n";
        $sql .= "commit;" . "\n";

        $conn->multi_query($sql);
        do {
            /* store the result set in PHP */
            if ($result = $conn->store_result()) {
                mysqli_free_result($result);
            }
        } while ($conn->next_result());

        $arrMetaData = array();
        // Get all the metadata for this holiday
        $sql = "SELECT p.post_name, m.post_id, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = ID  " .
            "and post_id = $post_id " .
            "order by meta_key ASC;";
        $result = $conn->query($sql);
        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];
            $post_name = $data['post_name'];

            $arrMetaData[$meta_key] = array(
                'post_name' => $post_name, 'post_id' => $post_id, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        return $arrMetaData;
    }

    private function getWebsiteDateString() : string
    {
        if (isset($this->website_date_string))
            return $this->website_date_string;

        $this->website_date_string = '';

        if (count($this->arrRepResorts) == 1)
        {
            $from_date = $this->arrRepResorts[0]['start_date'];
            $to_date = $this->arrRepResorts[0]['end_date'];
            if ($from_date != null && $to_date != null)
            {
                $this->website_date_string = $from_date->format('Y/m/d') . ' to ' . $to_date->format('Y/m/d');
            } else {
                $this->website_date_string = $this->arrRepResorts[0]['dates'];
            }
        } else {
            foreach ($this->arrRepResorts as $resort)
            {
                $from_date = $resort['start_date'];
                $to_date = $resort['end_date'];
                $resort_id = intval($resort['resort_id']);
                $resort_name = Post::getPostTitleFromID($resort_id);
                if ($from_date != null && $to_date != null)
                {
                    $this->website_date_string .= $resort_name . ': ' . $from_date->format('Y/m/d') . ' to ' . $to_date->format('Y/m/d') . ', ';
                } else {
                    $this->website_date_string .= $resort_name . ': ' . $resort['dates'] . ', ';
                }
                // strip off the last comma
            }
            $this->website_date_string = substr($this->website_date_string, 0, strlen($this->website_date_string) - 2);
        }
        return $this->website_date_string;
    }
}