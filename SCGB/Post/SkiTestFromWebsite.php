<?php

namespace SCGB;

use Exception;
use mysqli;

class SkiTestFromWebsite extends SkiTest
{
    protected static array $arrWebSkiTestsByName = [];
    protected static array $arrDefaultMeta = [];

    /**
     * @throws Exception
     */
    public function __construct(string $ski, array $skiMeta)
    {
        parent::__construct();
        $this->skiName = $ski;
        $this->skiMeta = $skiMeta;
        $this->post_id = $skiMeta['sections']['post_id'];
        foreach ($skiMeta as $meta_key => $data) {
            $meta_value = $data['meta_value'];
            if ($meta_key == 'sections_1_component_central_text_content')
            {
                // strip html tags and trim
                $meta_value = trim(strip_tags($meta_value));
                $this->weSay = $meta_value;
            } elseif ($meta_key == 'sections_0_component_central_text_content') {
                // strip html tags and trim
                $meta_value = trim(strip_tags($meta_value));
                $this->theySay = $meta_value;
            } elseif ($meta_key == 'ski_test_bumps') {
                $this->rating_bumps = $meta_value;
            } elseif ($meta_key == 'ski_test_construction') {
                $this->rating_construction = $meta_value;
            } elseif ($meta_key == 'ski_test_ease_of_use') {
                $this->rating_ease_of_use = $meta_value;
            } elseif ($meta_key == 'ski_test_length') {
                $this->length = $meta_value;
            } elseif ($meta_key == 'ski_test_mens_or_womens') {
                $this->mens_or_womens = $meta_value;
            } elseif ($meta_key == 'ski_test_off_piste_long_turns') {
                $this->rating_off_piste_long_turns = $meta_value;
            } elseif ($meta_key == 'ski_test_off_piste_short_turns') {
                $this->rating_off_piste_short_turns = $meta_value;
            } elseif ($meta_key == 'ski_test_on_piste_long_turns') {
                $this->rating_on_piste_long_turns = $meta_value;
            } elseif ($meta_key == 'ski_test_on_piste_short_turns') {
                $this->rating_on_piste_short_turns = $meta_value;
            } elseif ($meta_key == 'ski_test_overall_rating') {
                $this->rating_overall_rating = $meta_value;
            } elseif ($meta_key == 'ski_test_price') {
                $this->price = $meta_value;
            } elseif ($meta_key == 'ski_test_side_cut') {
                $this->side_cut = $meta_value;
            } elseif ($meta_key == 'ski_test_steeps') {
                $this->rating_steeps = $meta_value;
            } elseif ($meta_key == 'ski_test_strengths') {
                $this->strengths = $meta_value;
            } elseif ($meta_key == 'ski_test_subtitle') {
                $this->subtitle = $meta_value;
            } elseif ($meta_key == 'ski_test_suitability_for_level') {
                $this->suitability_for_level = $meta_value;
            } elseif ($meta_key == 'ski_test_turn_radius') {
                $this->turn_radius = $meta_value;
            } elseif ($meta_key == 'ski_test_weaknesses') {
                $this->weaknesses = $meta_value;
            } elseif ($meta_key == 'ski_test_year') {
                $this->year = $meta_value;
            }
        }
        $this->skiCategories = CategoryTaxonomy::getSkiCategories($this->post_id, self::$conn);
        $this->skiAbilities = AbilityTaxonomy::getSkiAbilities($this->post_id, self::$conn);
        $this->skiManufacturer = ManufacturerTaxonomy::getSkiManufacturer($this->post_id, self::$conn);
        return $this;
    }

    public function getSkiTestByName(string $skiName): ?SkiTestFromWebsite
    {
        return self::$arrWebSkiTestsByName[$skiName] ?? null;
    }

    /**
     * @throws Exception
     */
    public function checkForUpdates(SkiTestFromFile $skiTest) : void
    {
        // Big Function - check through each attribute of SkiTest and update if necessary
        $post_name = $this->skiName;
        $post_id = $this->post_id;
        if (Taxonomy::isDifferent($this->skiAbilities, $skiTest->skiAbilities)) {
            AbilityTaxonomy::updateSkiAbilities($post_name, $post_id, $skiTest->skiAbilities, self::$sqlLogger);
        }
        if (Taxonomy::isDifferent($this->skiCategories, $skiTest->skiCategories)) {
            CategoryTaxonomy::updateSkiCategories($post_name, $post_id, $skiTest->skiCategories, self::$sqlLogger);
        }
        if (Taxonomy::isDifferent(array($this->skiManufacturer), array($skiTest->skiManufacturer))) {
            ManufacturerTaxonomy::updateSkiManufacturer($post_name, $post_id, $skiTest->skiManufacturer, self::$sqlLogger);
        }
        $this->checkForFieldChange($this->weSay, $skiTest->weSay, 'sections_1_component_central_text_content');
        $this->checkForFieldChange($this->theySay, $skiTest->theySay, 'sections_0_component_central_text_content');
        $this->checkForFieldChange($this->rating_bumps, $skiTest->rating_bumps, 'ski_test_bumps');
        $this->checkForFieldChange($this->rating_construction, $skiTest->rating_construction, 'ski_test_construction');
        $this->checkForFieldChange($this->rating_ease_of_use, $skiTest->rating_ease_of_use, 'ski_test_ease_of_use');
        $this->checkForFieldChange($this->length, $skiTest->length, 'ski_test_length');
        $this->checkForFieldChange($this->mens_or_womens, $skiTest->mens_or_womens, 'ski_test_mens_or_womens');
        $this->checkForFieldChange($this->rating_off_piste_long_turns, $skiTest->rating_off_piste_long_turns, 'ski_test_off_piste_long_turns');
        $this->checkForFieldChange($this->rating_off_piste_short_turns, $skiTest->rating_off_piste_short_turns, 'ski_test_off_piste_short_turns');
        $this->checkForFieldChange($this->rating_on_piste_long_turns, $skiTest->rating_on_piste_long_turns, 'ski_test_on_piste_long_turns');
        $this->checkForFieldChange($this->rating_on_piste_short_turns, $skiTest->rating_on_piste_short_turns, 'ski_test_on_piste_short_turns');
        $this->checkForFieldChange($this->rating_overall_rating, $skiTest->rating_overall_rating, 'ski_test_overall_rating');
        $this->checkForFieldChange($this->price, $skiTest->price, 'ski_test_price');
        $this->checkForFieldChange($this->side_cut, $skiTest->side_cut, 'ski_test_side_cut');
        $this->checkForFieldChange($this->rating_steeps, $skiTest->rating_steeps, 'ski_test_steeps');
        $this->checkForFieldChange($this->suitability_for_level, $skiTest->suitability_for_level, 'ski_test_suitability_for_level');
        $this->checkForFieldChange($this->turn_radius, $skiTest->turn_radius, 'ski_test_turn_radius');
        $this->checkForFieldChange($this->weaknesses, $skiTest->weaknesses, 'ski_test_weaknesses');
        $this->checkForFieldChange($this->year, $skiTest->year, 'ski_test_year');
    }

    private function checkForFieldChange(?string $web_value, ?string $file_value, string $meta_key): void
    {
        if ($file_value === null || ($web_value == $file_value)) {
            // No value specified in the file so no change to make
            // or no change to make
            return;
        }
        $post_name = $this->skiName;
        $post_id = $this->post_id;
        $meta_id = intval($this->skiMeta[$meta_key]['meta_id']);
        self::$sqlLogger->makeMetaChange($post_name, $meta_id, $post_id, $meta_key, $file_value);
    }

    private function makeContent(string $content): string
    {
        return '<div class="accordion__separator"><span>' . $content . '</span></div>';
    }

    /**
     * @throws Exception
     */
    static public function loadSkiTests(mysqli $conn, string $defaultMetaFile, SqlLogger $sqlLogger): array
    {
        self::$conn = $conn;
        self::$sqlLogger = $sqlLogger;
        self::$arrDefaultMeta = self::loadDefaultMeta($defaultMetaFile);

        // Read data from the table wp_postmeta
        $sql = "SELECT m.post_id, p.post_title, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = id  " .
            "and meta_key not like '\_%' " .
            "and post_type = 'ski-test' " .
            "and post_status = 'publish' " .
            "order by post_title ASC, meta_key ASC;";
        $result = $conn->query($sql);

        // Output the data as a csv
        $fp = fopen('data/ski-meta.csv', 'w');

        // Output the headers
        fputcsv($fp, array('Post ID', 'Post Title', 'Meta ID', 'Meta Key', 'Meta Value'));

        // We need to collect all the data for a resort before we can run the checks
        // as some checks require data from multiple meta_keys
        $arrMetaKeys = array();

        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $post_title = $data['post_title'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];

            // Output the data as a csv
            fputcsv($fp, array($post_id, $post_title, $meta_id, $meta_key, $meta_value));

            $arrMetaKeys[$post_title][$meta_key] = array('post_id' => $post_id, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }

        // Now we can load the data into the skiTest objects
        foreach($arrMetaKeys as $ski => $arrMeta) {
            $skiTest = new SkiTestFromWebsite($ski, $arrMeta);
            self::$arrWebSkiTestsByName[$ski] = $skiTest;
        }
        return self::$arrWebSkiTestsByName;
    }

    static private function checkDefaultMeta(array $arrMetaKeys) : void
    {
        $sql = false;
        foreach($arrMetaKeys as $ski => $arrMeta) {
            // Do we need to do anything
            if ($arrMeta['sections']['meta_value'] != '') {
                continue;
            }

            $sql = true;

            Utils::logger()->info('Generating Defaults for ' . $ski);
            // Get next info from the first item in arrMetaKeys
            $post_id = intval($arrMeta['sections']['post_id']);
            $post_name = $ski;
            $meta_id = intval($arrMeta['sections']['meta_id']);

            self::$sqlLogger->makeMetaChange(
                $post_name, $meta_id, $post_id, 'sections', self::$arrDefaultMeta['sections']);

            foreach (self::$arrDefaultMeta as $meta_key => $meta_value) {
                if ($meta_key == 'sections') {
                    continue;
                }
                if (!key_exists($meta_key, $arrMeta)) {
                    self::$sqlLogger->addMetaKey($post_name, $post_id, $meta_key, $meta_value);
                }
            }
        }

        if ($sql) {
            Utils::logger()->info('Run the SQL to update the meta data and then rerun the loader');
            die(0);
        }
    }

    /**
     * @throws Exception
     */
    static private function loadDefaultMeta(string $metaFile) : array
    {
        $arrMeta['sections'] = 'a:7:{i:0;s:22:"component_central_text";i:1;s:22:"component_central_text";i:2;s:24:"component_review_summary";i:3;s:22:"component_where_to_buy";i:4;s:27:"component_you_may_also_like";i:5;s:8:"template";i:6;s:8:"template";}';

        // open the csv file and read into the array
        $handle = fopen($metaFile, "r");
        if ($handle === false) {
            throw new Exception('Failed to open meta file: ' . $metaFile);
        }

        while (($data = fgetcsv($handle, 1000)) !== FALSE) {
            /// replace !!! with ,
            $meta_key = $data[0];
            $meta_value = str_replace('!!!', ',', $data[1]);
            $arrMeta[$meta_key] = $meta_value;
        }

        return $arrMeta;
    }
    static public function createEmptySkiTest(string $ski_test_name, int $basePostID, mysqli $conn, SqlLogger $sqlLogger) : array
    {
        // create an empty entry in wp_posts and get the post_id
        // This is the one time we interact directly with the database
        // They will go in with menu_order = 0, and we will fix that later
        $sequence = $sqlLogger->getSequence();
        $post_name = Utils::create_post_name($ski_test_name);

        // escape any ' in the holiday name
        $ski_test_name = str_replace("'", "\'", $ski_test_name);

        $sql = "start transaction;" . "\n";
        $conn->query($sql);
        $sql = "INSERT INTO wp_posts (post_author, post_date, post_date_gmt, post_content, post_title, 
                      post_excerpt, post_status, comment_status, ping_status, post_password, 
                      post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, 
                      post_parent, guid, menu_order, post_type, post_mime_type, comment_count) " .
            "VALUES (15, now(), now(), '', '$ski_test_name', '', 'publish', 'closed', 'closed', '', '$post_name', 
                    '', '', now(), now(), '', 0, '', 0, 'ski-test', '', 0);" . "\n";
        $conn->query($sql);

        // Now iterate through the metadata and add it to the wp_postmeta table
        $post_id = $conn->insert_id;

        // Create the metadata by copying from the base post
        // Create audit entries as well
        $sql = "insert into scgb_wp_postmeta_audit (sequence, post_id, meta_key, meta_value_original, meta_value_new) " .
            "select $sequence, post_id, meta_key, null, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";
        $sql .= "insert into wp_postmeta (post_id, meta_key, meta_value) " .
            "select $post_id, meta_key, meta_value " .
            "from wp_postmeta " .
            "where post_id = $basePostID " .";\n";

        // Copy Data across from wp_term_relationships
        $sql .= "insert into wp_term_relationships (object_id, term_taxonomy_id, term_order) " .
            "select $post_id, term_taxonomy_id, term_order " .
            "from wp_term_relationships " .
            "where object_id = $basePostID " .";\n";
        $sql .= "commit;" . "\n";

        $conn->multi_query($sql);
        do {
            /* store the result set in PHP */
            if ($result = $conn->store_result()) {
                mysqli_free_result($result);
            }
        } while ($conn->next_result());

        $arrMetaData = array();
        // Get all the metadata for this holiday
        $sql = "SELECT m.post_id, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = ID  " .
            "and post_id = $post_id " .
            "order by meta_key ASC;";
        $result = $conn->query($sql);
        while ($data = $result->fetch_assoc()) {
            $post_id = $data['post_id'];
            $meta_id = $data['meta_id'];
            $meta_key = $data['meta_key'];
            $meta_value = $data['meta_value'];

            $arrMetaData[$meta_key] = array('post_id' => $post_id, 'meta_id' => $meta_id, 'meta_value' => $meta_value);
        }
        return $arrMetaData;
    }
}