<?php /** @noinspection PhpUnusedPrivateMethodInspection */
declare(strict_types=1);
namespace SCGB;

use Exception;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Exception\CommonMarkException;

class HolidayFromFile extends Holiday
{
    protected static array $arrFileHolidaysByName = [];
    const checkFunctions = array(
        '' => 'ignore',
        'Holiday Template name' => 'ignore',
        'Intro Text' => 'processIntroText',
        'Trip Highlights' => 'processTripHighlights',
        'Ski Standard text' => 'ignore',
        'Ski Standard formula' => 'ignore',
        'Ski Standard' => 'processSkiStandard',
        'Resort' => 'processResort',
        'Max' => 'processMax',
        'Min' => 'processMin',
        'Min Hex' => 'ignore',
        'Max Hex' => 'ignore',
        'On/Off' => 'processOnOffPiste',
        'Whats included' => 'processWhatsIncluded',
        'Whats not included' => 'processWhatsNotIncluded',
        'Content' => 'processGuideContent',
        'Position' => 'processPosition',
        'Name/content' => 'processNameContent',
        'Content ON SNOW' => 'processOnSnowExperience',
        'Hotel Content' => 'processHotelContent',
        'Hotel Name' => 'processHotelName',
        'Hotel Address' => 'processHotelAddress',
        'Map URL' => 'processMapURL',
        'Travel Content' => 'processTravelContent',
        'Travel Heading' => 'processTravelHeading',
        'Question' => 'processQuestion',
        'Answer' => 'processAnswer',
    );

    const standardColours = array(
        'On Piste' => array(
            'Early Intermediate (Blue)' => array('colour' => '#1e73be', 'image' => '4235',),
            'Intermediate (Red)' => array('colour' => '#dd3333', 'image' => '4165',),
            'Advanced Intermediate (Silver)' => array('colour' => '#9B9B9B', 'image' => '4166',),
            'Advanced (Purple)' => array('colour' =>'#8224e3', 'image' => '4167',),
            'Expert (Gold)' => array('colour' => '#dd9933', 'image' => '4236', ),
        ),
        'Off Piste' => array(
            'Aspirer (Red)' => array('colour' => '#dd3333', 'image' => '4169',),
            'Intermediate (Silver)' => array('colour' => '#9B9B9B', 'image' => '4170',),
            'Advanced (Purple)' => array('colour' => '#8224e3', 'image' => '4171',),
            'Expert (Gold)' => array('colour' =>'#dd9933', 'image' => '4172',),
        ),
    );

    private string $havePosition = '';
    private string $haveQuestion = '';

    private CommonMarkConverter $converter;

    public function __construct(string $holidayName, array $header, array $data)
    {
        parent::__construct($holidayName);
        $config = array(
            'html_input' => 'escape',
            'allow_unsafe_links' => true,
        );
        $this->converter = new CommonMarkConverter($config);

        // Iterate through the data and call the appropriate check function - if found
        for ($count = 0; $count < count($header); $count++) {
            $headerName = $header[$count];
            // strip non alpha characters from the header name
            $headerName = preg_replace('/[^A-Za-z0-9 ?\/]/', '', $headerName);
            $dataValue = trim($data[$count]);
            if (key_exists($headerName, self::checkFunctions)) {
                $checkFunction = self::checkFunctions[$headerName];
                if ($checkFunction != 'ignore') {
                    // Convert !!! back to , for some reason the csv file has !!! instead of ,
                    $dataValue = preg_replace('/!!!/', ',', $dataValue);
                    $this->$checkFunction($dataValue);
                }
            }else{
                Utils::logger()->error('Unknown column in Holiday File: ' . $headerName);
            }
        }

        return $this;
    }

    /**
     * @throws CommonMarkException
     */
    private function processIntroText(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->introText = $this->convertToHTML($dataValue);
    }

    /**
     * @throws CommonMarkException
     */
    private function processOnSnowExperience(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->onSnow = $this->convertToHTML($dataValue);
    }

    /**
     * @throws CommonMarkException
     */
    private function processTripHighlights(string $dataValue) : void
    {
        $dataValue = $this->processBulletPoints($dataValue);
        // Will be markdown - convert to html
        $this->tripHighlights = $this->convertToHTML($dataValue);
    }

    /**
     * @throws CommonMarkException
     */
    private function processSkiStandard(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->skiStandard = $this->convertToHTML($dataValue);
    }
    private function processResort(string $post_id) : void
    {
        // Convert to string of form a:1:{i:0;s:4:"1025";}
        // Note that we may have multiple resorts which will be separated by a comma
        // Add them all to the string
        if ($post_id == '') {
            Utils::logger()->warning('No resort specified for holiday: ' . $this->post_title);
            $this->resort_post_id_string = '';
        } else {
            $arrResorts = explode(',', $post_id);
            $arrResortsValid = [];
            // Check that post_id is a valid integer
            for ($i=0; $i < count($arrResorts); $i++) {
                if (!is_numeric($arrResorts[$i])) {
                    $issue = 'Invalid Resort: ' . $post_id . ' for holiday: ' . $this->holidayName;
                    Utils::logger()->error($issue);
                    continue;
                } else {
                    $post_id_int = intval($arrResorts[$i]);
                    // Check the resort exists
                    if (!Resort::isValidResort($post_id_int)) {
                        $issue = 'Invalid Resort: ' . $post_id_int . ' for holiday: ' . $this->holidayName;
                        Utils::logger()->error($issue);
                        continue;
                    }
                    if ($i==0) {
                        $this->resort_post_id = $post_id_int;
                    }
                }
                $arrResortsValid[] = $arrResorts[$i];
            }
            // start building the string
            if (count($arrResortsValid) == 0) {
                $this->resort_post_id_string = '';
                return;
            }
            $this->resort_post_id_string = 'a:' . count($arrResortsValid) . ':{';
            for ($count = 0; $count < count($arrResortsValid); $count++) {
                $this->resort_post_id_string .= 'i:' . $count . ';s:' . strlen($arrResortsValid[$count]) . ':"' . $arrResortsValid[$count] . '";';

            }
            $this->resort_post_id_string .= '}';
        }
    }
    private function processMax(string $dataValue) : void
    {
        $this->maxStandard = $dataValue;
    }
    private function processMin(string $dataValue) : void
    {
        $this->minStandard = $dataValue;
    }

    /**
     * @throws Exception
     */
    private function processOnOffPiste(string $dataValue) : void
    {
        if ($dataValue == 'On') {
            $this->holidayType = 'On Piste';
        } elseif ($dataValue == 'Off') {
            $this->holidayType = 'Off Piste';
        } else {
            throw new Exception('Incorrect Holiday Type: ' . $dataValue);
        }
    }

    /**
     * @throws CommonMarkException
     */
    private function processWhatsIncluded(string $dataValue) : void
    {
        $dataValue = $this->processBulletPoints($dataValue);
        // Will be markdown - convert to html
        $this->whatsIncluded = $this->convertToHTML($dataValue);
    }

    /**
     * @throws CommonMarkException
     */
    private function processWhatsNotIncluded(string $dataValue) : void
    {
        $dataValue = $this->processBulletPoints($dataValue);
        // Will be markdown - convert to html
        $this->whatsNotIncluded = $this->convertToHTML($dataValue);
    }

    /**
     * @throws Exception
     */
    private function processPosition(string $dataValue) : void
    {
        if ($this->havePosition == '') {
            $this->havePosition = $dataValue;
        } else {
            throw new Exception('Incorrectly formatted Guide Details: ' . $dataValue);
        }
    }

    /**
     * @throws CommonMarkException
     * @throws Exception
     */
    private function processNameContent(string $dataValue) : void
    {
        if ($this->havePosition == '') {
            throw new Exception('Incorrectly formatted Guide Details: ' . $dataValue);
        }

        $this->guideDetails[] = array('position' => $this->havePosition,
            'photo' => '',
            'content' => $this->convertToHTML($dataValue));
        $this->havePosition = '';
    }
    private function processHotelName(string $dataValue) : void
    {
        $this->hotelName = $dataValue;

        // Also set the accommodation header - if there is more than one then set to 'Multiple - see itinerary'
        // need to exclude blank lines
        $arrHotelNames = explode('\n', $dataValue);
        $count = 0;
        foreach ($arrHotelNames as $hotelName) {
            if ($hotelName != '') {
                $count++;
            }
        }
        if ($count > 1) {
            $this->accommodationHeader = 'Multiple - see itinerary';
        } else {
            $this->accommodationHeader = $dataValue;
        }
    }

    /**
     * @throws CommonMarkException
     */
    private function processHotelContent(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->hotelContent = $this->convertToHTML($dataValue);
    }
    /**
     * @throws CommonMarkException
     */
    private function processHotelAddress(string $dataValue) : void
    {
        // Convert to html
        $this->hotelAddress = $this->convertToHTML($dataValue);
    }
    private function processMapURL(string $dataValue) : void
    {
        $this->mapURL = $dataValue;
    }

    private function processTravelHeading(string $dataValue) : void
    {
        $this->travelHeading = $dataValue;
    }

    /**
     * @throws CommonMarkException
     */
    private function processTravelContent(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->travelContent = $this->convertToHTML($dataValue);
    }

    /**
     * @throws Exception
     */
    private function processQuestion(string $dataValue) : void
    {
        if ($dataValue == '')
            return;
        if ($this->haveQuestion == '') {
            $this->haveQuestion = $dataValue;
        } else {
            throw new Exception('Incorrectly formatted FAQ: ' . $dataValue);
        }
    }

    /**
     * @throws CommonMarkException
     * @throws Exception
     */
    private function processAnswer(string $dataValue) : void
    {
        if ($dataValue == '') {
            $this->haveQuestion = '';
            return;
        }
        if ($this->haveQuestion == '') {
            throw new Exception('Incorrectly formatted FAQ: ' . $dataValue);
        }
        $content = $this->convertToHTML($dataValue);
        if ($this->questions === null) {
            $this->questions = [];
        }
        $this->questions[] = array('question' => $this->haveQuestion, 'answer' => $content);
        $this->haveQuestion = '';
    }
    private function processBulletPoints(string $dataValue) : string
    {
        // We want:
        // The first line to have '### ' at the start - if not already there, add it
        // no empty lines
        // each line to have a '* ' at the start
        $arrLines = explode("\n", $dataValue);
        $doneFirstLine = false;
        $dataValue = '';
        foreach ($arrLines as $line) {
            $check = trim($line);
            if ($check == '') {
                continue;
            }
            if (!$doneFirstLine) {
                if (!str_starts_with($check, '### ')) {
                    $check  = '### ' . $check;
                }
                $doneFirstLine = true;
                $dataValue .= $check . "\n";
                continue;
            }
            if (!str_starts_with($check, '* ')) {
                $check = '* ' . $check;
            }
            $dataValue .= $check . "\n";
        }
        return $dataValue;
    }

    /**
     * @throws Exception
     */
    public function getStandardColour(string $holiday_type, string $standard) : string
    {
        if (!key_exists($holiday_type, self::standardColours)) {
            throw new Exception('Invalid holiday type ' . $holiday_type);
        }
        if (!key_exists($standard, self::standardColours[$holiday_type])) {
            throw new Exception('Invalid Ski standard: ' . $standard);
        }
        return self::standardColours[$holiday_type][$standard]['colour'];
    }

    /**
     * @throws Exception
     */
    public function getStandardImage(string $holiday_type, string $standard) : string
    {
        if (!key_exists($holiday_type, self::standardColours)) {
            throw new Exception('Invalid holiday type ' . $holiday_type);
        }
        if (!key_exists($standard, self::standardColours[$holiday_type])) {
            throw new Exception('Invalid Ski standard: ' . $standard);
        }
        return self::standardColours[$holiday_type][$standard]['image'];
    }
    public function getMinStandard() : string
    {
        return $this->minStandard;
    }
    public function getMaxStandard() : string
    {
        return $this->maxStandard;
    }
    /*
     * Convert from Markdown to HTML and strip off the <p> tags
     */
    /**
     * @throws CommonMarkException
     */
    protected function convertToHTML(string $dataValue) : string
    {
        $dataValue = $this->converter->convert($dataValue)->getContent();
        $dataValue = preg_replace('/^<p>/', '', $dataValue);
        return preg_replace('/<\/p>$/', '', $dataValue);
    }


    /**
     * @throws Exception
     */
    public static function loadHolidays($sourceFile) : array
    {
        // Read the data from the csv file - ignore the first 4 lines
        $fp = fopen($sourceFile, 'r');

        if ($fp === false) {
            throw new Exception('Failed to open holiday file: ' . $sourceFile);
        }
        for ($i=0; $i<4; $i++) {
            fgetcsv($fp);
        }

        $header = array_map('trim', fgetcsv($fp));
        while ($data = fgetcsv($fp)) {
            // Strip space of beginning and end of each field
            $data = array_map('trim', $data);

            // Remove non printables from the holiday name
            $holidayName = preg_replace('/[^[:print:]]/', '', $data[0]);

            // Remove any non alpha characters from the key
            try {
                if (key_exists($holidayName, self::$arrFileHolidaysByName)) {
                    throw new Exception('Duplicate holiday name: ' . $holidayName);
                }
                self::$arrFileHolidaysByName[$holidayName] =
                    new HolidayFromFile($holidayName, $header, $data);

            } catch (Exception $e) {
                Utils::logger()->error('Error loading Holiday for ' . $holidayName . " - " . $e->getMessage());
            }
        }
        fclose($fp);
        return self::$arrFileHolidaysByName;
    }
}
