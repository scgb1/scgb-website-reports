<?php
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class Attachment extends Post
{
    public function __construct(string $imageName)
    {
        parent::__construct($imageName);
        return $this;
    }

    /**
     * @throws Exception
     */
    public static function initialise(mysqli $conn): void
    {
        self::$conn = $conn;

        $sql = "SELECT ID, post_title, post_name " .
            "FROM wp_posts " .
            "WHERE post_type = 'attachment' " .
            "ORDER BY post_title";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load Attachment post data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['ID']);
            $post_title = trim($row['post_title']);
            $post_name = trim($row['post_name']);

            self::$arrPostDataByPostID[$post_id]['post_title'] = $post_title;
            self::$arrPostDataByPostID[$post_id]['post_name'] = $post_name;
            self::$arrPostDataByPostName[$post_name]['post_id'] = $post_id;
            self::$arrPostDataByPostName[$post_name]['post_title'] = $post_title;
            self::$arrPostDataByPostTitle[$post_title]['post_id'] = $post_id;
            self::$arrPostDataByPostTitle[$post_title]['post_title'] = $post_name;
        }
    }
}