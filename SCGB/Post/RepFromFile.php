<?php /** @noinspection PhpUnusedPrivateMethodInspection */
declare(strict_types=1);
namespace SCGB;

use Exception;
use League\CommonMark\Exception\CommonMarkException;

class RepFromFile extends Rep
{
    protected static array $arrFileRepsByName = [];

    public function __construct(string $repName, array $data, array $arrRepResorts, array $arrRepHolidays)
    {
        parent::__construct($repName);

        $this->introText = $data[1];
        $this->rep_role = 'Rep';

        // Construct post_name from repName
        $this->post_name = Utils::create_post_name($repName);

        foreach ($arrRepResorts as $resort) {
            $url = '/rep-signup';
            $resort['rep_register_link'] = 'a:3:{s:5:"title";s:21:"Pre-Register with Rep";s:3:"url";s:' . strlen($url) .
                ':"' . $url . '";s:6:"target";s:0:"";}';
            $this->arrRepResorts[] = $resort;
        }

        $this->arrRepHolidays = $arrRepHolidays;

        $rep_picture_name = 'rep-' . $this->post_name;
        $this->rep_picture_id = strval(Post::getPostIDFromName($rep_picture_name));

        return $this;
    }

    /**
     * @throws CommonMarkException
     */
    private function processIntroText(string $dataValue) : void
    {
        // Will be markdown - convert to html
        $this->introText = $this->convertToHTML($dataValue);
    }


    private function processResort(string $post_id) : void
    {

    }



    /**
     * @throws Exception
     */
    public static function loadReps(string $sourceFile, string $resortFile, string $holidayFile) : array
    {
        // Read the data from the csv file
        $fp = fopen($sourceFile, 'r');
        fgets($fp);

        if ($fp === false) {
            throw new Exception('Failed to open holiday file: ' . $sourceFile);
        }

        $arrResorts = self::loadResorts($resortFile);
        $arrHolidays = self::loadHolidays($holidayFile);

        // Get the Header
        while ($data = fgetcsv($fp)) {
            // Strip space of beginning and end of each field
            $data = array_map('trim', $data);

            // Remove non printables from the reps name
            $repName = preg_replace('/[^[:print:]]/', '', $data[0]);

            // Remove any non alpha characters from the key
            try {
                if (key_exists($repName, self::$arrFileRepsByName)) {
                    throw new Exception('Duplicate holiday name: ' . $repName);
                }

                $arrRepResorts = $arrResorts[$repName] ?? [];
                $arrRepHolidays = $arrHolidays[$repName] ?? [];
                self::$arrFileRepsByName[$repName] =
                    new RepFromFile($repName, $data, $arrRepResorts, $arrRepHolidays);

            } catch (Exception $e) {
                Utils::logger()->error('Error loading Reps for ' . $repName . " - " . $e->getMessage());
            }
        }
        fclose($fp);
        return self::$arrFileRepsByName;
    }

    static private function loadResorts(string $sourceFile) : array
    {
        // File is csv withe columns Rep, strFirstName, strSurname, Resort Country, Resort, dtmStartDate, dtmFinishDate
        // we want Rep, Resort and Start and End dates
        $fp = fopen($sourceFile, 'r');
        fgets($fp); // Skip the header line

        $arrResorts = [];
        while ($date = fgetcsv($fp)) {
            $repName = $date[0];
            $resortName = $date[4];
            $startDate = $date[5];
            $endDate = $date[6];

            $dates = $startDate . ' - ' . $endDate;
            $resort_id = Post::getPostIDFromName($resortName);
            if ($resort_id === null) {
                Utils::logger()->error('Processing Rep Resorts - Resort not found: ' . $resortName);
                continue;
            }

            $url = '/rep-signup';
            $arrResorts[$repName][] = [
                'resort_id' => $resort_id,
                'dates' => $dates,
                'rep_register_link' => 'a:3:{s:5:"title";s:21:"Pre-Register with Rep";s:3:"url";s:' . strlen($url) .
                    ':"' . $url . '";s:6:"target";s:0:"";}',
            ];
        }
        return $arrResorts;
    }
    static private function loadHolidays(string $sourceFile) : array
    {
        // File is csv with Format Name, Resort, Holiday Name, From, To
        // we want Name, Resort and Holiday Name
        $fp = fopen($sourceFile, 'r');
        fgets($fp); // Skip the header line

        $arrHolidays = [];
        while ($data = fgetcsv($fp))
        {
            $repName = $data[0];
            $holidayName = trim($data[2]);

            $holiday_id = Holiday::getPostIDFromName($holidayName);
            if ($holiday_id === null) {
                Utils::logger()->error('Processing Rep Holidays - Holiday not found: ' . $holidayName);
            } else {
                $arrHolidays[$repName][] = $holiday_id;
            }
        }
        return $arrHolidays;
    }
}
