<?php

namespace SCGB;

use Exception;

class SkiTestFromFile extends SkiTest
{
    protected static array $arrFileSkiTestsByName = [];
    const checkFunctions = array(
        'Name' => 'ignore',
        'CAT2' => 'ignore',
        'Brand' => 'processManufacturer',
        'Year' => 'processYear',
        'Subtitle' => 'processSubtitle',
        'T/C' => 'ignore',
        'Model' => 'ignore',
        'CAT1' => 'processCat',
        'TP?' => 'ignore',
        'E/A/R' => 'processEAR',
        'M/F/U' => 'ignore',
        'w/Bindings?' => 'processBindings',
        'Cost' => 'processCost',
        'WeSay' => 'processWeSay',
        'TheySay' => 'processTheySay',
        'Lgth/cm' => 'processLength',
        'Sidecut' => 'processSidecut',
        'Radius' => 'processRadius',
        'ReferenceLength' => 'ignore',
        'Construction' => 'processConstruction',
        'Pros' => 'processPlus',
        'Cons' => 'processMinus',
        'Overall' => 'processOverall',
        'OnS' => 'processOnPisteShortTurns',
        'OffS' => 'processOffPisteShortTurns',
        'OnL' => 'processOnPisteLongTurns',
        'OffL' => 'processOffPisteLongTurns',
        'Bmps' => 'processBumps',
        'Ease' => 'processEaseOfUse',
        'Level' => 'processLevel',
        'Edge Hold' => 'processEdgeHold',
        'Short Radius Turns' => 'processShortRadiusTurns',
        'Long Radius Turns' => 'processLongRadiusTurns',
        'High Speed Turns' => 'processHighSpeedTurns',
        'Low Speed Turns' => 'processLowSpeedTurns',
        'Smoothness' => 'processSmoothness',
        'Responsiveness' => 'processResponsiveness',
        'All terrain/Snow versatility' => 'processAllTerrainSnowVersatility',
        'Powder/Off Piste' => 'processPowderOffPiste',
        'Groomed Piste' => 'processGroomedPiste',
        'Moguls' => 'processMoguls',
        'Performance' => 'processPerformance',
    );

    const categoryLookup = array(
        'AM' => 'All Mountain',
        'FRD' => 'Freeride',
        'FST' => 'Freestyle',
        'BM' => 'Big Mountain',
        'FT' => 'Free-Tour',
        'PIS' => 'Piste Performance',
        'OPC' => 'On Piste Carvers',
        'RGS' => 'Race/Giant Slalom',
        'RS' => 'Race/Slalom',
        'TOU' => 'Touring',
        'SUS' => 'Sustainable',
        'LUX' => 'Luxury',
        'RAC' => 'Race',
    );

    const EARLookup = array(
        'E' => 'Expert',
        'A' => 'Advanced',
        'I' => 'Intermediate',
    );

    private string $tempCategory = '';

    public function __construct(array $header, array $data)
    {
        parent::__construct();

        // Iterate through the data and call the appropriate check function - if found
        for ($count = 0; $count < count($header); $count++) {
            $headerName = $header[$count];
            // strip non alpha characters from the header name
            $headerName = preg_replace('/[^A-Za-z0-9?\/]/', '', $headerName);
            $dataValue = trim($data[$count]);
            if (key_exists($headerName, self::checkFunctions)) {
                $checkFunction = self::checkFunctions[$headerName];
                if ($checkFunction != 'ignore') {
                    // Remove non printables from the data value
                    $dataValue = preg_replace('/[^[:print:]]/', '', $dataValue);
                    // Convert !!! back to , for some reason the csv file has !!! instead of ,
                    $dataValue = preg_replace('/!!!/', ',', $dataValue);
                    if (method_exists($this, $checkFunction)) {
                        $this->$checkFunction($dataValue);
                    } else {
                        Utils::logger()->error('Unknown function in Ski Test File: ' . $checkFunction);
                    }
                }
            } else {
                Utils::logger()->error('Unknown column in Ski Test File: ' . $headerName);
            }
        }

        return $this;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @throws Exception
     */
    private function processManufacturer(string $value) : void
    {
        $skiManufacturer = ManufacturerTaxonomy::getSkiManufacturerByName($value);
        $this->setSkiManufacturer($skiManufacturer);
    }
    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @throws Exception
     */
    private function processCat(string $value) : void
    {
        if (!key_exists($value, self::categoryLookup)) {
            Utils::logger()->error('Unknown CategoryTaxonomy in Ski Test File: ' . $value);
            return;
        }
        $this->tempCategory = self::categoryLookup[$value];
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @throws Exception
     */
    private function processEAR(string $value) : void
    {
        if (!key_exists($value, self::EARLookup)) {
            Utils::logger()->error('Unknown Ski StandardTaxonomy in Ski Test File: ' . $value);
            return;
        }
        $skiAbility = AbilityTaxonomy::getSkiAbility(self::EARLookup[$value]);
        $this->setSkiAbilities(array($skiAbility));
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function processCost(string $value) : void
    {
        // if the first character isn't £ then make it so
        if (!str_starts_with($value, '£')) {
            $value = '£' . $value;
        }
        $this->setPrice($value);
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @throws Exception
     */
    private function processMFU(string $value) : void
    {
        $skiCategory = $this->tempCategory;
        if ($value == 'M') {
            $value = 'men';
            $skiCategory = 'Men\'s ' . $skiCategory;
        } elseif ($value == 'W' || $value == 'F') {
            $value = 'women';
            $skiCategory = 'Women\'s ' . $skiCategory;
        } elseif ($value == 'U') {
            $value = 'both';
        } else {
            Utils::logger()->error('Unknown MFU in Ski Test File: ' . $value);
            return;
        }
        $this->setMensOrWomens($value);

        // Can now set the category
        $this->setSkiCategories(array(CategoryTaxonomy::getSkiCategory($skiCategory)));
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function processBindings(string $value) : void
    {
        if ($value == '1') {
            $value = 'yes';
        } elseif ($value == '0') {
            $value = 'no';
        } else {
            Utils::logger()->error('Unknown Bindings in Ski Test File: ' . $value);
            return;
        }
        $this->setBindings($value);
    }
    private function processWeSay(string $value) : void
    {
        $this->setWeSay($value);
    }
    private function processTheySay(string $value) : void
    {
        $this->setTheySay($value);
    }
    private function processLength(string $value) : void
    {
        $this->setLength($value);
    }
    private function processRadius(string $value) : void
    {
        $this->setTurnRadius($value);
    }
    private function processConstruction(string $value) : void
    {
        $this->setConstruction($value);
    }
    private function processPlus(string $value) : void
    {
        $this->setStrengths($value);
    }
    private function processMinus(string $value) : void
    {
        $this->setWeaknesses($value);
    }
    private function processYear(string $value) : void
    {
        $this->setYear($value);
    }

    /**
     * @throws Exception
     */
    private function processSidecut(string $value) : void
    {
        $this->setSideCut($value);
    }

    /**
     * @throws Exception
     */
    private function processSubtitle(string $value) : void
    {
        // Made up of three bits of information - 2023/24 Unisex Sustainable
        $arrWords = explode(' ', $value);
        if (count($arrWords) < 3) {
            throw new Exception('Subtitle not in correct format: ' . $value);
        }
        $this->setYear($arrWords[0]);
        $gender = $arrWords[1];
        // strip off any "'s" at the end
        if (str_ends_with($gender, "'s")) {
            $gender = substr($gender, 0, strlen($gender) - 2);
        }
        $gender = strtolower($gender);
        $this->setMensOrWomens($gender);
        $category = '';
        for ($count = 1; $count < count($arrWords); $count++) {
            $category = $category . ' ' . $arrWords[$count];
        }
        $category = trim($category);

        $this->setSkiCategories(array(CategoryTaxonomy::getSkiCategory($category)));
    }
    private function processOverall(string $value) : void
    {
        $this->setRatingOverallRating(intval($value));
    }
    private function processOnPisteShortTurns(string $value) : void
    {
        $this->setRatingOnPisteShortTurns(intval($value));
    }
    private function processOffPisteShortTurns(string $value) : void
    {
        $this->setRatingOffPisteShortTurns(intval($value));
    }
    private function processOnPisteLongTurns(string $value) : void
    {
        $this->setRatingOnPisteLongTurns(intval($value));
    }
    private function processOffPisteLongTurns(string $value) : void
    {
        $this->setRatingOffPisteLongTurns(intval($value));
    }
    private function processBumps(string $value) : void
    {
        $this->setRatingBumps(intval($value));
    }
    private function processEaseOfUse(string $value) : void
    {
        $this->setRatingEaseOfUse(intval($value));
    }
    private function processLevel(string $value) : void
    {
        $this->setSuitabilityForLevel(intval($value));
    }


    public static function loadSkiTests($sourceFile) : array
    {
        // Read the data from the csv file - first line is the header
        $fp = fopen($sourceFile, 'r');

        $header = array_map('trim', fgetcsv($fp));
        while ($data = fgetcsv($fp)) {
            // Check Column TP? - if not P then ignore
            if ($data[8] != '1') {
                continue;
            }
            // Strip space of beginning and end of each field
            $data = array_map('trim', $data);
            // Remove non printables from the data value
            $key = preg_replace('/[^[:print:]]/', '', $data[0]);
            Utils::logger()->info('Processing ' . $key. ' from file.');

            // Remove any non alpha characters from the key
            try {
                self::$arrFileSkiTestsByName[$key] = new SkiTestFromFile($header, $data);
            } catch (Exception $e) {
                Utils::logger()->error('Error loading Ski Test for ' . $key . " - " . $e->getMessage());
            }
        }
        fclose($fp);
        return self::$arrFileSkiTestsByName;
    }
}
