<?php
declare(strict_types=1);
namespace SCGB;

use mysqli;

class SkiTest
{
    protected static SqlLogger $sqlLogger;
    protected static mysqli $conn;

    protected int $post_id;
    protected string $skiName;
    protected array $skiMeta;
    protected ?array $skiAbilities = [];
    protected ?array $skiCategories = [];
    protected ?Taxonomy $skiManufacturer = null;
    protected string $theySay = '';
    protected string $weSay = '';

    protected int $rating_bumps = 0;
    protected string $rating_construction = '';
    protected int $rating_ease_of_use = 0;
    protected string $length = '';
    protected string $mens_or_womens = '';
    protected int $rating_off_piste_long_turns = 0;
    protected int $rating_off_piste_short_turns = 0;
    protected int $rating_on_piste_long_turns = 0;
    protected int $rating_on_piste_short_turns = 0;
    protected int $rating_overall_rating = 0;
    protected string $price = '';
    protected string $side_cut = '';
    protected int $rating_steeps = 0;
    protected string $strengths = '';
    protected string $subtitle = '';
    protected int $suitability_for_level = 0;
    protected string $turn_radius = '';
    protected string $weaknesses = '';
    protected string $year = '';

    public function __construct()
    {
        return $this;
    }

    // Setters
    protected function setPostId(int $post_id): void {
        $this->post_id = $post_id;
    }
    protected function setSkiName(string $skiName): void {
        $this->skiName = $skiName;
    }
    protected function setSkiAbilities(array $skiAbilities): void {
        $this->skiAbilities = $skiAbilities;
    }
    protected function setSkiCategories(array $skiCategories): void {
        $this->skiCategories = $skiCategories;
    }
    protected function setSkiManufacturer(Taxonomy $skiManufacturer): void {
        $this->skiManufacturer = $skiManufacturer;
    }
    protected function setTheySay(string $theySay): void {
        $this->theySay = $theySay;
    }
    protected function setWeSay(string $weSay): void {
        $this->weSay = $weSay;
    }
    protected function setRatingBumps(int $rating_bumps): void {
        $this->rating_bumps = $rating_bumps;
    }
    protected function setConstruction(string $rating_construction): void {
        $this->rating_construction = $rating_construction;
    }
    protected function setRatingEaseOfUse(int $rating_ease_of_use): void {
        $this->rating_ease_of_use = $rating_ease_of_use;
    }
    protected function setLength(string $length): void {
        $this->length = $length;
    }
    protected function setMensOrWomens(string $mens_or_womens): void {
        $this->mens_or_womens = $mens_or_womens;
    }
    protected function setRatingOffPisteLongTurns(int $rating_off_piste_long_turns): void {
        $this->rating_off_piste_long_turns = $rating_off_piste_long_turns;
    }
    protected function setRatingOffPisteShortTurns(int $rating_off_piste_short_turns): void {
        $this->rating_off_piste_short_turns = $rating_off_piste_short_turns;
    }
    protected function setRatingOnPisteLongTurns(int $rating_on_piste_long_turns): void {
        $this->rating_on_piste_long_turns = $rating_on_piste_long_turns;
    }
    protected function setRatingOnPisteShortTurns(int $rating_on_piste_short_turns): void {
        $this->rating_on_piste_short_turns = $rating_on_piste_short_turns;
    }
    protected function setRatingOverallRating(int $rating_overall_rating): void {
        $this->rating_overall_rating = $rating_overall_rating;
    }
    protected function setPrice(string $price): void {
        $this->price = "Price: ". $price;
    }
    protected function setBindings(string $binding): void {
        if ($binding == 'yes') {
            $this->price .= ' (with Bindings)';
        }
    }
    protected function setSideCut(string $side_cut): void {
        $this->side_cut = $side_cut;
    }
    protected function setRatingSteeps(int $rating_steeps): void {
        $this->rating_steeps = $rating_steeps;
    }
    protected function setSubtitle(string $subtitle): void {
        $this->subtitle = $subtitle;
    }
    protected function setSuitabilityForLevel(int $suitability_for_level): void {
        $this->suitability_for_level = $suitability_for_level;
    }
    protected function setTurnRadius(string $turn_radius): void {
        $this->turn_radius = $turn_radius;
    }
    protected function setWeaknesses(string $weaknesses): void
    {
        if ($this->weaknesses == '') {
            $this->weaknesses = $weaknesses;
        } else {
            $this->weaknesses .= '\n' . $weaknesses;
        }
    }
    protected function setStrengths(string $strengths) : void {
        if ($this->strengths == '') {
            $this->strengths = $strengths;
        } else {
            $this->strengths .= '\n' . $strengths;
        }
    }
    protected function setYear(string $year): self {
        $this->year = $year;
        return $this;
    }
}