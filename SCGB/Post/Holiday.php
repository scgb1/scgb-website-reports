<?php
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class Holiday extends Post
{
    protected static mysqli $conn;
    protected static array $arrHolidayMeta = [];

    protected ?int $post_id = null;
    protected ?string $holidayName = null;
    protected ?string $introText = null;
    protected ?string $tripHighlights = null;
    protected ?string $skiStandard = null;
    protected ?string $resort_post_id_string = null;
    protected ?int $resort_post_id = null;
    protected ?string $maxStandard = null;
    protected ?string $minStandard = null;
    protected ?string $onSnow = null;
    protected ?string $whatsIncluded = null;
    protected ?string $whatsNotIncluded = null;
    protected ?array $guideDetails = null;
    protected ?string $hotelName = null;
    protected ?string $accommodationHeader = null;
    protected ?string $hotelContent = null;
    protected ?string $hotelAddress = null;
    protected ?string $mapURL = null;
    protected ?string $travelHeading = null;
    protected ?string $travelContent = null;
    protected ?array $questions = null;
    protected ?string $holidayType = null;
    protected ?string $holidayPrice = null;
    protected ?array $arrSugatiTours = [];
    protected array $arrDepartureMonths = [];


    public function __construct(string $holidayName)
    {
        parent::__construct($holidayName);
        return $this;
    }

    // Find holidays that this rep on - will be slow, but we will do using sql for now
    // This function won't be run often
    /**
     * @throws Exception
     */
    public static function loadHolidaysForRep(int $post_id) : array
    {
        $sql = 'SELECT post_id, meta_value ' .
            'FROM wp_postmeta ' .
            'WHERE meta_key = "holiday_reps" ' .
            'AND meta_value like "%\"' . $post_id . '\"%"';
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load holidays for rep: ' . self::$conn->error);
        }
        $arrHols = [];
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['post_id']);
            $arrHols[] = $post_id;
        }
        return $arrHols;
    }

    public function getHolidayName() : string
    {
        return $this->holidayName;
    }
    public function getPostID() : ?int
    {
        return $this->post_id;
    }

    // Setters
    protected function setPostId(int $post_id): self
    {
        $this->post_id = $post_id;
        return $this;
    }

    /**
     * @throws Exception
     */
    public static function initialise(mysqli $conn): void
    {
        self::$conn = $conn;
        $sql = "SELECT post_id, meta_id, meta_key, meta_value " .
            "FROM wp_postmeta " .
            "WHERE post_id in ( " .
            "SELECT ID " .
            "FROM wp_posts " .
            "WHERE post_type = 'holiday' " .
            "AND post_status = 'publish') " .
            "ORDER BY post_id, meta_key";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load resort meta data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['post_id']);
            $meta_key = trim($row['meta_key']);
            $meta_value = trim($row['meta_value']);
            $meta_id= intval($row['meta_id']);
            if (!key_exists($post_id, self::$arrHolidayMeta)) {
                self::$arrHolidayMeta[$post_id] = array();
            }
            Post::$arrPostDataByPostID[$post_id][$meta_key] = array('meta_id' => $meta_id, 'meta_value' => $meta_value);
        }

        $sql = "SELECT ID, post_title, post_name " .
            "FROM wp_posts " .
            "WHERE post_type = 'holiday' " .
            "AND post_status = 'publish' " .
            "ORDER BY post_title";
        $result = self::$conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load resort post data: ' . self::$conn->error);
        }
        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['ID']);
            $post_title = trim($row['post_title']);
            $post_name = trim($row['post_name']);

            Post::$arrPostDataByPostID[$post_id]['post_title'] = $post_title;
            Post::$arrPostDataByPostID[$post_id]['post_name'] = $post_name;
            Post::$arrPostDataByPostName[$post_name]['post_id'] = $post_id;
            Post::$arrPostDataByPostName[$post_name]['post_title'] = $post_title;
            Post::$arrPostDataByPostTitle[$post_title]['post_id'] = $post_id;
            Post::$arrPostDataByPostTitle[$post_title]['post_title'] = $post_name;
        }
    }
}