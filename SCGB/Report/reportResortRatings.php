<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class reportResortRatings extends WebsiteReportsBase
{
    const REPORT_NAME = 'Report - Resort Ratings';
    const REPORT_TEMPLATE = 'resortRatings.html.twig';
    const RATINGS_TEMPLATE = 'ratingsList.html.twig';
    public function __construct(SqlLogger $sqlLogger)
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resort ratings
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        // Looking for meta key doesn't start with
        // - resort_tabs
        // - section
        // - resort_badge
        // - resort_banner_image_desktop
        // - resort_banner_image_mobile
        if (!str_starts_with($meta_key, 'resort_tabs') &&
            !str_starts_with($meta_key, 'section') &&
            $meta_key != 'resort_badge' &&
            $meta_key != 'resort_banner_image_desktop' &&
            $meta_key != 'resort_banner_image_mobile' &&
            $meta_key != 'resort_intro_text' &&
            $meta_key != 'resort_weather_resort_api_id')
        {
            $this->addReportData(array('name' => $resort, 'post_id' => $post_id, 'post_name' => $post_name, 'meta_key' => $meta_key, 'meta_value' => $meta_value));
        }
    }

    public function addReportData(mixed $data): void
    {
        $this->reportData[] = $data;
    }

    /**
     * @throws Exception
     */
    public function renderReport($twig) : void
    {
        // Build up the 2D array of resorts
        $arrMetaKeys = array();
        $resorts = array();
        foreach ($this->reportData as $resort) {
            $name = $resort['name'];
            $post_id = $resort['post_id'];
            $post_name = $resort['post_name'];
            $meta_key = $resort['meta_key'];
            $meta_value = $resort['meta_value'];

            if (!key_exists($name, $resorts)) {
                $resorts[$name]['post_id'] = $post_id;
                $resorts[$name]['post_name'] = $post_name;
            }
            $resorts[$name]['ratings'][$meta_key] = $meta_value;

            // Keep a record of the Meta Keys
            if (!key_exists($meta_key, $arrMetaKeys)) {
                $arrMetaKeys[$meta_key] = $meta_key;
            }
        }

        // Now make sure there is a Meta Key Entry for each resort and make sure all sorted
        ksort($arrMetaKeys);
        ksort($resorts);
        foreach ($resorts as $resort => $resortData) {
            $ratings = array();
            foreach ($arrMetaKeys as $meta_key) {
                if (!key_exists($meta_key, $resortData['ratings'])) {
                    $ratings[$meta_key] = "<unset>";
                }else {
                    $ratings[$meta_key] = $resortData['ratings'][$meta_key];
                }
            }
            $resorts[$resort]['ratings'] = $ratings;
        }

        // create the resort level report
        foreach ($resorts as $resort => $resortData) {

            $post_id = $resortData['post_id'];
            $post_name = $resortData['post_name'];
            $reportName = $resort . ' - Resort Ratings';
            $filename = Utils::getConfigItem('dirWebRoot') . '/' . Utils::getConfigItem('dirWebSupport') . '/' .
                $this->getHTMLFilename($post_name . '_resort_ratings');
            $resorts[$resort]['resort_report'] = $filename;

            file_put_contents($filename,
                $twig->render(self::RATINGS_TEMPLATE, array(
                    'url' => self::SKICLUB_URL,
                    'name' => $reportName,
                    'resort' => $resort,
                    'post_id' => $post_id,
                    'post_name' => $post_name,
                    'data' => $resortData['ratings'])));
        }

        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $resorts)));
    }
}