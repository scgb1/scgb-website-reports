<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class reportSectionUsage extends WebsiteReportsBase
{
    const REPORT_NAME = 'Consistency - Resort Section Usage';
    const MAIN_REPORT_TEMPLATE = 'resortSectionUsage.html.twig';
    const RESORT_REPORT_TEMPLATE = 'resortSectionUsage2.html.twig';
    private array $metaKeyToEnglish;
    public function __construct(SqlLogger $sqlLogger)
    {
        $this->metaKeyToEnglish = include(__DIR__ . '/../../Decodes.php');
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Looking for section status identified by
     * sections identified by the meta_key matching with the regex 'resort_tabs_[0-9]_sections.*_status' or
     * 'sections_[0-9]_section_display_status
     *
     * @throws Exception
     */
    public function buildReport(string $resort, $meta_key, $meta_value, $post_id, $post_name) : void
    {
        // only interested in the sections
        if (preg_match('/(resort_tabs_[0-9]_sections.*_status)|(sections_[0-9]_section_display_status)/', $meta_key)) {
            // Check meta-value only 'show' or 'hide'
            if (!in_array($meta_value, array('show', 'hide', 'all', 'logged', 'non-logged'))) {
                return;
            }
            $this->addReportData(
                array('name' => $resort, 'section' => $meta_key, 'status' => $meta_value,
                    'post_id' => $post_id, 'post_name' => $post_name));
        }
    }

    public function renderReport($twig) : void
    {
        $arrReportData = array();
        $arrReportData['00_Totals'] = array();
        foreach( $this->reportData as $resortStatus) {
            $resort = $resortStatus['name'];
            $section = $resortStatus['section'];
            $post_id = $resortStatus['post_id'];
            $post_name = $resortStatus['post_name'];

            // Check if there is an English version of the section
            if (key_exists($section, $this->metaKeyToEnglish)) {
                $section = $this->metaKeyToEnglish[$section];
            }
            $status = $resortStatus['status'];
            if (!isset($arrReportData[$resort])) {
                $arrReportData[$resort]['sections'] = array();
                $arrReportData[$resort]['post_id'] = $post_id;
                $arrReportData[$resort]['post_name'] = $post_name;
            }
            if (!isset($arrReportData[$resort]['sections'][$section])) {
                $arrReportData[$resort]['sections'][$section] = $status;
            }

            if (!isset($arrReportData['00_Totals'][$section])) {
                $arrReportData['00_Totals'][$section] = array();
            }
            if (!isset($arrReportData['00_Totals'][$section][$status])) {
                $arrReportData['00_Totals'][$section][$status] = array();
            }
            $arrReportData['00_Totals'][$section][$status][] = array(
                'name' => $resort, 'post_id' => $post_id, 'post_name' => $post_name);
        }

        // Need to make sure every key in the totals array is in the resort array
        // And make sure show and hide in the totals array
        $arrayTotals = $arrReportData['00_Totals'];
        foreach ($arrReportData as $resort => $data) {
            foreach ($arrayTotals as $section => $status) {
                if ($resort == '00_Totals') {
                    foreach (array('show', 'hide', 'all', 'logged', 'non-logged') as $type) {
                        if (!isset($arrReportData['00_Totals'][$section][$type])) {
                            $arrReportData['00_Totals'][$section][$type] = array();
                        }
                    }
                    ksort($arrReportData['00_Totals'][$section]);
                    continue;
                }
                if (!isset($arrReportData[$resort]['sections'][$section])) {
                    $arrReportData[$resort]['sections'][$section] = '-';
                }
            }
        }

        // Now sort the data
        ksort($arrReportData);
        foreach ($arrReportData as $resort => $data) {
            if ($resort == '00_Totals') {
                ksort($arrReportData['00_Totals']);
            } else {
                ksort($data['sections']);
            }
        }

        // Next create the sub files to allow better analysis
        // One report per section per show/hide - list of resorts
        // one report per resort - list of sections and show/hide
        foreach ($arrReportData['00_Totals'] as $section => $data) {
            // If $key ends in '_resorts' then create a page with that data in it
            foreach ($data as $type => $arrResorts) {
                $name = 'Resort with "' . $section . '" of visibility "' . $type . '"';
                $filename = $section . '-' . $type;
                if (count($arrResorts) > 0) {
                    $arrReportData['00_Totals'][$section][$type]['show_filename'] = $this->getHTMLFilename($filename);

                    file_put_contents($this->reportDirectory . "/" . $this->getHTMLFilename($filename),
                        $twig->render("resortList.html.twig", array('url' => self::SKICLUB_URL, 'name' => $name, 'data' => $arrResorts)));
                }
            }
        }

        foreach ($arrReportData as $resort => $data) {
            if ($resort == '00_Totals') {
                continue;
            }

            $arrResortData = array();
            // Create an ascii version of the resort with no spaces characters that cant be used in a filename

            foreach ($data as $section => $status) {
                $arrResortData[$section] = $status;
                // Create a file for each section
            }
            file_put_contents($this->reportDirectory ."/" . $this->getHTMLFilename($data['post_name']),
                $twig->render(self::RESORT_REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $resort, 'data' => $arrResortData)));
            $arrReportData[$resort]['resort_report'] = $data['post_name'] . '.html';
        }

        // set the path of the template directory relative to here
        file_put_contents($this->reportFilename,
            $twig->render(self::MAIN_REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'resorts' => $arrReportData)));
    }
}