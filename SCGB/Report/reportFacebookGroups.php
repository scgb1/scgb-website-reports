<?php
declare(strict_types=1);
namespace SCGB;

class reportFacebookGroups extends WebsiteReportsBase
{
    const REPORT_NAME = 'Report - Resort Facebook Groups';
    const REPORT_TEMPLATE = 'facebookGroups.html.twig';
    public function __construct(SqlLogger $sqlLogger)
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resorts with reps - looking for meta_key = resort_rep_resort
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param $post_id
     * @param $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, $post_id, $post_name) : void
    {
        if ($meta_key == 'resort_tabs_0_sections_6_section_display_status' && $meta_value == 'show')
        {
            $this->addReportData(
                array('name' => $resort, 'status' => 'show', 'post_id' => $post_id, 'post_name' => $post_name));
        }
        // We don't care about the key - just looking for links
        preg_match('/(https:\/\/www.facebook.com\/groups\/.*\/)/', $meta_value, $matches);
        if (isset($matches[1])) {
            $this->addReportData(
                array('name' => $resort, 'fbgroup' => $matches[1], 'post_id' => $post_id, 'post_name' => $post_name));
        }

        // Add the Ski CLub App data - unite the data at the next step
        preg_match('#"(https://skiclubofgb.honeycommb.com/groups/[^"]+)"#', $meta_value, $matches);
        if (isset($matches[1])) {
            $this->addReportData(
                array('name' => $resort, 'skiclubapp' => $matches[1], 'post_id' => $post_id, 'post_name' => $post_name));
        }
    }

    public function renderReport($twig) : void
    {
        // Need to iterate through the data and merge the two arrays
        $resorts = array();
        foreach ($this->reportData as $resort) {
            $name = $resort['name'];
            $resorts[$name]['name'] = $name;
            $resorts[$name]['post_id'] = $resort['post_id'];
            $resorts[$name]['post_name'] = $resort['post_name'];

            if (isset($resort['fbgroup'])) {
                $resorts[$name]['fbgroup'] = $resort['fbgroup'];
            }
            if (isset($resort['skiclubapp'])) {
                $resorts[$name]['skiclubapp'] = $resort['skiclubapp'];
            }
            if (isset($resort['status'])) {
                $resorts[$name]['status'] = $resort['status'];
            }
        }

        // Now iterate back though and remove any resorts that don't have a status of show
        foreach ($resorts as $key => $resort) {
            if (!isset($resort['status'])) {
                unset($resorts[$key]);
            }
        }

        // set the path of the template directory relative to here
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'resorts' => $resorts)));
    }
}