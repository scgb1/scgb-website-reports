<?php
declare(strict_types=1);
namespace SCGB;

require_once __DIR__ . '/WebsiteReportsBase.php';

class reportConsistencyByField extends WebsiteReportsBase
{
    const REPORT_NAME = 'Consistency - Issues By Field';
    const REPORT_TEMPLATE = 'ConsistencyCheckByField.html.twig';
    public function __construct(SqlLogger $sqlLogger)
    {
        $this->sqlLogger = $sqlLogger;
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resorts with reps - looking for meta_key = resort_rep_resort
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        //NOOP  -  we don't want to do anything here
    }

    public function addReportData(mixed $data): void
    {
        $this->reportData = $data;
    }

    public function renderReport($twig) : void
    {
        $resorts = $this->reportData;
        // Also create a report by field
        $arrReportbyField = array();
        foreach ($resorts as $resort => $resortData)
        {
            foreach ($resortData['fields'] as $field => $data) {
                if (!key_exists($field, $arrReportbyField)) {
                    $arrReportbyField[$field] = array();
                }
                if (!in_array($resort, $arrReportbyField[$field])) {
                    $arrReportbyField[$field][$resort] = array();
                    $post_id = $resortData['post_id'];
                    $post_name = $resortData['post_name'];
                    $name = $resortData['name'];
                    $arrReportbyField[$field][$resort]['post_id'] = $post_id;
                    $arrReportbyField[$field][$resort]['post_name'] = $post_name;
                    $arrReportbyField[$field][$resort]['name'] = $name;
                    $arrReportbyField[$field][$resort]['issues'] = array();
                }
                foreach ($data as $issue) {
                    $arrReportbyField[$field][$resort]['issues'][] = $issue;
                }
            }
        }

        print("Creating report " . $this->reportFilename . "\n");
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $arrReportbyField)));


    }
}