<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class ReportGenerator
{
    protected SqlLogger $sqlLogger;
    private array $arrReportsList;
    public function __construct(SqlLogger $sqlLogger)
    {

        $this->sqlLogger = $sqlLogger;
        $this->createReportsList();
        return $this;
    }

    public function buildReports($resort, $meta_key, $meta_value, $post_id, $post_name) : void
    {
        foreach ($this->arrReportsList as $report) {
            $report->buildReport($resort, $meta_key, $meta_value, $post_id, $post_name);
        }
    }

    public function renderReports($twig) : void
    {
        foreach ($this->arrReportsList as $report) {
            $report->renderReport($twig);
        }
    }
    private function createReportsList() : void
    {
        // Look for every class extends WebsiteReportsBase and then instantiate it
        // then add to the array of Report
        $this->arrReportsList = array();
        $arrClasses = get_declared_classes();
        foreach ($arrClasses as $class) {
            if (is_subclass_of($class, 'SCGB\Report\WebsiteReportsBase')) {
                $report = new $class($this->sqlLogger);
                $reportName = $report->getReportName();
                $this->arrReportsList[$reportName] = $report;
            }
        }
    }

    /**
     * @throws Exception
     */
    public function buildIndexPage($twig) : void
    {
        $arrIndex = array();
        foreach ($this->arrReportsList as $report) {
            $arrIndex[$report->getReportName()] = array('name' => $report->getReportName(), 'url' => $report->getReportURL());
        }

        ksort($arrIndex);

        $indexFile = Utils::getConfigItem('dirWebRoot') . '/' .
            Utils::getConfigItem('dirWebSupport') . '/index.html';
        file_put_contents($indexFile,
            $twig->render('index.html.twig', array('url' => WebsiteReportsBase::SKICLUB_URL, 'name' => 'Report Index', 'data' => $arrIndex)));
    }

    public function getReport(string $reportName) : WebsiteReportsBase
    {
        return $this->arrReportsList[$reportName];
    }

}