<?php
declare(strict_types=1);
namespace SCGB;

class reportConsistencyByResort extends WebsiteReportsBase
{
    const REPORT_NAME = 'Consistency - Issues By Resort';
    const REPORT_BY_RESORT_TEMPLATE = 'ConsistencyCheckByResort.html.twig';
    public function __construct(SqlLogger $sqlLogger)
    {
        $this->sqlLogger = $sqlLogger;
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resorts with reps - looking for meta_key = resort_rep_resort
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        //NOOP  -  we don't want to do anything here
    }

    public function addReportData(mixed $data): void
    {
        $this->reportData = $data;
    }

    public function renderReport($twig) : void
    {
        $resorts = $this->reportData;
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_BY_RESORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $resorts)));
    }
}