<?php
declare(strict_types=1);
namespace SCGB;

class reportFreshTracksResorts extends WebsiteReportsBase
{
    const REPORT_NAME = 'Report - Fresh Tracks Resort';
    const REPORT_TEMPLATE = 'resortList.html.twig';
    public function __construct(SqlLogger $sqlLogger)
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resorts with Fresh Tracks holidays - looking for meta_key = resort_freshtrack_holidays
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        if ($meta_key == 'resort_freshtrack_holidays' && $meta_value == 'yes') {
            $this->addReportData(array('name' => $resort, 'post_id' => $post_id, 'post_name' => $post_name));
        }
    }

    public function renderReport($twig) : void
    {
        // set the path of the template directory relative to here
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $this->reportData)));
    }
}