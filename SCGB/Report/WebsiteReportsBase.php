<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class WebsiteReportsBase
{
    const SKICLUB_URL = 'https://newsite.skiclub.co.uk';
    protected ?SqlLogger $sqlLogger;
    protected array $reportData = array();
    protected string $reportName = '';
    protected string $reportUrl = '';
    protected string $reportFilename = '';
    protected string $reportDirectory = '';

    /**
     * @throws Exception
     */
    public function __construct(string $reportName, string $reportUrl, ?SqlLogger $sqlLogger = null)
    {
        $this->reportName = $reportName;
        $this->reportUrl = $reportUrl;
        $this->sqlLogger = $sqlLogger;
        $this->reportDirectory = Utils::getConfigItem('dirWebRoot') . '/' . Utils::getConfigItem('dirWebSupport');
        $this->reportFilename = $this->reportDirectory . '/' . $this->reportUrl;
        return $this;
    }

    public function addReportData(mixed $data) : void
    {
        $this->reportData[] = $data;
    }

    public function getReportData() : array
    {
        return $this->reportData;
    }

    protected function getHTMLFilename($reportName) : string
    {
        // Strip out spaces and make first character lowercase
        $reportName = lcfirst(str_replace(' ', '', $reportName));
        $reportName = str_replace('\'', '', $reportName);
        $reportName = str_replace('/', '', $reportName);
        return $reportName . '.html';
    }

    // Getters
    public function getReportName() : string
    {
        return $this->reportName;
    }

    public function getReportUrl() : string
    {
        return $this->reportUrl;
    }
}