<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class reportSugatiTours extends WebsiteReportsBase
{
    const REPORT_NAME = 'Report - Sugati Tour Information';
    const REPORT_TEMPLATE = 'sugatiTours.html.twig';
    public function __construct()
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME));
        return $this;
    }

    public function buildReport(string $resort, $meta_key, $meta_value, $post_id, $post_name) : void
    {
        // NOOP - This report created on its own
    }

    /**
     * Report on Sugati Tour ID's in the holiday which link the website holiday to Sugati.
     *
     * The report will show the
     * following:
     * - Tour ID's in two places
     * - Unused Tour ID's
     * - Tour ID name vs Holiday name
     *
     * @throws Exception
     */
    public function renderReport($twig) : void
    {
        // We only do anything if called by processToursFromSugati.php
        if (basename ($_SERVER['PHP_SELF']) !== 'processToursFromSugati.php') {
            Utils::logger()->info('Sugati Tours report suppressed as not called by processToursFromSugati.php',
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__));
            return;
        }
        $arrHolidayData = array();
        $arrDupeTourIds = array();
        $arrInvalidTourIds = array();
        $arrUnusedTours = array();
        foreach (SugatiTour::getSugatiTourIDs() as $tour_id => $tour) {
            $arrUnusedTours[$tour->getSugatiTourName()] = $tour_id;
        }

        foreach ($this->getReportData() as $holiday) {
            $arrData = array();
            $arrSugatiTours = array();
            $holiday_name = $holiday->getHolidayName();

            // Info for adding URLS
            $post_id = Post::getPostIDFromName($holiday_name);
            if ($post_id === null) {
                $post_id = 0;
                $post_name = 'unknown';
            } else {
                $post_name = Post::getPostNameFromID($post_id);
            }
            $edit_url = self::SKICLUB_URL . '/wp-admin/post.php?post=' . $post_id . '&action=edit';
            $view_url = self::SKICLUB_URL . '/holiday/' . $post_name;
            $arrData['edit_url'] = $edit_url;
            $arrData['view_url'] = $view_url;

            foreach($holiday->getSugatiTourIDs() as $sugatiTourID) {
                if (in_array($sugatiTourID, $arrUnusedTours)) {
                    $name = array_search($sugatiTourID, $arrUnusedTours);
                    unset($arrUnusedTours[$name]);
                } else {
                    if (!key_exists($sugatiTourID, $arrInvalidTourIds))
                        $arrInvalidTourIds[$sugatiTourID] = array(
                            'holiday_name' => $holiday_name,
                            'edit_url' => $edit_url,
                            'view_url' => $view_url,
                        );
                    $arrDupeTourIds[$sugatiTourID][] = array(
                        'holiday_name' => $holiday_name,
                        'edit_url' => $edit_url,
                        'view_url' => $view_url,
                    );

                }
                $sugatiTour = SugatiTour::getSugatiTour($sugatiTourID);
                if ($sugatiTour !== null) {
                    $arrSugatiTours[$sugatiTourID] = $sugatiTour->getSugatiTourName();
                } else {
                    $arrSugatiTours[$sugatiTourID] = 'Invalid ID';
                }

            }
            $arrData['sugati_tours'] = $arrSugatiTours;
            $arrData['holiday_name'] = $holiday_name;
            $arrHolidayData[$holiday_name] = $arrData;
        }

        // Remove entries from $arrDupeTourIds that only have one entry
        foreach ($arrDupeTourIds as $sugatiTourID => $arrHolidayNames) {
            if (count($arrHolidayNames) === 1) {
                unset($arrDupeTourIds[$sugatiTourID]);
            }
        }

        // sort the arrays
        ksort($arrHolidayData);
        ksort($arrDupeTourIds);
        ksort($arrUnusedTours);
        ksort($arrInvalidTourIds);

        // To make the template generation simpler find the largest count of tour ids in a holiday
        $maxSugatiTours = 0;
        foreach ($arrHolidayData as $holiday_name => $holiday) {
            $sugati_tour_count = count($holiday['sugati_tours']);
            $maxSugatiTours = max($maxSugatiTours, $sugati_tour_count);
            $arrHolidayData[$holiday_name]['sugati_tour_count'] = $sugati_tour_count;
        }
        $arrHolidayData['max_sugati_tours'] = $maxSugatiTours;

        // set the path of the template directory relative to here
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName,
                'holiday_data' => $arrHolidayData, 'invalid_tour_ids' => $arrInvalidTourIds,
                'dupe_tour_ids' => $arrDupeTourIds, 'unused_tour_ids' => $arrUnusedTours)));
    }
}