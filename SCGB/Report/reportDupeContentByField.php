<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

class reportDupeContentByField extends WebsiteReportsBase
{
    const REPORT_NAME = 'Consistency - Content Fields with Duplicate Values';
    const REPORT_TEMPLATE = 'dupeContentByField.html.twig';
    const REPORT_TEMPLATE2 = 'dupeContentByFieldCount.html.twig';
    private array $arrDupesByField = array();

    const ignoreContent = array  (
        "Check out the Ski Club's holidays available in this resort",
        "Click the link below to find out about",
        "Body copy. Lorem ipsum dolor sit amet",
        "This section may have other information",
        "We have X amount of holidays",
        "This will be standard content with links to external",
        "DayEventMeeting timeMeeting",
	"Dates for the 2023/24 season are still to be confirmed",
	"by joining our resort facebook group or resort group",
	"by joining our resort Facebook group or resort group",
	"If no community tab, this component does not show",
	"There are two main airport options, Venice and Verona",
	"The Chamonix Valley is incredibly accessible",
	"Direct flights to Calgary airport are by far the best option",
	"Unique ski experience in the winter wonderland",
	"Upper mountain can close and be bleak in bad weather",
    );
    public function __construct(SqlLogger $sqlLogger)
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     *
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        // if the key is resort_intro_text ends in _content, _pros or _cons then add it to the array
        $words = explode('_', $meta_key);
        $last_word = array_pop($words);
        if ($meta_key == 'resort_intro_text' || in_array($last_word, array('content', 'pros', 'cons')) && $meta_value != '') {
            // remove HTML tags
            $meta_value = strip_tags($meta_value);

            // remove any \r \t \n
            $meta_value = str_replace(array("\r", "\n", "\t"), '', $meta_value);

            // remove any extra spaces
            $meta_value = preg_replace('/\s+/', ' ', $meta_value);

            // remove &nbsp;
            $meta_value = str_replace('&nbsp;', '', $meta_value);

            if ($meta_value == '') {
                return;
            }

            // see if this to be ignored
            foreach (self::ignoreContent as $ignore) {
                if (str_starts_with($meta_value, $ignore)) {
                    return;
                }
            }

            // hash on the first 100 characters
            $meta_value_sha1 = sha1(substr($meta_value, 0, 100));
            // if the value doesn't exist then create it
            if (!array_key_exists($meta_key, $this->arrDupesByField)) {
                $this->arrDupesByField[$meta_key] = array();
            }
            if (!array_key_exists($meta_value_sha1, $this->arrDupesByField[$meta_key])) {
                $this->arrDupesByField[$meta_key][$meta_value_sha1] = array(
                    'content' => $meta_value,
                    'resorts' => array()
                );
            }

            // add the post_id to the array
            $this->arrDupesByField[$meta_key][$meta_value_sha1]['resorts'][$post_id] = array('post_name' => $post_name, 'resort' => $resort);
        }
    }

    /**
     * @throws Exception
     */
    public function renderReport($twig) : void
    {
        // Remove any fields that don't have duplicates
        foreach ($this->arrDupesByField as $meta_key => $shaContent) {
            foreach ($shaContent as $sha1 => $arrContent) {
                if (count($arrContent['resorts']) < 2) {
                    unset($this->arrDupesByField[$meta_key][$sha1]);
                } else {

                    // Sort the resorts by post_id as the first is most likely the original
                    ksort($arrContent['resorts']);

                    // Construct some SQL to blank the duplicate values
                    $this->sqlLogger->deleteDuplicateContent($meta_key, $sha1, $this->arrDupesByField[$meta_key][$sha1]['resorts']);

                    // Render the content specific report in filename replace 'consistency-' with the hash
                    $filename = str_replace('consistency-', $sha1 . '-', $this->reportFilename);
                    $this->arrDupesByField[$meta_key][$sha1]['filename'] = basename($filename);
                    $reportName = 'Dupe Content Report for ' . $meta_key;
                    file_put_contents($filename,
                        $twig->render(self::REPORT_TEMPLATE2,
                            array(
                                'url' => self::SKICLUB_URL,
                                'meta_key' => $meta_key,
                                'name' => $reportName,
                                'data' => $arrContent)));
                }
            }

            if (count($this->arrDupesByField[$meta_key]) == 0) {
                unset($this->arrDupesByField[$meta_key]);
            }
        }

        // set the path of the template directory relative to here
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $this->arrDupesByField)));
    }
}
