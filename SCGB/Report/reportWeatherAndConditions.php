<?php
declare(strict_types=1);
namespace SCGB;

use Exception;
use mysqli;

class reportWeatherAndConditions extends WebsiteReportsBase
{
    const REPORT_NAME = 'Report - Weather and Conditions';
    const REPORT_TEMPLATE = 'WeatherAndConditions.html.twig';

    protected array $weatherData = array();
    public function __construct(SqlLogger $sqlLogger)
    {
        parent::__construct(self::REPORT_NAME, $this->getHTMLFilename(self::REPORT_NAME), $sqlLogger);
        return $this;
    }

    /**
     * Creating a list of resorts with reps - looking for meta_key = resort_rep_resort
     * @param string $resort
     * @param string $meta_key
     * @param string $meta_value
     * @param string $post_id
     * @param string $post_name
     * @return void
     */
    public function buildReport(string $resort, string $meta_key, string $meta_value, string $post_id, string $post_name) : void
    {
        // Collect the Weather API ID from the WordPress Metadata
        if ($meta_key == 'resort_weather_resort_api_id') {
            $this->reportData[$meta_value]['resort'] = $resort;
            $this->reportData[$meta_value]['post_id'] = $post_id;
            $this->reportData[$meta_value]['post_name'] = $post_name;
        }
    }

    /**
     * Get Weather and conditions from the table scgb_resort_data.
     *
     * @param mysqli $conn
     * @return void
     */
    public function addWeatherData(mysqli $conn) : void
    {
        // $sql = "SELECT * FROM scgb_resort_data";
        $sql = "SELECT * FROM scgb_resort_data";
        $result = $conn->query($sql);

        while ($data = $result->fetch_assoc()) {
            // Add the API information to the reportData array
            if (key_exists($data['intSCGBResortID'], $this->reportData)) {
                $resort = $this->reportData[$data['intSCGBResortID']]['resort'];
                $post_name = $this->reportData[$data['intSCGBResortID']]['post_name'];
                $post_id = $this->reportData[$data['intSCGBResortID']]['post_id'];
            }else {
                $resort = 'Unknown - ' . $data['strResortName'];
                $post_id = 0;
                $post_name = 'Unknown';
            }
            $this->weatherData[$resort]['weather'] = $data;
            $this->weatherData[$resort]['post_id'] = $post_id;
            $this->weatherData[$resort]['post_name'] = $post_name;
            $this->weatherData[$resort]['forecast'] = $this->getForecast($conn, intval($data['intSCGBResortID']));
        }
        ksort($this->weatherData);
    }

    public function getForecast(mysqli $conn, int $resortID) : array
    {
        $sql = "SELECT * FROM scgb_resort_forecast " .
            "WHERE intSCGBResortID = $resortID ".
            "Order By blnIsTopForecast ,dtmForecastDate";
        $result = $conn->query($sql);
        $forecast = array();
        while ($data = $result->fetch_assoc()) {
            // Remove intSCGBResortID, strResortName & strResortCountry from the array
            unset($data['intSCGBResortID']);
            unset($data['strResortName']);
            unset($data['strResortCountry']);

            // If the key ends with URL then add the Ski Club URL to the front of the value
            foreach ($data as $key => $value) {
                if (str_ends_with($key, 'URL')) {
                    $data[$key] = self::SKICLUB_URL . $value;
                }
            }
            $forecast[] = $data;
        }
        return $forecast;
    }

    public function addReportData(mixed $data): void
    {
        // NOOP resort_weather_resort_api_id
    }

    /**
     * @throws Exception
     */
    public function renderReport($twig) : void
    {
        if (Utils::getConfigItem('suppressWeather', false)) {
            Utils::logger()->info('Weather report suppressed');
            return;
        }
        $weatherData = $this->weatherData;

        // Remove unnecessary data from array, Fix the URLs and render the forecasts
        $arrDontWant = array('intSCGBResortID', 'strResortName', 'strResortCountry',);
        foreach ($weatherData as $resort => $resortData) {
            foreach ($resortData['weather'] as $key => $value) {
                // If $key in the array $arrDontWant then remove from $weatherData
                if (in_array($key, $arrDontWant)) {
                    unset($weatherData[$resort]['weather'][$key]);
                }

                // If the key ends with URL then add the Ski Club URL to the front of the value
                if (str_ends_with($key, 'URL')) {
                    $weatherData[$resort]['weather'][$key] = self::SKICLUB_URL . $value;
                }

                $reportFilename = $this->getHTMLFilename($resortData['post_name'] . '_forecast');
                $reportFilename = Utils::getConfigItem('dirWebRoot') . '/' . Utils::getConfigItem('dirWebSupport') . '/' . $reportFilename;
                file_put_contents($reportFilename,
                    $twig->render('Forecast.html.twig', array(
                        'url' => self::SKICLUB_URL,
                        'name' => $resort,
                        'post_id' => $resortData['post_id'],
                        'post_name' => $resortData['post_name'],
                        'data' => $resortData)));
            }
        }
        file_put_contents($this->reportFilename,
            $twig->render(self::REPORT_TEMPLATE, array('url' => self::SKICLUB_URL, 'name' => $this->reportName, 'data' => $weatherData)));
    }
}
