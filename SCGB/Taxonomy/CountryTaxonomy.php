<?php

namespace SCGB;

use Exception;
use mysqli;

class CountryTaxonomy extends Taxonomy
{
    /**
     * @throws Exception
     */
    static public function getCountriesByPostID(?int $post_id, mysqli $conn, bool $child_only = false): array | null | Taxonomy
    {
        if ($post_id === null)
            return null;
        $arrCountries = self::getTaxonomies($post_id, $conn, self::COUNTRY);
        if (!$child_only || $arrCountries === null)
            return $arrCountries;
        $country_taxonomy = null;
        foreach ($arrCountries as $country) {
            if ($country->getParentTaxonomyTermID() != 0) {
                $country_taxonomy = $country;
                break;
            }
        }
        return $country_taxonomy;
    }

    /**
     * @throws Exception
     */
    static public function updateCountries(string $post_name, int $post_id, array $arrCountries, SqlLogger $sqlLogger): void
    {
        self::updateTaxonomies($post_name, $post_id, self::COUNTRY, $arrCountries, $sqlLogger);
    }
    /**
     * @throws Exception
     */
    static public function getCountryByName(string $country): CountryTaxonomy
    {
        if (!key_exists($country, self::$arrTaxonomyByName[self::COUNTRY])) {
            throw new Exception('Country not found: ' . $country);
        }
        return new CountryTaxonomy(self::$arrTaxonomyByName[self::COUNTRY][$country]);
    }

    public static function getPostsOnTheSameContinent($post_id) : array
    {
        return Taxonomy::getPostsWithSameParent($post_id, self::COUNTRY);
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomyPostID(int $post_id, ?Taxonomy $taxonomy) : void
    {
        Taxonomy::updateTaxonomyForPostID($post_id, Taxonomy::COUNTRY, $taxonomy);
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomiesPostID(int $post_id, array $arrTaxonomies) : void
    {
        Taxonomy::updateTaxonomiesForPostID($post_id, Taxonomy::COUNTRY, $arrTaxonomies);
    }

    /**
     * @throws Exception
     */

    public static function getCountryByPostID(int|string $post_id) : ?Taxonomy
    {
        if (!key_exists($post_id, self::$arrTaxonomyByPostID[self::COUNTRY])) {
            throw new Exception('Country not found: ' . $post_id);
        }
        $ret = null;
        $data = self::$arrTaxonomyByPostID[self::COUNTRY][$post_id]['data'];
        foreach ($data as $taxonomy) {
            if (count($taxonomy->arrChildren) == 0)
                $ret =  $taxonomy;
        }
        return $ret;
    }
}