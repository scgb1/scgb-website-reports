<?php

namespace SCGB;

use Exception;
use mysqli;

class HolidayTypeTaxonomy extends Taxonomy
{
    public function __construct(array $holidayType)
    {
        parent::__construct($holidayType);
        return $this;
    }

    /**
     * @throws Exception
     */
    static public function getHolidayTypeByPostID(int $post_id, mysqli $conn): null | Taxonomy
    {
        $arrHolidayTypes = self::getTaxonomies($post_id, $conn, self::HOLIDAY_TYPE);
        if ($arrHolidayTypes === null)
            return null;

        if (count($arrHolidayTypes) > 1)
            Utils::logger()->info('Multiple Holiday Types found for post_id: ' . $post_id . ' - using first one', array('file' => basename(__FILE__), 'line' => __LINE__));
        // pop the first one off the array
        return array_pop($arrHolidayTypes);
    }

    /**
     * @throws Exception
     */
    static public function updateHolidayType(string $post_name, int $post_id, array $arrHolidayTypes, SqlLogger $sqlLogger): void
    {
        self::updateTaxonomies($post_name, $post_id, self::HOLIDAY_TYPE, $arrHolidayTypes, $sqlLogger);
    }
    /**
     * @throws Exception
     */
    static public function getHolidayTypeByName(string $holidayType): Taxonomy
    {
        if (!key_exists($holidayType, self::$arrTaxonomyByName[self::HOLIDAY_TYPE])) {
            throw new Exception('Holiday Type not found: ' . $holidayType);
        }
        return self::$arrTaxonomyByName[self::HOLIDAY_TYPE][$holidayType];
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomyPostID(int $post_id, ?Taxonomy $taxonomy) : void
    {
        Taxonomy::updateTaxonomyForPostID($post_id, Taxonomy::HOLIDAY_TYPE, $taxonomy);
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomiesPostID(int $post_id, array $arrTaxonomies) : void
    {
        Taxonomy::updateTaxonomiesForPostID($post_id, Taxonomy::HOLIDAY_TYPE, $arrTaxonomies);
    }
}