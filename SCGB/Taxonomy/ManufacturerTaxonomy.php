<?php

namespace SCGB;

use Exception;
use mysqli;

class ManufacturerTaxonomy extends Taxonomy
{
    public function __construct(array $skiManufacturer)
    {
        parent::__construct($skiManufacturer);
        return $this;
    }

    /**
     * @throws Exception
     */
    public static function updateSkiManufacturer(string $post_name, int $post_id, Taxonomy $skiManufacturer, SqlLogger $sqlLogger): void
    {
        self::updateTaxonomies($post_name, $post_id, self::SKI_MANUFACTURER, array($skiManufacturer), $sqlLogger);
    }
    /**
     * @throws Exception
     */
    static public function getSkiManufacturer(int $post_id, mysqli $conn): Taxonomy | null
    {
        // should only be one!
        $arrManufacturer = self::getTaxonomies($post_id, $conn, self::SKI_MANUFACTURER);
        if ($arrManufacturer === null) {
            Utils::logger()->warning('No manufacturer found for post_id: ' . $post_id);
            return null;
        }
        if (count($arrManufacturer) > 1) {
            Utils::logger()->warning('More than one manufacturer found for post_id: ' . $post_id);
            return null;
        }
        // Pop the first element of the array
        return array_pop($arrManufacturer);
    }

    /**
     * @throws Exception
     */
    static public function getSkiManufacturerByName(string $skiManufacturer): Taxonomy
    {
        if (!key_exists($skiManufacturer, self::$arrTaxonomyByName[self::SKI_MANUFACTURER])) {
            throw new Exception('Ski Manufacturer not found: !' . $skiManufacturer . '!');
        }
       return self::$arrTaxonomyByName[self::SKI_MANUFACTURER][$skiManufacturer];
    }
}