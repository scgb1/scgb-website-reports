<?php

namespace SCGB;

use Exception;
use mysqli;

class StandardTaxonomy extends Taxonomy
{
    /**
     * @throws Exception
     */
    static public function getSkiStandards(int $post_id, mysqli $conn): array | null
    {
        $arrSkiCategories = null;
        $arrTaxonomies = self::getTaxonomies($post_id, $conn, self::SKI_STANDARD);
        if ($arrTaxonomies !== null) {
            foreach ($arrTaxonomies as $taxonomy) {
                $parent_taxonomy_term_id = $taxonomy->getParentTaxonomyTermID();
                if ($parent_taxonomy_term_id == 0)
                    continue;
                $taxonomy_parent_name = self::$arrTaxonomyByTermID[$parent_taxonomy_term_id]->getTaxonomyName();
//                if ($taxonomy_parent_name != 'On piste level' && $taxonomy_parent_name != 'Off piste level')
//                    continue;
                $taxonomy_name = $taxonomy->getTaxonomyName();
                $arrSkiCategories[$taxonomy_parent_name][$taxonomy_name] = $taxonomy;
            }
        }
        return $arrSkiCategories;
    }

    /**
     * @throws Exception
     */
    static public function createTaxonomyList(string $onOff, string $minStandard, string $maxStandard) : array
    {
        $arrTaxonomies = [];

        $started = false;
        $finished = false;
        $pisteLevel = self::$arrTaxonomyByName[self::SKI_STANDARD][$onOff];
        foreach ($pisteLevel->arrChildren as $taxonomy) {
            if ($taxonomy->getTaxonomyName() == $minStandard)
                $started = true;
            if ($started)
                $arrTaxonomies[] = $taxonomy;
            if ($taxonomy->getTaxonomyName() == $maxStandard) {
                $finished = true;
                break;
            }
        }
        if (!$started)
            throw new Exception('Invalid minStandard value: ' . $minStandard);
        if (!$finished)
            throw new Exception('Invalid maxStandard value: ' . $maxStandard);
        return $arrTaxonomies;
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomyPostID(int $post_id, ?Taxonomy $taxonomy) : void
    {
        Taxonomy::updateTaxonomyForPostID($post_id, Taxonomy::SKI_STANDARD, $taxonomy);
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomiesPostID(int $post_id, array $arrTaxonomies) : void
    {
        Taxonomy::updateTaxonomiesForPostID($post_id, Taxonomy::SKI_STANDARD, $arrTaxonomies);
    }

//    /**
//     * @throws Exception
//     */
//    static public function getSkiStandard(string $skiStandard): StandardTaxonomy
//    {
//        if (!key_exists($skiStandard, self::$arrTaxonomyByName[self::SKI_STANDARD])) {
//            throw new Exception('Ski CategoryTaxonomy not found: ' . $skiStandard);
//        }
//        return new StandardTaxonomy(self::$arrTaxonomyByName[self::SKI_CATEGORY][$skiStandard]);
//    }
}