<?php

namespace SCGB;

use Exception;
use mysqli;


class AbilityTaxonomy extends Taxonomy
{
    public function __construct(array $skiAbility)
    {
        parent::__construct($skiAbility);
        return $this;
    }

    /**
     * @throws Exception
     */
    static public function getSkiAbilities(int $post_id, mysqli $conn): array | null
    {
        return self::getTaxonomies($post_id, $conn, self::SKI_ABILITY);
    }

    /**
     * @throws Exception
     */
    static public function updateSkiAbilities(string $post_name, int $post_id, array $arrSkiAbilities, SqlLogger $sqlLogger): void
    {
        self::updateTaxonomies($post_name, $post_id, self::SKI_ABILITY, $arrSkiAbilities, $sqlLogger);
    }
    /**
     * @throws Exception
     */
    static public function getSkiAbility(string $skiAbility): Taxonomy
    {
        if (!key_exists($skiAbility, self::$arrTaxonomyByName[self::SKI_ABILITY])) {
            throw new Exception('Ski Ability not found: ' . $skiAbility);
        }
        return self::$arrTaxonomyByName[self::SKI_ABILITY][$skiAbility];
    }
}