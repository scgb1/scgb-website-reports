<?php

namespace SCGB;

use Exception;
use mysqli;

class CategoryTaxonomy extends Taxonomy
{
    public function __construct(array $skiCategory)
    {
        parent::__construct($skiCategory);
        return $this;
    }

    /**
     * @throws Exception
     */
    public static function updateSkiCategories(string $post_name, int $post_id, array $arrSkiCategories, SqlLogger $sqlLogger): void
    {
        self::updateTaxonomies($post_name, $post_id, self::SKI_CATEGORY, $arrSkiCategories, $sqlLogger);
    }


    /**
     * @throws Exception
     */
    static public function getSkiCategories(int $post_id, mysqli $conn): array | null
    {
        return self::getTaxonomies($post_id, $conn, self::SKI_CATEGORY);
    }

    /**
     * @throws Exception
     */
    static public function getSkiCategory(string $skiCategory): Taxonomy
    {
        if (!key_exists($skiCategory, self::$arrTaxonomyByName[self::SKI_CATEGORY])) {
            throw new Exception('Ski Category not found: ' . $skiCategory);
        }
        return self::$arrTaxonomyByName[self::SKI_CATEGORY][$skiCategory];
    }

}