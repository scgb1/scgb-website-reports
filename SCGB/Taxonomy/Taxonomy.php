<?php

namespace SCGB;

use Exception;
use mysqli;

class Taxonomy
{
    public const SKI_ABILITY = 'ski_ability';
    public const SKI_CATEGORY = 'ski_category';
    public const SKI_MANUFACTURER = 'ski_manufacturer';
    public const SKI_STANDARD = 'ski_standard';
    public const HOLIDAY_TYPE = 'holiday_type';
    public const COUNTRY = 'country';
    static protected array $arrTaxonomyByName = array();
    static protected array $arrTaxonomyByPostID = array();
    static protected array $arrTaxonomyByTermID = array();
    static private ?mysqli $conn = null;
    protected string $taxonomy_name;
    protected int $taxonomy_term_id;
    protected int $taxonomy_parent_term_id;
    protected array $arrChildren = array();

    /**
     * @throws Exception
     */
    public function __construct(array $taxonomy)
    {
        $taxonomy_name = $taxonomy['name'];
        $taxonomy_term_id = $taxonomy['term_id'];
        $taxonomy_parent_term_id = $taxonomy['parent'];

        $this->taxonomy_name = $taxonomy_name;
        $this->taxonomy_term_id = $taxonomy_term_id;
        $this->taxonomy_parent_term_id = $taxonomy_parent_term_id;

        return $this;
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomyForPostID(int $post_id, string $taxonomy_name, ?Taxonomy $taxonomy) : void
    {
        if ($taxonomy === null)
            Taxonomy::updateTaxonomiesForPostID($post_id, $taxonomy_name, array());
        else
            Taxonomy::updateTaxonomiesForPostID($post_id, $taxonomy_name, array($taxonomy));
    }

    /**
     * @throws Exception
     */
    static public function updateTaxonomiesForPostID(int $post_id, string $taxonomy_name, array $arrTaxonomies) : void
    {
        $sql = "START TRANSACTION;" . "\n";
        // delete the old taxonomies
        $sql .= "DELETE FROM wp_term_relationships " .
            "WHERE object_id = $post_id " .
            "AND term_taxonomy_id in (SELECT term_taxonomy_id FROM wp_term_taxonomy WHERE taxonomy = '$taxonomy_name');" . "\n";

        // insert the new taxonomies
        foreach ($arrTaxonomies as $taxonomy) {
            $sql .= "INSERT INTO " .
                "wp_term_relationships (object_id, term_taxonomy_id, term_order) " .
                "values ($post_id, " . $taxonomy->getTaxonomyTermID() . ", 0);" . "\n";
        }
        $sql .= "COMMIT;" . "\n";
        if (self::$conn->multi_query($sql) === false) {
            throw new Exception('Error updating taxonomies: ' . self::$conn->error . "\n" . $sql);
        }
        while (self::$conn->more_results()) {
            self::$conn->next_result();
        }
    }

    public function getName(): string
    {
        return $this->taxonomy_name;
    }

    public function getTaxonomyTermID(): int
    {
        return $this->taxonomy_term_id;
    }
    public function getParent() : Taxonomy
    {
        return self::$arrTaxonomyByTermID[$this->taxonomy_parent_term_id];
    }
    public static function getParentTaxonomy(string $taxonomy_name, $post_id) : ?Taxonomy
    {
        $taxonomy = self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['data'];
        $ret = null;
        foreach ($taxonomy as $tax) {
            if ($tax->taxonomy_parent_term_id == 0) {
                $ret = $tax;
            }
        }
        return $ret;
    }
    public static function getTaxonomy(string $taxonomy_name, $post_id) : ?Taxonomy
    {
        $taxonomy = self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['data'];
        $ret = null;
        foreach ($taxonomy as $tax) {
            if ($tax->taxonomy_parent_term_id != 0) {
                $ret = $tax;
            }
        }
        return $ret;
    }
    static public function getPostsWithSameParent(int $post_id, string $taxonomy_name) : array
    {
        $target_taxonomy = self::getParentTaxonomy($taxonomy_name, $post_id);
        $arrPostsWithSameParent = array();
        foreach(self::$arrTaxonomyByPostID[$taxonomy_name] as $search_post_id => $taxonomy) {
            $this_taxonomy = self::getParentTaxonomy($taxonomy_name, $search_post_id);
            if ($this_taxonomy === null)
                continue;
            if ($target_taxonomy->taxonomy_name == $this_taxonomy->taxonomy_name) {
                $arrPostsWithSameParent[] = $search_post_id;
            }
        }
        return $arrPostsWithSameParent;
    }

    static public function isDifferent(?array $taxonomy1, ?array $taxonomy2): bool
    {
        if ($taxonomy1 !== null && $taxonomy2 !== null) {
            return array_diff($taxonomy1, $taxonomy2) !== array_diff($taxonomy2, $taxonomy1);
        }
        return $taxonomy1 !== $taxonomy2;
    }

    /**
     * @throws Exception
     */
    static protected function getTaxonomies(int $post_id, mysqli $mysqli, string $taxonomy_name): array | null
    {
        if (self::$conn === null) {
            self::$conn = $mysqli;
        }
        if (!key_exists($taxonomy_name, self::$arrTaxonomyByName)) {
            self::$arrTaxonomyByName[$taxonomy_name] = array();
            self::$arrTaxonomyByPostID[$taxonomy_name] = array();
            self::loadTaxonomies($mysqli, $taxonomy_name);
            self::loadPostTaxonomies($mysqli, $taxonomy_name);
        }

        $taxonomies = null;
        if (key_exists($post_id, self::$arrTaxonomyByPostID[$taxonomy_name])) {
            $taxonomies = self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['data'];
        }
        return $taxonomies;
    }

    /**
     * @throws Exception
     */
    static private function loadTaxonomies(mysqli $conn, string $taxonomy_name): void
    {
        if (self::$conn === null) {
            self::$conn = $conn;
        }
        $sql = "select a.term_id,  a.name, b.parent " .
            "from wp_terms a, wp_term_taxonomy b " .
            "where a.term_id = b.term_taxonomy_id " .
            "and b.taxonomy = '" . $taxonomy_name . "' " .
            "order by b.parent, a.term_id, b.taxonomy;";
        $result = $conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load taxonomy data: ' . $taxonomy_name . ' - ' . $conn->error);
        }

        while ($row = $result->fetch_assoc()) {
            $term_id = intval($row['term_id']);
            $name = trim($row['name']);
            $parent = intval($row['parent']);

            $taxonomy = new Taxonomy(array('term_id' => $term_id, 'name' => $name, 'parent' => $parent));
            self::$arrTaxonomyByTermID[$term_id] = $taxonomy;
            if ($parent == 0) {
                self::$arrTaxonomyByName[$taxonomy_name][$name] = $taxonomy;
            } else {
                foreach (self::$arrTaxonomyByName[$taxonomy_name] as $key => $value) {
                    if ($value->getTaxonomyTermID() == $parent) {
                        $parent = $key;
                        break;
                    }
                }
                $parent_taxonomy = self::$arrTaxonomyByName[$taxonomy_name][$parent];
                $parent_taxonomy->addChild($taxonomy);
            }

        }
    }

    protected function addChild(Taxonomy $taxonomy) : void
    {
        $this->arrChildren[$taxonomy->taxonomy_name] = $taxonomy;
    }

    /**
     * @throws Exception
     */
    static private function loadPostTaxonomies($conn, $taxonomy_name): void
    {
        $sql = "select p.ID, p.post_title, a.term_id,  a.name " .
            "from wp_terms a, wp_term_taxonomy b, wp_posts p, wp_term_relationships r " .
            "where a.term_id = b.term_taxonomy_id " .
            "and b.taxonomy = '" . $taxonomy_name . "' " .
            "and r.term_taxonomy_id = b.term_taxonomy_id " .
            "and r.object_id = p.ID " .
            "and p.post_status = 'publish' " .
            "order by b.parent, p.post_title, a.name";

        $result = $conn->query($sql);
        if ($result === false) {
            throw new Exception('Failed to load taxonomy data for ' . $taxonomy_name . ': ' . $conn->error);
        }

        while ($row = $result->fetch_assoc()) {
            $post_id = intval($row['ID']);
            $post_title = trim($row['post_title']);
            $term_id = intval($row['term_id']);
            $name = trim($row['name']);

            if (!isset(self::$arrTaxonomyByPostID[$taxonomy_name][$post_id])) {
                self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['post_title'] = $post_title;
                self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['data'] = array();
            }
            $taxonomy = self::$arrTaxonomyByTermID[$term_id];
            self::$arrTaxonomyByPostID[$taxonomy_name][$post_id]['data'][$name] = $taxonomy;
        }
    }

    /**
     * @throws Exception
     */
    static protected function updateTaxonomies(string $post_name, int $post_id, string $taxonomy, array $arrTaxonomies, SqlLogger $sqlLogger) : void
    {
        $sqlLogger->updateTaxonomies($post_name, $post_id, $taxonomy, $arrTaxonomies);
    }

    public function __toString() : string
    {
        return $this->taxonomy_name;
    }

    public function getTaxonomyName(): string
    {
        return $this->taxonomy_name;
    }

    public function getParentTaxonomyTermID(): int
    {
        return $this->taxonomy_parent_term_id;
    }
}