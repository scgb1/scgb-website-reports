<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use mysqli;
use SCGB\Holiday\Resort\Resort;
use SCGB\Holiday\Utilities\Utils;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Post/Resort/Resort.php';

const arrWeights = array (
    'Freshtracks' => 8,
    'Rep or ILG' => 4,
    'Europe' => 3
);


// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Start of the main processing
Utils::logger()->info('Started SCGB Resort Randomiser - Roll The Dice', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    Resort::initialise($conn);
    $arrResorts = getResorts($conn);
    $arrResorts = randomiseResorts($arrResorts);
    updateResorts($conn, $arrResorts);
    Resort::updateResortsYouMayLike();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to randomise Resort: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

Utils::logger()->info('End of SCGB Resort Randomiser', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);

// End of main processing
// NOTREACHED

/**
 * @throws Exception
 */
function getResorts(mysqli $conn) : array
{
    $arrResorts = array();

    // Get the resorts
    $sql = "select p.ID, p.post_title, a.name, c.name " .
            "from	wp_terms a, wp_term_taxonomy b, wp_terms c, wp_posts p, wp_term_relationships r " .
            "where	a.term_id = b.term_taxonomy_id " .
            "and	c.term_id = b.parent " .
            "and   p.ID = r.object_id " .
            "and   r.term_taxonomy_id = a.term_id " .
            "and	b.taxonomy = 'country' " .
            "and   p.post_status = 'publish' " .
            "and   p.post_type = 'sc_resort';";
    $result = $conn->query($sql);
    if ($result === false) {
        throw new Exception('Failed to get resorts: ' . $conn->error);
    }
    while ($row = $result->fetch_row()) {
        $post_id = $row[0];
        $post_title = $row[1];
        $country = $row[2];
        $region = $row[3];

        $arrResorts[$post_id] = array(
            'post_id' => $post_id,
            'post_title' => $post_title,
            'country' => $country,
            'region' => $region
        );
    }

    // now get the metadata
    $sql = "select post_id, meta_key, meta_value " .
        "FROM wp_postmeta, wp_posts p " .
        "where p.ID = post_id " .
        "and p.post_status = 'publish' " .
        "and meta_key in ('resort_freshtrack_holidays', 'resort_rep_resort', 'resort_instructor_led_guiding_resort') " .
        "and p.post_type = 'sc_resort';";

    $result = $conn->query($sql);
    if ($result === false) {
        throw new Exception('Failed to get resorts: ' . $conn->error);
    }
    $arrReturn = array('counter' => 0);
    while ($row = $result->fetch_row()) {
        $post_id = $row[0];
        $meta_key = $row[1];
        $meta_value = $row[2];

        if ($meta_key == 'resort_freshtrack_holidays' && $meta_value == 'yes') {
            $weight = arrWeights['Freshtracks'];
        } elseif (($meta_key == 'resort_rep_resort') || ($meta_key == 'resort_instructor_led_guiding_resort')) {
            $weight = arrWeights['Rep or ILG'];
        } elseif ($arrResorts[$post_id]['region'] == 'Europe') {
            $weight = arrWeights['Europe'];
        } else {
            $weight = 1;
        }
        if (!key_exists('weight', $arrResorts[$post_id])) {
            $arrResorts[$post_id]['weight'] = $weight;
            $arrReturn[$weight][] = $arrResorts[$post_id];
            $arrReturn['counter']++;
        }
    }

    return $arrReturn;
}

function randomiseResorts(array $arrResorts) : array
{
    $arrRandom = array();

    $total = $arrResorts['counter'];
    while ($total > 0) {
        if (key_exists(arrWeights['Freshtracks'], $arrResorts))
            $ft_count_boundary = count($arrResorts[arrWeights['Freshtracks']]) * arrWeights['Freshtracks']-1;
        else
            $ft_count_boundary = 0;
        if (key_exists(arrWeights['Rep or ILG'], $arrResorts))
            $rep_count_boundary = $ft_count_boundary + count($arrResorts[arrWeights['Rep or ILG']]) * arrWeights['Rep or ILG'];
        else
            $rep_count_boundary = $ft_count_boundary;
        if (key_exists(arrWeights['Europe'], $arrResorts))
            $euro_count_boundary = $rep_count_boundary + count($arrResorts[arrWeights['Europe']]) * arrWeights['Europe'];
        else
            $euro_count_boundary = $rep_count_boundary;
        if (key_exists(1, $arrResorts))
            $total = $euro_count_boundary + count($arrResorts[1]);
        else
            $total = $euro_count_boundary;

        $rand = rand(0, $total-1);
        if ($rand <= $ft_count_boundary) {
            $weight = arrWeights['Freshtracks'];
            $index = floor($rand / $weight);
        } elseif ($rand <= $rep_count_boundary) {
            $weight = arrWeights['Rep or ILG'];
            $index = floor(($rand - $ft_count_boundary) / $weight);
        } elseif ($rand <= $euro_count_boundary) {
            $weight = arrWeights['Europe'];
            $index = floor(($rand - $rep_count_boundary) / $weight);
        } else {
            $weight = 1;
            $index = $rand - $euro_count_boundary;
        }
        // Find the $index element in the array
        $count = 0;
        foreach ($arrResorts[$weight] as $key => $resort) {
            if ($count++ == $index) {
                $arrRandom[] = $resort;
                unset($arrResorts[$weight][$key]);
                break;
            }
        }
    }

    return $arrRandom;
}

/**
 * @throws Exception
 */
function updateResorts(mysqli $conn, array $arrResorts) : void
{
    $count = 0;
    $sql = 'START TRANSACTION;';
    $result = $conn->query($sql);
    if ($result === false) {
        throw new Exception('Failed to start transaction: ' . $conn->error);
    }
    foreach ($arrResorts as $resort) {
        $sql = "update wp_posts set menu_order = " . $count++ . " where ID = " . $resort['post_id'] . ";";
        $result = $conn->query($sql);
        if ($result === false) {
            $sql = 'ROLLBACK;';
            $result = $conn->query($sql);
            if ($result === false) {
                throw new Exception('Failed to rollback transaction: ' . $conn->error);
            }
            throw new Exception('Failed to update resort: ' . $conn->error);
        }
    }
    $sql = 'COMMIT;';
    $result = $conn->query($sql);
    if ($result === false) {
        throw new Exception('Failed to commit transaction: ' . $conn->error);
    }
}




