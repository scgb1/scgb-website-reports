<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use SCGB\Holiday\Utilities\Utils;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Post/Resort/ResortCheckFunctions.php';
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Utilities/SqlLogger.php';
require_once __DIR__ . '/Utilities/IssueLogger.php';
require_once __DIR__ . '/Report/ReportGenerator.php';


// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
$bulkHandler = Utils::initialise();


// Look for ant php file that start with report and end with .php and require them
$files = glob(__DIR__ . '/Report/report*.php');
foreach ($files as $file) {
    require_once "$file";
}

// Fire up the Template Engine
$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader, [
    'cache' => __DIR__ . '/templates/cache',
    'debug' => true,
]);
$twig->addExtension(new DebugExtension());

// Start of the main processing
Utils::logger()->info('Started SCGB Content Validator', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {

    /**
     * Create the list of checks to run. For each meta_key there is a list of 0 or more checks to run
     */
    // Read data from the table wp_postmeta
    $sql = "SELECT p.post_type, m.post_id, post_title, post_name, meta_id, meta_key, meta_value " .
        "FROM wp_postmeta m, wp_posts p " .
        "where post_id = id  " .
        "and post_status = 'publish' " .
        "and meta_key not like '\_%' " .
        "order by p.post_type ASC, p.post_name ASC, meta_key ASC;";
    $result = $conn->query($sql);

    $fp = fopen('data/link-report.csv', 'w');

    $arrDisplayStatus = array();
    $arrMatches = array();
    $last_link_post_id = 0;
    $last_latin_post_id = 0;
    while ($data = $result->fetch_assoc()) {
        $post_type = $data['post_type'];
        $post_id = $data['post_id'];
        $post_title = $data['post_title'];
        $meta_id = $data['meta_id'];
        $meta_key = $data['meta_key'];
        $meta_value = $data['meta_value'];
        $post_name = $data['post_name'];

        // Is this a status link
        if (str_ends_with($meta_key, '_display_status')) {
            // extract any numbers from the meta_key
            preg_match_all('/_([0-9]*)_/',$meta_key , $matches);
            if (count($matches) < 2) {
                continue;
            }
            if (!key_exists(0, $matches[1])) {
                continue;
            }
            $first = intval($matches[1][0]);
            $second = 1000;
            if (count($matches[1]) == 2) {
                $second = intval($matches[1][1]);
            }
            $arrDisplayStatus[$post_id][$first][$second] = $meta_value;
        }

        // Looking for meta_value containing a URL with href="#" or href=""
        // get the first and second numbers from the meta_key
        preg_match('/_([0-9]*)_/',$meta_key , $matches);
        if (!count($matches) > 0) {
            continue;
        }
        $first = intval($matches[1]);
        $second = 1000;
        preg_match('/_[0-9]*_[a-z]*_\([0-9]*\)_/',$meta_key , $matches);
        if (count($matches) > 0) {
            $second = intval($matches[2]);
        }
        if (preg_match('/href="(#|")/', $meta_value) || str_contains($meta_value, '"url";s:1:"#"')) {
            if ($post_id != $last_link_post_id) {
                $arrMatches[$post_id][$first][$second]['link'] = array('meta_key' => $meta_key, 'post_type' => $post_type, 'post_title' => $post_title, 'post_name' => $post_name, 'issue' => 'link');
                $last_link_post_id = $post_id;
            }
        }

        // Looking for a meta_value with standard Latin Loreum Ipsum
        if (str_contains($meta_value, 'Lorem ipsum dolor sit amet')) {
            if ($post_id != $last_latin_post_id) {
                $arrMatches[$post_id][$first][$second]['latin'] = array('meta_key' => $meta_key, 'post_type' => $post_type, 'post_title' => $post_title, 'post_name' => $post_name, 'issue' => 'latin');
                $last_latin_post_id = $post_id;
            }
        }
    }

    ksort($arrMatches);
    ksort($arrDisplayStatus);
    // Now go through and see if any of the sections with match are also not marked as 'hide'
    foreach ($arrMatches as $post_id => $matchData) {
        // There must be at least one match
        foreach ($matchData as $first => $issue) {
            foreach ($issue as $second => $data) {
                if (key_exists($post_id, $arrDisplayStatus) && key_exists($first, $arrDisplayStatus[$post_id])) {
                    if (key_exists($second, $arrDisplayStatus[$post_id][$first])) {
                        if ($arrDisplayStatus[$post_id][$first][$second] == 'hide') {
                            continue;
                        }
                        foreach ($data as $item) {
                            fputcsv($fp, array($item['post_type'], $item['post_title'], $item['post_name'], $item['issue'], $post_id));
                        }
                    }
                }
            }
        }

    }

    fclose($fp);

} catch (Exception $e) {
    Utils::logger()->emergency(
        'Exception processing Resort Data: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

Utils::logger()->info('End of SCGB Website Content Validator', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED

