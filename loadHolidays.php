<?php
declare(strict_types=1);

namespace SCGB;

use Exception;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/SCGB/Utilities/scgb_autoload.php';

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Source file in first argument
if ($argc < 4) {

    Utils::logger()->emergency(
        'Usage: php loadHolidays.php source-file base-post-id meta-file',
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$sourceFile = $argv[1];
$basePostID = intval($argv[2]);
$metaFile = $argv[3];



// Start of the main processing
Utils::logger()->info('Started SCGB Holiday Loader', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

try {
    $sqlLogger = new SqlLogger($conn);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to create SQL Logger: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    Resort::initialise($conn);
    Holiday::initialise($conn);
    $postDataDictionary = new PostDataDictionary($metaFile);

    // load the ski tests from the csv file
    $arrHolidaysFromFile = HolidayFromFile::loadHolidays($sourceFile);

    // Load the current ski tests from the database
    $arrWebSiteHolidays = HolidayFromWebsite::loadHolidays($conn, $postDataDictionary, $sqlLogger);

    // Now iterate through the data read from the csv file and see what needs to be updated
    foreach ($arrHolidaysFromFile as $holiday => $holidayData) {
        if (key_exists($holiday, $arrWebSiteHolidays)) {
            Utils::logger()->info("Processing holiday: $holiday");
            // This holiday is already on the website (which it should be) - check if it needs updating
            $arrWebSiteHolidays[$holiday]->checkForUpdatesFromFile($holidayData);
        } else {
            Utils::logger()->info(
                'Holiday ' . $holiday . ' not found in database - Ignoring',
                array('file' => basename(__FILE__), 'line' => __LINE__));
            continue;
//                // create a holiday using the default Meta values and then update them
//                $arrMetaData = HolidayFromWebsite::createEmptyHoliday($holiday, $basePostID, $conn, $sqlLogger);
//                // pop the first element off the array to get the post_id
//                $post_id = array_shift($arrMetaData)['post_id'];
//                $newHoliday = new HolidayFromWebsite($holiday, $post_id, $arrMetaData, $postDataDictionary);
//                $newHoliday->checkForUpdatesFromFile($holidayData);
        }
    }
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update Holiday: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$conn->close();
Utils::logger()->info('End of SCGB Holiday Loader', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED




