<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use SCGB\Holiday\Holiday\Holiday;
use SCGB\Holiday\Rep\RepFromWebsite;
use SCGB\Holiday\Resort\Resort;
use SCGB\Holiday\Utilities\SqlLogger;
use SCGB\Holiday\Utilities\Utils;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Utilities/SqlLogger.php';

require_once __DIR__ . '/Post/PostDataDictionary.php';
require_once __DIR__ . '/Post/Post.php';
require_once __DIR__ . '/Post/Resort/Resort.php';
require_once __DIR__ . '/Post/Rep/RepFromFile.php';
require_once __DIR__ . '/Post/Rep/RepFromWebsite.php';
require_once __DIR__ . '/Post/Attachment.php';

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Start of the main processing
Utils::logger()->info('Started SCGB Rep Processor', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

try {
    $sqlLogger = new SqlLogger($conn);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to create SQL Logger: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    Resort::initialise($conn);
    Holiday::initialise($conn);

    // Load the current ski tests from the database
    $arrWebSiteReps = RepFromWebsite::loadReps($conn, null, $sqlLogger);

    // Now iterate through the data read from the csv file and see what needs to be updated
    foreach ($arrWebSiteReps as $rep => $repData) {
        Utils::logger()->info("Processing Rep: $rep");
        $repData->updateResorts();
    }
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update Resorts: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$conn->close();
Utils::logger()->info('End of SCGB Rep Processor', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED
