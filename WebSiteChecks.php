<?php

namespace SCGB;

class WebSiteChecks
{
    /**
     * @throws Exception
     */
    private function checkMediaFolders($reporter, $resort, $arrMetaKeys) : void
    {
        $post_id = $arrMetaKeys['resort_tabs']['post_id'];
        $post_name = $arrMetaKeys['resort_tabs']['post_name'];
        $country = $this->arrResortCountries[$post_id][0];

        // Get the Parent Media Folder
        $mediaFolderRoot = WordPressMediaFolder::getMediaFolderByName('Resort Media');

        // do we have a folder for the country
        $countryFolder = WordPressMediaFolder::getMediaFolderByName($country) ?? null;
        if ($countryFolder === null) {
            // TODO Create One
            return;
        }
        // Do we have a folder for the Resort
        $resortFolder = WordPressMediaFolder::getMediaFolderByName($resort) ?? null;
        if ($resortFolder === null) {
            // TODO Create One
            return;
        }

        // We want the following items to be in there
        // 1. desktop image
        // 2. mobile image
        // 3. map image
        // 4. map pdf
        // 5. Featured Image
        // 6. Gallery Images
        // If an image is used in more than one resort then it should be in the country folder
        // If an image is used in more than one country then it should be in the root folder
        // Build a map of how all the images are used
        foreach ($this->arrAttachmentPosts as $image)
        {

        }


    }
}