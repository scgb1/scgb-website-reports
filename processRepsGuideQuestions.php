<?php

// Simple script to prepare Guide/Rep Info for Holiday questions

// input file is arg1, output file is arg2
$sourceFile = $argv[1];
$destFile = $argv[2];

// source file is csv with column headers
$source = fopen($sourceFile, 'r');

// ignore the first line
fgetcsv($source);

$arrData = array();
while (($data = fgetcsv($source)) !== FALSE) {
    $type = trim($data[0]);
    $holiday_name = trim($data[1]);
    $start_date = trim($data[2]);
    $end_date = trim($data[3]);
    $first_name = trim($data[4]);
    $last_name = trim($data[5]);

    if ($last_name == 'N/A' || $last_name == 'TBC') {
        continue;
    }

    $name = $first_name . ' ' . $last_name;
    $dates = $start_date . ' - ' . $end_date;
    $arrData[] = array(
        'type' => $type,
        'holiday_name' => $holiday_name,
        'dates' => $dates,
        'name' => $name,
    );
}

$arrOutput = array();
foreach ($arrData as $data) {
    $type = $data['type'];
    $holiday_name = $data['holiday_name'];
    $dates = $data['dates'];
    $name = $data['name'];

    if (!key_exists($holiday_name, $arrOutput)) {
        $arrOutput[$holiday_name] = array();
    }
    if (!key_exists($type, $arrOutput[$holiday_name])) {
        $arrOutput[$holiday_name][$type] = array();
    }
    if (!key_exists($dates, $arrOutput[$holiday_name][$type])) {
        $arrOutput[$holiday_name][$type][$dates] = array();
    }
    $arrOutput[$holiday_name][$type][$dates][] = $name;
}

// Finally output the data
$dest = fopen($destFile, 'w');
foreach ($arrOutput as $holiday_name => $arrTypes) {
    foreach ($arrTypes as $type => $arrDates) {
        $output = '';
        foreach ($arrDates as $dates => $arrNames) {
            if (count($arrNames) == 1) {
                $output .= $dates . ' : ' . $arrNames[0] . "\n\n";
            } else {
                $output .= $dates . ' : ';
                $name = implode('!!! ', $arrNames);
                // convert the last comma to an ampersand
                $output .= preg_replace('/!!! ([^!]*)$/', ' & \1', $name) . "\n\n";
            }
        }
        // strip the last newlines
        $output = preg_replace('/\n\n$/', '', $output);
        fwrite($dest, $holiday_name . '-' . $type . ',"' . $output . '"' . "\n");
    }
}

