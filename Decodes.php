<?php

/**
 * Turn WordPress speak into plain English
 */

/**
 * Mapping of WordPress meta_key to the relevant section on Resort Edit Section
 */

return array (
    'resort_tabs_0_sections_0_section_display_status'  => '001-Overview-Ski Club Offerings',
    'resort_tabs_0_sections_1_section_display_status'  => '002-Overview-Resort Overview',
    'resort_tabs_0_sections_2_section_display_status'  => '003-Overview-Pros And Cons',
    'resort_tabs_0_sections_3_section_display_status'  => '004-Overview-Coloured BG Sections',
    'resort_tabs_0_sections_4_section_display_status'  => '005-Overview-Coloured BG Sections',
    'resort_tabs_0_sections_5_section_display_status'  => '006-Overview-Resort Lift Passes',
    'resort_tabs_0_sections_6_section_display_status'  => '007-Overview-Image Left & Right',
    'resort_tabs_0_sections_7_section_display_status'  => '008-Overview-Review Summary',
    'resort_tabs_0_sections_8_section_display_status'  => '009-Overview-Resort Holiday',
    'resort_tabs_0_sections_9_section_display_status'  => '010-Overview-Gallery',
    'resort_tabs_0_sections_10_section_display_status' => '011-Overview-Full Width FAQ',
    'resort_tabs_0_sections_11_section_display_status' => '012-Overview-Component - Resort Map',
    'resort_tabs_0_sections_12_section_display_status' => '013-Overview-Image Left & Right',
    'resort_tabs_0_sections_13_section_display_status' => '000-Don\'t Know',
    'resort_tabs_1_sections_0_section_display_status'  => '101-Community-Image Left & Right',
    'resort_tabs_1_sections_1_section_display_status'  => '102-Community-Resort Community Highlights',
    'resort_tabs_1_sections_2_section_display_status'  => '103-Community-IG Feed',
    'resort_tabs_1_sections_3_section_display_status'  => '104-Community-Template',
    'resort_tabs_1_sections_5_section_display_status'  => '105-Community-Review Summary',
    'resort_tabs_2_sections_0_section_display_status'  => '201-Maps-Resort Map',
    'resort_tabs_2_sections_1_section_display_status'  => '202-Maps-Resort Map',
    'resort_tabs_2_sections_2_section_display_status'  => '203-Maps-Resort Map',
    'resort_tabs_4_sections_0_section_display_status'  => '301-Instructor Led Guiding-Central Text',
    'resort_tabs_5_sections_0_section_display_status'  => '401-Reps-Hero Content',
    'resort_tabs_5_sections_1_section_display_status'  => '402-Reps-Central Text',
    'resort_tabs_6_sections_0_section_display_status'  => '501-Discount-Resort Discounts',
    'resort_tabs_7_sections_0_section_display_status'  => '601-Freshtracks-Resort Holiday',
    'resort_tabs_7_sections_1_section_display_status'  => '602-Freshtracks-Template',
    'sections_0_section_display_status'                => '701-Sections-You may also like',
    'sections_1_section_display_status'                => '702-Sections-Template',
    'sections_2_section_display_status'                => '703-Sections-Template',
);