<?php
declare(strict_types=1);

namespace SCGB;

use Exception;
use SCGB\Holiday\Holiday\Holiday;
use SCGB\Holiday\Rep\RepFromFile;
use SCGB\Holiday\Rep\RepFromWebsite;
use SCGB\Holiday\Resort\Resort;
use SCGB\Holiday\Utilities\SqlLogger;
use SCGB\Holiday\Utilities\Utils;

require_once(__DIR__ . '/vendor/autoload.php');
require_once __DIR__ . '/Utilities/Utils.php';
require_once __DIR__ . '/Utilities/SqlLogger.php';

require_once __DIR__ . '/Post/PostDataDictionary.php';
require_once __DIR__ . '/Post/Post.php';
require_once __DIR__ . '/Post/Resort/Resort.php';
require_once __DIR__ . '/Post/Rep/RepFromFile.php';
require_once __DIR__ . '/Post/Rep/RepFromWebsite.php';
require_once __DIR__ . '/Post/Attachment.php';

// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
Utils::initialise();

// Source file in first argument
if ($argc < 6) {

    Utils::logger()->emergency(
        'Usage: php loadReps.php source-file resort-file holiday-file base-post-id meta-file',
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$sourceFile = $argv[1];
$resortFile = $argv[2];
$repsFile = $argv[3];
$basePostID = intval($argv[4]);
$metaFile = $argv[5];



// Start of the main processing
Utils::logger()->info('Started SCGB Rep Loader', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = Utils::getDBConnection();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

try {
    $sqlLogger = new SqlLogger($conn);
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to create SQL Logger: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    Attachment::initialise($conn);
    Resort::initialise($conn);
    Holiday::initialise($conn);
    $repDataDictionary = new PostDataDictionary($metaFile);

    // load the ski tests from the csv file
    $arrRepsFromFile = RepFromFile::loadReps($sourceFile, $resortFile, $repsFile);

    // Load the current ski tests from the database
    $arrWebSiteReps = RepFromWebsite::loadReps($conn, $repDataDictionary, $sqlLogger);

    // Now iterate through the data read from the csv file and see what needs to be updated
    foreach ($arrRepsFromFile as $rep => $repData) {
        if (key_exists($rep, $arrWebSiteReps)) {
            Utils::logger()->info("Processing Rep: $rep");
            // This holiday is already on the website (which it should be) - check if it needs updating
            $arrWebSiteReps[$rep]->checkForUpdates($repData);
        } else {
            Utils::logger()->info(
                'Rep ' . $rep . ' not found in database - Creating New one',
                array('file' => basename(__FILE__), 'line' => __LINE__));
                // create a Rep using the default Meta values and then update them
                $arrMetaData = RepFromWebsite::createEmptyRep($rep, $basePostID, $conn, $sqlLogger);
                // pop the first element off the array to get the post_id
                $post_id = array_shift($arrMetaData)['post_id'];
                $post_name = array_shift($arrMetaData)['post_name'];
                $post_content = '';
                $newRep = new RepFromWebsite($rep, $post_id, $post_name, $post_content, $arrMetaData, $repDataDictionary);
                $newRep->checkForUpdates($repData);
        }
    }
    RepFromWebsite::finaliseHolidays();
} catch (Exception $e) {
    Utils::logger()->emergency(
        'Failed to update Rep: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}
$conn->close();
Utils::logger()->info('End of SCGB Rep Loader', array('file' => basename(__FILE__), 'line' => __LINE__));
die(0);
// End of main processing
// NOTREACHED
